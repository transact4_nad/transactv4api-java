/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transactv4api.java.integracion;

import com.uy.nad.transact4.api.DevolucionAutomaticaRespuesta;
import com.uy.nad.transact4.api.ITransActAPI_Tarjetas_v400;
import com.uy.nad.transact4.api.TransActAPI_Fabrica;
import com.uy.nad.transact4.api.IAPIListener;
import com.uy.nad.transact4.api.IDatosCierre;
import com.uy.nad.transact4.api.IMetadatosSolicitudCampo;
import java.awt.Cursor;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

public class IntegracionExtendida extends javax.swing.JFrame {

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        textTermCod = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textEmpCod = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        textMultiEmp = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        textPosDireccionIP = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        textPOSPuerto = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        textPosTimeout = new javax.swing.JTextField();
        radioPosTipoConexionTCP = new javax.swing.JRadioButton();
        radioPosTipoConexionBridge = new javax.swing.JRadioButton();
        jCheckBoxUsarGUI = new javax.swing.JCheckBox();
        jCheckBoxPropinaEnPos = new javax.swing.JCheckBox();
        jPanel6 = new javax.swing.JPanel();
        buttonCierre = new javax.swing.JButton();
        checkCierreUltimo = new javax.swing.JCheckBox();
        checkCierreCentralizado = new javax.swing.JCheckBox();
        jButtonProcesarTransaccion = new javax.swing.JButton();
        jButtonReiniciarDispositivo = new javax.swing.JButton();
        jButtonStatusDispositivo = new javax.swing.JButton();
        chkCargarTarj = new javax.swing.JCheckBox();
        chkMontoTap = new javax.swing.JCheckBox();
        jButtonReimprimir = new javax.swing.JButton();
        jButtonLeerMsr = new javax.swing.JButton();
        jCheckBoxModoEmulacion = new javax.swing.JCheckBox();
        jPanel8 = new javax.swing.JPanel();
        jCheckBoxConsumidorFinal = new javax.swing.JCheckBox();
        jTextFieldNroFactura = new javax.swing.JTextField();
        jTextFieldMontoIVA = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldMontoFactura = new javax.swing.JTextField();
        jTextFieldMontoGravado = new javax.swing.JTextField();
        jTextFieldIdDecretoLey = new javax.swing.JTextField();
        jButtonLimpiarDatosFacturacion = new javax.swing.JButton();
        jButtonResetDatosFacturacion = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jRadioButtonMonedaUYU = new javax.swing.JRadioButton();
        jRadioButtonMonedaUSD = new javax.swing.JRadioButton();
        jTextFieldMonto = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        jTextFieldPropina = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jTextFieldCuotas = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        jTextFieldCashback = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jRadioButtonDevolucion = new javax.swing.JRadioButton();
        jRadioButtonVenta = new javax.swing.JRadioButton();
        jPanelDatosDevolucionAutomatica = new javax.swing.JPanel();
        jTextFieldMontoDevolucionAutomatica = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jTextFieldAdquirenteId = new javax.swing.JTextField();
        jRadioButtonDevolucionAutomatica = new javax.swing.JRadioButton();
        jLabel14 = new javax.swing.JLabel();
        jTextFieldTicketOriginal = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jRadioButtonImprimirPOS = new javax.swing.JRadioButton();
        jRadioButtonImprimirERP = new javax.swing.JRadioButton();
        jRadioButtonNoImprimir = new javax.swing.JRadioButton();
        jPanel5 = new javax.swing.JPanel();
        jButtonReversarTransaccion = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jTextFieldTrnIdReversar = new javax.swing.JTextField();
        jCheckBoxSolicitarConfirmarTrn = new javax.swing.JCheckBox();
        jPanel7 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jTextFieldPlanId = new javax.swing.JTextField();
        jTextFieldPlanVtaId = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jTextFieldEstresTotalOperaciones = new javax.swing.JTextField();
        jTextFieldDelayTransacciones = new javax.swing.JTextField();
        jTextFieldmaximoOperacionesReiniciar = new javax.swing.JTextField();
        jLabelEstres = new javax.swing.JLabel();
        jButtonIniciarEstress = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ejemplo de Integracion API-Extendida");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Codigos Transact"));

        textTermCod.setText("T00028");
        textTermCod.setToolTipText("");
        textTermCod.setName("textTermCod"); // NOI18N

        jLabel2.setText("Codigo de Terminal");

        jLabel3.setText("Codigo de Empresa");

        textEmpCod.setText("NEWAGE");
        textEmpCod.setName("textEmpCod"); // NOI18N

        jLabel1.setText("(Estos Codigos debe ser solicitados a Transact para cada cliente)");

        jLabel4.setText("MultiEmp");

        textMultiEmp.setText("0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textEmpCod, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(textTermCod, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textMultiEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(21, 21, 21)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textEmpCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(textTermCod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(textMultiEmp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Conexion"));

        jLabel5.setText("IP");

        textPosDireccionIP.setText("192.168.1.11");
        textPosDireccionIP.setToolTipText("");

        jLabel6.setText("Puerto");

        textPOSPuerto.setText("2986");

        jLabel7.setText("Timeout");

        textPosTimeout.setText("55");

        buttonGroup4.add(radioPosTipoConexionTCP);
        radioPosTipoConexionTCP.setText("Tcp");

        buttonGroup4.add(radioPosTipoConexionBridge);
        radioPosTipoConexionBridge.setText("Bridge");
        radioPosTipoConexionBridge.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                radioPosTipoConexionBridgeStateChanged(evt);
            }
        });

        jCheckBoxUsarGUI.setActionCommand("Solicita Campos en API");
        jCheckBoxUsarGUI.setLabel("Solicita Campos en API");

        jCheckBoxPropinaEnPos.setLabel("Solicita Propina En POS");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(textPosDireccionIP, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(textPOSPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(textPosTimeout, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(radioPosTipoConexionTCP)
                        .addGap(105, 105, 105)
                        .addComponent(radioPosTipoConexionBridge)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jCheckBoxUsarGUI, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckBoxPropinaEnPos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(17, 17, 17))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radioPosTipoConexionTCP)
                    .addComponent(radioPosTipoConexionBridge)
                    .addComponent(jCheckBoxPropinaEnPos))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(textPosDireccionIP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(textPOSPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(textPosTimeout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxUsarGUI))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        buttonCierre.setText("Cierre Lote");
        buttonCierre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCierreActionPerformed(evt);
            }
        });

        checkCierreUltimo.setText("Consular Ultimo");

        checkCierreCentralizado.setText("Centralizado");

        jButtonProcesarTransaccion.setText("Procesar Transaccion");
        jButtonProcesarTransaccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonProcesarTransaccionActionPerformed(evt);
            }
        });

        jButtonReiniciarDispositivo.setText("Reiniciar");
        jButtonReiniciarDispositivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReiniciarDispositivoActionPerformed(evt);
            }
        });

        jButtonStatusDispositivo.setText("Status");
        jButtonStatusDispositivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonStatusDispositivoActionPerformed(evt);
            }
        });

        chkCargarTarj.setText("Cargar Tarj");

        chkMontoTap.setText("Monto Tap");

        jButtonReimprimir.setText("Reimprimir");
        jButtonReimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReimprimirActionPerformed(evt);
            }
        });

        jButtonLeerMsr.setText("Leer MSR");
        jButtonLeerMsr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLeerMsrActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(buttonCierre, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(checkCierreUltimo)
                    .addComponent(checkCierreCentralizado))
                .addGap(29, 29, 29)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonLeerMsr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonStatusDispositivo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonReiniciarDispositivo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonReimprimir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 16, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkCargarTarj, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(chkMontoTap, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonProcesarTransaccion)
                .addGap(19, 19, 19))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(buttonCierre, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel6Layout.createSequentialGroup()
                            .addComponent(jButtonProcesarTransaccion, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(22, 22, 22)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(chkCargarTarj)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chkMontoTap)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonReiniciarDispositivo)
                    .addComponent(checkCierreUltimo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonStatusDispositivo)
                    .addComponent(checkCierreCentralizado))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonReimprimir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonLeerMsr)
                .addGap(0, 5, Short.MAX_VALUE))
        );

        jCheckBoxModoEmulacion.setText("Modo Emulacion (Responderá siempre aprobado sin finalizar Transacción)");

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos de Facturación"));
        jPanel8.setName(""); // NOI18N

        jCheckBoxConsumidorFinal.setText("Consumidor Final");

        jTextFieldNroFactura.setText("0");

        jTextFieldMontoIVA.setText("0");

        jLabel9.setText("Nro Factura");

        jLabel10.setText("Monto IVA");

        jLabel11.setText("Monto Factura");

        jLabel12.setText("Monto Gravado");

        jLabel13.setText("Id Decreto Ley");

        jTextFieldMontoFactura.setText("0");

        jTextFieldMontoGravado.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        jTextFieldMontoGravado.setText("0");

        jTextFieldIdDecretoLey.setText("0");

        jButtonLimpiarDatosFacturacion.setText("Limpiar");
        jButtonLimpiarDatosFacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonLimpiarDatosFacturacionActionPerformed(evt);
            }
        });

        jButtonResetDatosFacturacion.setText("Por Defecto");
        jButtonResetDatosFacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonResetDatosFacturacionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(jCheckBoxConsumidorFinal)
                        .addGap(42, 42, 42)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldMontoFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jButtonLimpiarDatosFacturacion)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonResetDatosFacturacion))
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextFieldMontoIVA)
                                    .addComponent(jTextFieldNroFactura, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addComponent(jLabel13)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextFieldIdDecretoLey))
                                    .addGroup(jPanel8Layout.createSequentialGroup()
                                        .addComponent(jLabel12)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextFieldMontoGravado, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBoxConsumidorFinal)
                    .addComponent(jLabel11)
                    .addComponent(jTextFieldMontoFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldNroFactura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel12)
                    .addComponent(jTextFieldMontoGravado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldMontoIVA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel13)
                    .addComponent(jTextFieldIdDecretoLey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonLimpiarDatosFacturacion)
                    .addComponent(jButtonResetDatosFacturacion))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder("Monto"));

        buttonGroup1.add(jRadioButtonMonedaUYU);
        jRadioButtonMonedaUYU.setText("$");

        buttonGroup1.add(jRadioButtonMonedaUSD);
        jRadioButtonMonedaUSD.setText("U$S");

        jTextFieldMonto.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jTextFieldMonto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldMonto.setText("890");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(jRadioButtonMonedaUYU)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addComponent(jRadioButtonMonedaUSD)
                .addContainerGap())
            .addComponent(jTextFieldMonto)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonMonedaUYU)
                    .addComponent(jRadioButtonMonedaUSD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextFieldMonto, javax.swing.GroupLayout.DEFAULT_SIZE, 41, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder("Propina"));

        jTextFieldPropina.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jTextFieldPropina.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldPropina.setText("89");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTextFieldPropina, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jTextFieldPropina, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel12.setBorder(javax.swing.BorderFactory.createTitledBorder("Cuotas"));

        jTextFieldCuotas.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jTextFieldCuotas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldCuotas.setText("3");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTextFieldCuotas, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jTextFieldCuotas, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder("CashBack"));

        jTextFieldCashback.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jTextFieldCashback.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTextFieldCashback.setText("79");
        jTextFieldCashback.setToolTipText("");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTextFieldCashback, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(jTextFieldCashback, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Operacion"));

        buttonGroup2.add(jRadioButtonDevolucion);
        jRadioButtonDevolucion.setText("Devolucion");

        buttonGroup2.add(jRadioButtonVenta);
        jRadioButtonVenta.setText("Venta");
        jRadioButtonVenta.setName("jRadioButtonVenta"); // NOI18N

        jPanelDatosDevolucionAutomatica.setBorder(javax.swing.BorderFactory.createTitledBorder("Devolucion Auto"));

        jTextFieldMontoDevolucionAutomatica.setText("50");

        jLabel8.setText("Monto Dev");

        jLabel15.setText("Adquirente");

        jTextFieldAdquirenteId.setText("5");

        javax.swing.GroupLayout jPanelDatosDevolucionAutomaticaLayout = new javax.swing.GroupLayout(jPanelDatosDevolucionAutomatica);
        jPanelDatosDevolucionAutomatica.setLayout(jPanelDatosDevolucionAutomaticaLayout);
        jPanelDatosDevolucionAutomaticaLayout.setHorizontalGroup(
            jPanelDatosDevolucionAutomaticaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDatosDevolucionAutomaticaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelDatosDevolucionAutomaticaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelDatosDevolucionAutomaticaLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldMontoDevolucionAutomatica, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE))
                    .addGroup(jPanelDatosDevolucionAutomaticaLayout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldAdquirenteId)))
                .addGap(0, 0, 0))
        );
        jPanelDatosDevolucionAutomaticaLayout.setVerticalGroup(
            jPanelDatosDevolucionAutomaticaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelDatosDevolucionAutomaticaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelDatosDevolucionAutomaticaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTextFieldMontoDevolucionAutomatica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelDatosDevolucionAutomaticaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jTextFieldAdquirenteId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        buttonGroup2.add(jRadioButtonDevolucionAutomatica);
        jRadioButtonDevolucionAutomatica.setText("Devolucion Automatica");

        jLabel14.setText("Factura Original");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jRadioButtonVenta)
                        .addGap(18, 18, 18)
                        .addComponent(jRadioButtonDevolucion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jRadioButtonDevolucionAutomatica)
                        .addGap(17, 17, 17))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(jLabel14))
                            .addComponent(jTextFieldTicketOriginal, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanelDatosDevolucionAutomatica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonVenta)
                    .addComponent(jRadioButtonDevolucion)
                    .addComponent(jRadioButtonDevolucionAutomatica))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jLabel14)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldTicketOriginal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanelDatosDevolucionAutomatica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2))))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Impresión del Vocher"));

        buttonGroup3.add(jRadioButtonImprimirPOS);
        jRadioButtonImprimirPOS.setText("POS");

        buttonGroup3.add(jRadioButtonImprimirERP);
        jRadioButtonImprimirERP.setText("SOFTWARE FACTURACION");

        buttonGroup3.add(jRadioButtonNoImprimir);
        jRadioButtonNoImprimir.setText("NO IMPRIMIR");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jRadioButtonImprimirPOS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jRadioButtonImprimirERP)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jRadioButtonNoImprimir))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonImprimirPOS)
                    .addComponent(jRadioButtonImprimirERP)
                    .addComponent(jRadioButtonNoImprimir))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Reversos"));

        jButtonReversarTransaccion.setText("Reversar Transaccion");
        jButtonReversarTransaccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonReversarTransaccionActionPerformed(evt);
            }
        });

        jLabel16.setText("Trn ID Reversar:");

        jTextFieldTrnIdReversar.setText("0");

        jCheckBoxSolicitarConfirmarTrn.setText("Solicitar confirmar transaccion al usuario, si no confirma queda reverso de transaccion.");
        jCheckBoxSolicitarConfirmarTrn.setAutoscrolls(true);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jCheckBoxSolicitarConfirmarTrn, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(84, Short.MAX_VALUE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldTrnIdReversar, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonReversarTransaccion)
                        .addGap(25, 25, 25))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(jTextFieldTrnIdReversar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButtonReversarTransaccion, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jCheckBoxSolicitarConfirmarTrn, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Plan"));

        jLabel17.setText("Plan ID");

        jLabel18.setText("PlanVta ID");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextFieldPlanId, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 146, Short.MAX_VALUE)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextFieldPlanVtaId, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18)
                    .addComponent(jTextFieldPlanId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPlanVtaId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder("Modo Estres"));
        jPanel14.setToolTipText("");

        jLabel19.setText("Transacciones");

        jLabel20.setText("Delay(S)");

        jLabel21.setText("Reiniciar Aqui");

        jTextFieldEstresTotalOperaciones.setText("1000");

        jTextFieldDelayTransacciones.setText("20");

        jTextFieldmaximoOperacionesReiniciar.setText("130");

        jLabelEstres.setText("Total Iteraciones (0)");

        jButtonIniciarEstress.setText("Iniciar");
        jButtonIniciarEstress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonIniciarEstressActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel14Layout.createSequentialGroup()
                                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel21))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jTextFieldDelayTransacciones, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextFieldEstresTotalOperaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jTextFieldmaximoOperacionesReiniciar, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel14Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel20)))
                        .addGap(18, 18, 18)
                        .addComponent(jLabelEstres, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(22, 22, 22))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(jButtonIniciarEstress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabelEstres, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(jTextFieldEstresTotalOperaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextFieldDelayTransacciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(jTextFieldmaximoOperacionesReiniciar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(3, 3, 3)))
                .addComponent(jButtonIniciarEstress, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jPanel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(114, Short.MAX_VALUE)
                .addComponent(jCheckBoxModoEmulacion)
                .addGap(587, 587, 587))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(23, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBoxModoEmulacion, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21))
        );

        jPanel8.getAccessibleContext().setAccessibleDescription("");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    //ACCIONES
    private void jButtonProcesarTransaccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonProcesarTransaccionActionPerformed
        try {
           ProcesarTransaccion();    
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }

    }//GEN-LAST:event_jButtonProcesarTransaccionActionPerformed

    private void buttonCierreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCierreActionPerformed
        try {
            CierreLote();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_buttonCierreActionPerformed

    private void jButtonLimpiarDatosFacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLimpiarDatosFacturacionActionPerformed

        jTextFieldNroFactura.setText("0");
        jTextFieldMontoIVA.setText("0");
        jTextFieldMontoFactura.setText("0");
        jTextFieldMontoGravado.setText("0");
        jTextFieldIdDecretoLey.setText("0");
    }//GEN-LAST:event_jButtonLimpiarDatosFacturacionActionPerformed

    private void jButtonResetDatosFacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonResetDatosFacturacionActionPerformed
        // TODO add your handling code here:
        jTextFieldNroFactura.setText("1234567");
        jTextFieldMontoIVA.setText("220");
        jTextFieldMontoFactura.setText("1220");
        jTextFieldMontoGravado.setText("1000");
        jTextFieldIdDecretoLey.setText("0");
    }//GEN-LAST:event_jButtonResetDatosFacturacionActionPerformed

    private void jButtonReiniciarDispositivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReiniciarDispositivoActionPerformed
        // TODO add your handling code here:
        ReiniciarDispositivo();
    }//GEN-LAST:event_jButtonReiniciarDispositivoActionPerformed

    private void jButtonStatusDispositivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonStatusDispositivoActionPerformed
        // TODO add your handling code here:
        StatusDispositivo();
    }//GEN-LAST:event_jButtonStatusDispositivoActionPerformed

    private void jButtonReversarTransaccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReversarTransaccionActionPerformed
        // TODO add your handling code here:
        ReversarTransaccion();
    }//GEN-LAST:event_jButtonReversarTransaccionActionPerformed

    private void radioPosTipoConexionBridgeStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_radioPosTipoConexionBridgeStateChanged
        if(radioPosTipoConexionBridge.isSelected()){
            textPosDireccionIP.setText("127.0.0.1");
            textPOSPuerto.setText("2988");
            
        }else{
            textPosDireccionIP.setText("192.168.1.11");
            textPOSPuerto.setText("2986");
        }
// TODO add your handling code here:
    }//GEN-LAST:event_radioPosTipoConexionBridgeStateChanged

    private void jButtonReimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonReimprimirActionPerformed
         Reimprimir();
    }//GEN-LAST:event_jButtonReimprimirActionPerformed

    private void jButtonLeerMsrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonLeerMsrActionPerformed
        // TODO add your handling code here:
        LeerTarjeta();
    }//GEN-LAST:event_jButtonLeerMsrActionPerformed

    private void jButtonIniciarEstressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonIniciarEstressActionPerformed
        if(jButtonIniciarEstress.getText().equals("Iniciar")){
            salirHilo = false;
            jButtonIniciarEstress.setText("Detener");
         swingWorker = new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                      return EstresarAplicacionUX();
                    }

                    @Override
                    protected void done() {
                         jButtonIniciarEstress.setText("Iniciar");
                         jButtonIniciarEstress.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                         JOptionPane.showMessageDialog(null,"FINALIZADO" );
                    }

                };
         swingWorker.execute();    
        }else{
        salirHilo = true;
        jButtonIniciarEstress.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        while(!swingWorker.isDone()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(IntegracionExtendida.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
      
        }        
       
           
        
    }//GEN-LAST:event_jButtonIniciarEstressActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IntegracionExtendida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IntegracionExtendida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IntegracionExtendida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IntegracionExtendida.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                IntegracionExtendida dialog = new IntegracionExtendida(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonCierre;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.JCheckBox checkCierreCentralizado;
    private javax.swing.JCheckBox checkCierreUltimo;
    private javax.swing.JCheckBox chkCargarTarj;
    private javax.swing.JCheckBox chkMontoTap;
    private javax.swing.JButton jButtonIniciarEstress;
    private javax.swing.JButton jButtonLeerMsr;
    private javax.swing.JButton jButtonLimpiarDatosFacturacion;
    private javax.swing.JButton jButtonProcesarTransaccion;
    private javax.swing.JButton jButtonReimprimir;
    private javax.swing.JButton jButtonReiniciarDispositivo;
    private javax.swing.JButton jButtonResetDatosFacturacion;
    private javax.swing.JButton jButtonReversarTransaccion;
    private javax.swing.JButton jButtonStatusDispositivo;
    private javax.swing.JCheckBox jCheckBoxConsumidorFinal;
    private javax.swing.JCheckBox jCheckBoxModoEmulacion;
    private javax.swing.JCheckBox jCheckBoxPropinaEnPos;
    private javax.swing.JCheckBox jCheckBoxSolicitarConfirmarTrn;
    private javax.swing.JCheckBox jCheckBoxUsarGUI;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelEstres;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanelDatosDevolucionAutomatica;
    private javax.swing.JRadioButton jRadioButtonDevolucion;
    private javax.swing.JRadioButton jRadioButtonDevolucionAutomatica;
    private javax.swing.JRadioButton jRadioButtonImprimirERP;
    private javax.swing.JRadioButton jRadioButtonImprimirPOS;
    private javax.swing.JRadioButton jRadioButtonMonedaUSD;
    private javax.swing.JRadioButton jRadioButtonMonedaUYU;
    private javax.swing.JRadioButton jRadioButtonNoImprimir;
    private javax.swing.JRadioButton jRadioButtonVenta;
    private javax.swing.JTextField jTextFieldAdquirenteId;
    private javax.swing.JTextField jTextFieldCashback;
    private javax.swing.JTextField jTextFieldCuotas;
    private javax.swing.JTextField jTextFieldDelayTransacciones;
    private javax.swing.JTextField jTextFieldEstresTotalOperaciones;
    private javax.swing.JTextField jTextFieldIdDecretoLey;
    private javax.swing.JTextField jTextFieldMonto;
    private javax.swing.JTextField jTextFieldMontoDevolucionAutomatica;
    private javax.swing.JTextField jTextFieldMontoFactura;
    private javax.swing.JTextField jTextFieldMontoGravado;
    private javax.swing.JTextField jTextFieldMontoIVA;
    private javax.swing.JTextField jTextFieldNroFactura;
    private javax.swing.JTextField jTextFieldPlanId;
    private javax.swing.JTextField jTextFieldPlanVtaId;
    private javax.swing.JTextField jTextFieldPropina;
    private javax.swing.JTextField jTextFieldTicketOriginal;
    private javax.swing.JTextField jTextFieldTrnIdReversar;
    private javax.swing.JTextField jTextFieldmaximoOperacionesReiniciar;
    private javax.swing.JRadioButton radioPosTipoConexionBridge;
    private javax.swing.JRadioButton radioPosTipoConexionTCP;
    private javax.swing.JTextField textEmpCod;
    private javax.swing.JTextField textMultiEmp;
    private javax.swing.JTextField textPOSPuerto;
    private javax.swing.JTextField textPosDireccionIP;
    private javax.swing.JTextField textPosTimeout;
    private javax.swing.JTextField textTermCod;
    // End of variables declaration//GEN-END:variables
    SwingWorker<Void, Void>  swingWorker;
    Boolean salirHilo;
    public IntegracionExtendida() {
    }

    public IntegracionExtendida(java.awt.Frame parent, boolean modal) {
        super();
        initComponents();
        radioPosTipoConexionTCP.setSelected(true);
        jPanelDatosDevolucionAutomatica.setEnabled(false);
        jRadioButtonVenta.setSelected(true);
        jRadioButtonMonedaUYU.setSelected(true);
        jRadioButtonNoImprimir.setSelected(true);

    }

////////////////IMPLEMENTACIONES 
    private void ProcesarTransaccion() throws Exception {
        //#############################################################################################
        //###################       REGISTRA LISTENER PARA RECIBIR EVENTOS DEL API  ###################
        //#############################################################################################
        TransActAPI_Fabrica objFabrica = new TransActAPI_Fabrica();
        ITransActAPI_Tarjetas_v400 objTransactAPITarjetas = objFabrica.ObtenerTransactAPITarjetas(new IAPIListener() {
            @Override
            public void evtNotificadAvance(String mensaje) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### MUESTRA UNA VENTANA DE NOTIFICACION DE AVANCE###################");
                System.out.println("####   " + mensaje + "   ####");
                System.out.println("###################################################################################################");
            }

            @Override
            public void evtSolicitarCampoEnAPI(IMetadatosSolicitudCampo datosSolicitudCampo) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### INGRESA EL DATO ###################");
                System.out.println("####   " + datosSolicitudCampo.getWfCampoMensaje() + "   ####");
                System.out.println("###################################################################################################");
                VentanaPedirDato ventanaPedirDato = new VentanaPedirDato(null,true);
                ventanaPedirDato.setDatosSolicitudCampo(datosSolicitudCampo);
                //ventanaPedirDato.setSize(200,200);
                ventanaPedirDato.setLocationRelativeTo(null);
                ventanaPedirDato.setVisible(true);
                 
                
            }
        });

        //#############################################################################################
        //###################       CONFIGURACION      ################################################
        //#############################################################################################
        objTransactAPITarjetas.Configuracion().setModoEmulacion(jCheckBoxModoEmulacion.isSelected());
        objTransactAPITarjetas.Configuracion().setPOSTipo(0);
         //>>>>>>>>>>> DATOS GUI
        int GUITipo = 9;
        if(jCheckBoxUsarGUI.isSelected()){
            GUITipo = 0;
        }
        objTransactAPITarjetas.Configuracion().setGUITipo(GUITipo);
        objTransactAPITarjetas.Configuracion().setIngresaPropinaEnPos(jCheckBoxPropinaEnPos.isSelected());
        // >>>>>>>>>>>> DATOS DE CONEXION <<<<<<<<<<<<<<<<<<<<<
        int tipoConexion = 1;
        if (radioPosTipoConexionTCP.isSelected()) {
            tipoConexion = 1;
        } else if (radioPosTipoConexionBridge.isSelected()) {
            tipoConexion = 0;
        }
        objTransactAPITarjetas.Configuracion().setPOSTipoCnx(tipoConexion);
        objTransactAPITarjetas.Configuracion().setPOSDireccionIP(textPosDireccionIP.getText());
        objTransactAPITarjetas.Configuracion().setPOSPuerto(Integer.parseInt(textPOSPuerto.getText()));
        objTransactAPITarjetas.Configuracion().setPOSSegsTimeout(Integer.parseInt(textPosTimeout.getText()));
        objTransactAPITarjetas.Configuracion().setURLConcentrador("http://wwwi.transact.com.uy/Concentrador");

        // >>>>>>>>>>>> DATOS DE IMPRESION <<<<<<<<<<<<<<<<<<<<<            
        objTransactAPITarjetas.Configuracion().setImpresionTipo(ObtenerTipoImpresion());

        //#############################################################################################
        //###################       DATOS TRANSACCION        ##########################################
        //#############################################################################################
        objTransactAPITarjetas.Transaccion().NuevaTransaccion();

        objTransactAPITarjetas.Transaccion().setEmpCod(textEmpCod.getText());
        objTransactAPITarjetas.Transaccion().setTermCod(textTermCod.getText());
        objTransactAPITarjetas.Transaccion().setOperacion(ObtenerOperacion());
        objTransactAPITarjetas.Transaccion().setMonedaISO(ObtenerMonedaOperacion());
        objTransactAPITarjetas.Transaccion().setMonto(ObtenerMontoOperacion());
        
        if (jTextFieldPropina.getText().length() > 0){
             objTransactAPITarjetas.Transaccion().setMontoPropina(ObtenerPropinaOperacion());
        }
        if (jTextFieldCashback.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setMontoCashBack(new Long(0));
        }
        if (jTextFieldNroFactura.getText().length() > 0){
           objTransactAPITarjetas.Transaccion().setFacturaNro(ObtenerFacturaNro()); 
        }
        if (jTextFieldMontoGravado.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setFacturaMontoGravado(ObtenerFacturaMontoGravado());
        }
        if (jTextFieldMontoIVA.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setFacturaMontoIVA(ObtenerFacturaMontoIVA());
        }
        if (jTextFieldTicketOriginal.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setTicketOriginal(ObtenerTicketOriginal());
        }
        if (jTextFieldMontoFactura.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setFacturaMonto(ObtenerFacturaMonto());
        }
        
        
        

        //#############################################################################################
        //###################       CASO DEVOLUCION AUTOMATICA, ESPECIFICAR ADQID       ###############
        //#############################################################################################
        if (jRadioButtonDevolucionAutomatica.isSelected()) {
            objTransactAPITarjetas.Transaccion().setTarjetaId(ObtenerTarjetaTipo());
        }

        //#############################################################################################
        //###################       TRANSACCION EXTENDIDA       #######################################
        //#############################################################################################
        objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setCuotas(ObtenerCuotasOperacion());
        if (jCheckBoxConsumidorFinal.isSelected()) {
            objTransactAPITarjetas.Transaccion().setFacturaConsumidorFinal(true);
        } else {
            objTransactAPITarjetas.Transaccion().setFacturaConsumidorFinal(false);
        }

        objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setDecretoLeyId(ObtenerDecretoLey());
        if(!jTextFieldPlanId.getText().isEmpty()){
             Integer planID = Integer.parseInt(jTextFieldPlanId.getText());
            if(planID > 0){
                objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setPlanId(planID);
            }
        }
        if(!jTextFieldPlanVtaId.getText().isEmpty()){
            Integer planvtaID = Integer.parseInt(jTextFieldPlanVtaId.getText());
            if(planvtaID > 0){
                objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setPlanVtaId(planvtaID);
            }
        }
        
       
        if(chkCargarTarj.isSelected()){
          //ACA SE PUEDE DEFINIR EL TIEMPO ENTRE LECTURA Y TRANSACCION 
           objTransactAPITarjetas.Configuracion().setSegTimeoutLecturaTarjFactura(20);
           if(chkMontoTap.isSelected()){
             //Para no hacer doble tap en tarjetas ctls se necesita setear la moneda y el montoTAP
             objTransactAPITarjetas.Transaccion().setMonedaISO(ObtenerMonedaOperacion());
            Double montoOperacion = new Double(0);
            if (jTextFieldMonto.getText().length() > 0) {
                montoOperacion += Double.parseDouble(jTextFieldMonto.getText()) * 100;
            }
            if (jTextFieldPropina.getText().length() > 0) {
                montoOperacion += Double.parseDouble(jTextFieldPropina.getText()) * 100;
            }
              if (jTextFieldCashback.getText().length() > 0) {
                montoOperacion += Double.parseDouble(jTextFieldCashback.getText()) * 100;
            }
            objTransactAPITarjetas.Transaccion().setMontoTap(montoOperacion.longValue());
           }
           
           if(objTransactAPITarjetas.Transaccion().CargarDatosTarjeta()){
            String Msg = "TarjetaId: " + objTransactAPITarjetas.Transaccion().getDatosTarjeta().getTarjetaId() + "\n" +
                    "TarjetaTipo: " + objTransactAPITarjetas.Transaccion().getDatosTarjeta().getTarjetaTipo() +"\n" + 
                     "TarjetaNro: " + objTransactAPITarjetas.Transaccion().getDatosTarjeta().getTarjetaNro() +  "\n"+
                    "EmisorId: " + objTransactAPITarjetas.Transaccion().getDatosTarjeta().getEmisorId()+ "\n" + 
                    "TarjetaIIN: " + objTransactAPITarjetas.Transaccion().getDatosTarjeta().getTarjetaIIN()+ "\n" + 
                    "TarjetaUlt4: " + objTransactAPITarjetas.Transaccion().getDatosTarjeta().getTarjetaUlt4()+ "\n" + 
                    "TarjetaAlimentacion: " + objTransactAPITarjetas.Transaccion().getDatosTarjeta().getTarjetaAlimentacion()+ "\n" + 
                    "TarjetaLargo: " + objTransactAPITarjetas.Transaccion().getDatosTarjeta().getTarjetaLargo()+ "\n" +
                    "TarjetaPrepaga: " + objTransactAPITarjetas.Transaccion().getDatosTarjeta().getTarjetaPrepaga()+ "\n";
              Msg += "\n---------------\n";
              Msg += "DESEA CONTINUAR ?\n" ;
              int accion = JOptionPane.showConfirmDialog(this, Msg, "LECTURA TARJETA", JOptionPane.OK_CANCEL_OPTION);
              if ( accion != 0){
                objTransactAPITarjetas.Transaccion().Cancelar();
                return;
              }
           }else{
               String MsgErr = "Error al Obtener Tarjeta - Respuesta: \n"  + objTransactAPITarjetas.Transaccion().getRespuesta().getCodRespuesta() + " - " + objTransactAPITarjetas.Transaccion().getRespuesta().getMsgRespuesta();
               JOptionPane.showMessageDialog(this,MsgErr );
               return;
           }
        }


        //#############################################################################################
        //###################       PROCESAR TRANSACCION         #######################################
        //#############################################################################################
        objTransactAPITarjetas.Transaccion().ProcesarTransaccion();

        //Verifico Respuesta del Autorizador
        if (!objTransactAPITarjetas.Transaccion().getRespuesta().getAprobada()) {
            //#############################################################################################
            //###################       SE SOLICITA EN CASO DE DEVOLUCION AUTOMATICA EN DEPENDENCIA DEL 
            //###################       CODIGO DE SALIDA SI SE DESEA APLICAR DEVOLUCION MANUAL O NO    ####
            //#############################################################################################
            if (ObtenerOperacion().equals("DVA")) {
                if (objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().compareTo(DevolucionAutomaticaRespuesta.enumDevolucionAutoCodigo.DEV_AUTO_POR_DEVOLUCION) == 0) {
                    if (JOptionPane.showConfirmDialog(this, "Se solicita realizar la devolucion manual, desea efectuarla?", "Confirmar", JOptionPane.OK_CANCEL_OPTION) == 0) {
                        jRadioButtonDevolucion.setSelected(true);
                        jRadioButtonDevolucionAutomatica.setSelected(false);
                        ProcesarTransaccion();
                    } else {
                        JOptionPane.showMessageDialog(this, "DEV AUTO - Respuesta: \n" + objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().toString() + " - " + objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoMensaje());
                    }
                } else if (objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().compareTo(DevolucionAutomaticaRespuesta.enumDevolucionAutoCodigo.DEV_AUTO_POR_CARTA) == 0) {
                    JOptionPane.showMessageDialog(this, "Debe solicitar la devolución por carta de la operación.");
                } else if (objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().compareTo(DevolucionAutomaticaRespuesta.enumDevolucionAutoCodigo.DEV_AUTO_ERROR) == 0) {
                    JOptionPane.showMessageDialog(this, "Error en la operación.");
                } else if (objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().compareTo(DevolucionAutomaticaRespuesta.enumDevolucionAutoCodigo.DEV_AUTO_OK) == 0) {
                    JOptionPane.showMessageDialog(this, "Operación exitosa.");
                }
                return;
            } else {
                JOptionPane.showMessageDialog(this, "Denegada - Respuesta: \n" + objTransactAPITarjetas.Transaccion().getRespuesta().getCodRespuesta() + " - " + objTransactAPITarjetas.Transaccion().getRespuesta().getMsgRespuesta());
                return;
            }
        }

        //#############################################################################################
        //###################       ACÁ SE DEBE PERSISTIR LOS DATOS DEL PAGO (TRNID,TICKET,LOTE Y NROAUTORIZACION) 
        //###################       EN EL SOFTWARE DE FACTURACIÓN COMO PENDIENTES DE CONFIRMACIÓN  ####
        //#############################################################################################
        Long TrnId = objTransactAPITarjetas.Transaccion().getRespuesta().getTransaccionId();
        Long ticket = objTransactAPITarjetas.Transaccion().getRespuesta().getTicket();
        Long lote = objTransactAPITarjetas.Transaccion().getRespuesta().getLote();
        String NroAutorizacion = objTransactAPITarjetas.Transaccion().getRespuesta().getNroAutorizacion();

        //************************************************************************************************************
        //***GUARDO EL TRNID PARA POSIBLE REVERSO
        //************************************************************************************************************
        jTextFieldTrnIdReversar.setText(objTransactAPITarjetas.Transaccion().getRespuesta().getTransaccionId().toString());

        //#############################################################################################
        //###################       CONFIRMACION DE LA TRANSACCION#####################################
        //#############################################################################################
        Boolean ConfirmadaOK = false;

        if (jCheckBoxSolicitarConfirmarTrn.isSelected()) {
            if (JOptionPane.showConfirmDialog(this, "Desea confirmar la transacción?", "Confirmar", JOptionPane.OK_CANCEL_OPTION) == 0) {
                ConfirmadaOK = objTransactAPITarjetas.Transaccion().ConfirmarTransaccion(TrnId);
            }
        } else {
            ConfirmadaOK = objTransactAPITarjetas.Transaccion().ConfirmarTransaccion(TrnId);
        }

        if (!ConfirmadaOK) {
            //****************************************************************************************************
            //*** ACÁ SE DEBE BORRAR DEL SOFTWARE DE FACTURACIÓN EL PAGO QUE ESTABA PENDIENTE DE CONFIRMACIÓN  ***
            //***NOTA: LA AUTORIZACIÓN CUYA CONFIRMACIÓN FALLÓ, SE REVERSARÁ AL REALIZAR LA PROXIMA TRANSACCIÓN***
            //****************************************************************************************************
            JOptionPane.showMessageDialog(this, "Error Confirmando Transacción: \n"
                    + objTransactAPITarjetas.Transaccion().getRespuesta().getCodRespuesta()
                    + " - " + objTransactAPITarjetas.Transaccion().getRespuesta().getMsgRespuesta());
           
            //****************************************************************************************************
            //*** ACA SE DEBE COMPROBAR EL ESTADO DE LA ULTIMA CONFIRMACION Y TOMAR MEDIDAS ***
            //***NOTA: SI EL CONSULTAR ESTADO DA FALSO ENCONTCES SE DEBE REVERSAR EN EL ERP                    ***
            //****************************************************************************************************
            boolean salir = false;
            
        }
        else {

            //************************************************************************************************************
            //***ACÁ SE DEBE MARCAR EL PAGO EN EL SOFTWARE DE FACTURACIÓN COMO CONFIRMADO***
            //***NOTA: EN EL CASO EN QUE LA IMPRESIÓN LA HACE EL SOFTWARE DE FACTURACIÓN, AQUÍ SE PROCEDERA A IMPRIMIR EL VOUCHER 
            //************************************************************************************************************
            System.out.println("Aprobada" + objTransactAPITarjetas.Transaccion().getRespuesta().getAprobada());
            System.out.println("TarjetaID" + objTransactAPITarjetas.Transaccion().getRespuesta().getTarjetaId());
            System.out.println("Nro Autorizacion" + objTransactAPITarjetas.Transaccion().getRespuesta().getNroAutorizacion());

            //************************************************************************************************************
            //***ACA GUARDO EL ADQID Y NRO FACTURA DE LA ULTIMA OPERACION PARA POSIBLE DEVOLUCION AUTOMATICA
            //************************************************************************************************************
            jTextFieldAdquirenteId.setText(objTransactAPITarjetas.Transaccion().getRespuesta().getTarjetaId().toString());
            jTextFieldTicketOriginal.setText(objTransactAPITarjetas.Transaccion().getRespuesta().getDatosTransaccion().getDatosTransaccionExtendida().getFacturaNro().toString());

            JOptionPane.showMessageDialog(this, "Aprobada - Nro. Autorización: " + objTransactAPITarjetas.Transaccion().getRespuesta().getNroAutorizacion());
            VentanaRespuestaTransaccion ventanaResp = new VentanaRespuestaTransaccion(this,true);
            ventanaResp.SetTransaccionRespuesta(objTransactAPITarjetas.Transaccion().getRespuesta());
            ventanaResp.show();
        }
    }

    private void CierreLote() throws Exception {
        //#############################################################################################
        //###################       REGISTRA LISTENER PARA RECIBIR EVENTOS DEL API  ###################
        //#############################################################################################
        TransActAPI_Fabrica objFabrica = new TransActAPI_Fabrica();
        ITransActAPI_Tarjetas_v400 objTransactAPITarjetas = objFabrica.ObtenerTransactAPITarjetas(new IAPIListener() {
            @Override
            public void evtNotificadAvance(String mensaje) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### MUESTRA UNA VENTANA DE NOTIFICACION DE AVANCE###################");
                System.out.println("####   " + mensaje + "   ####");
                System.out.println("###################################################################################################");
            }

            @Override
            public void evtSolicitarCampoEnAPI(IMetadatosSolicitudCampo datosSolicitudCampo) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### INGRESA EL DATO ###################");
                System.out.println("####   " + datosSolicitudCampo.getWfCampoMensaje() + "   ####");
                System.out.println("###################################################################################################");
            }
        });

        //#############################################################################################
        //###################       CONFIGURACION      ################################################
        //#############################################################################################
        // >>>>>>>>>>>> DATOS DE CONEXION <<<<<<<<<<<<<<<<<<<<<
        objTransactAPITarjetas.Configuracion().setModoEmulacion(jCheckBoxModoEmulacion.isSelected());
        objTransactAPITarjetas.Configuracion().setPOSTipo(0);
        //>>>>>>>>>>> DATOS GUI
        int GUITipo = 9;
        if(jCheckBoxUsarGUI.isSelected()){
            GUITipo = 0;
        }
        objTransactAPITarjetas.Configuracion().setGUITipo(GUITipo);
        
          // >>>>>>>>>>>> DATOS DE CONEXION <<<<<<<<<<<<<<<<<<<<<
        int tipoConexion = 1;
        if (radioPosTipoConexionTCP.isSelected()) {
            tipoConexion = 1;
        } else if (radioPosTipoConexionBridge.isSelected()) {
            tipoConexion = 0;
        }
        objTransactAPITarjetas.Configuracion().setPOSTipoCnx(tipoConexion);
        objTransactAPITarjetas.Configuracion().setPOSDireccionIP(textPosDireccionIP.getText());
        objTransactAPITarjetas.Configuracion().setPOSPuerto(Integer.parseInt(textPOSPuerto.getText()));
        objTransactAPITarjetas.Configuracion().setPOSSegsTimeout(Integer.parseInt(textPosTimeout.getText()));
        objTransactAPITarjetas.Configuracion().setURLConcentrador("http://wwwi.transact.com.uy/Concentrador");


        // >>>>>>>>>>>> DATOS DE IMPRESION <<<<<<<<<<<<<<<<<<<<<            
         objTransactAPITarjetas.Configuracion().setImpresionTipo(ObtenerTipoImpresion());

        //#############################################################################################
        //###################       CIERRE          ################################################
        //##########################################################################################
        // >>>>>>>>>>>>>>>>>>> CARGA LOS DATOS DEL CIERRE DE LOTE
        objTransactAPITarjetas.CierreLote().setEmpCod(textEmpCod.getText());
        objTransactAPITarjetas.CierreLote().setTermCod(textTermCod.getText());
//        objTransactAPITarjetas.CierreLote().setProcesadorId(0);
        String TokenCierre = "";
        if (checkCierreCentralizado.isSelected()) {
            objTransactAPITarjetas.CierreLote().setCierreCentralizado(true);
            objTransactAPITarjetas.CierreLote().setEmpHash("DF4D21265D1F2F1DDF4D21265D1F2F1D");
        }

        if (checkCierreUltimo.isSelected()) {
            TokenCierre = "ULTIMO";
        } else {
            // >>>>>>>>>>>>>>>>>>> SOLICITA EL CIERRE Y SE QUEDA CON EL TOCKEN DE CONSULTA
            objTransactAPITarjetas.CierreLote().ProcesarCierre();
            if (objTransactAPITarjetas.CierreLote().getRespuesta().getFinalizado()) {
                JOptionPane.showMessageDialog(this, "Error Cerrando Lotes: " + objTransactAPITarjetas.CierreLote().getRespuesta().getEstado());
                return;
            }
            TokenCierre = objTransactAPITarjetas.CierreLote().getRespuesta().getTokenCierre();
            //    
        }
        // >>>>>>>>>>>>>>>>>>> SE QUEDA CONSULTANDO ESTADO DEL CIERRE MIENTRAS NO FINALICE O DE ERROR
        //Cada 5 segundos verifico el estado de avance del procesamiento de cierre
        Boolean ResultOK = false;
        Boolean finalizado = false;
        while (!objTransactAPITarjetas.CierreLote().getRespuesta().getFinalizado()) {
            if (!checkCierreUltimo.isSelected()) {
                Thread.sleep(5000);
            }
            ResultOK = objTransactAPITarjetas.CierreLote().ConsultarEstadoCierre(TokenCierre);
            if (!objTransactAPITarjetas.CierreLote().getRespuesta().getFinalizado()) {
                System.out.println("CIERRE LOTE AUN NO FINALIZADO");
            }
        }
        if (!ResultOK) {
            JOptionPane.showMessageDialog(this, "Error Cerrando Lotes: " + objTransactAPITarjetas.CierreLote().getRespuesta().getEstado());
            return;
        }
        String CierresSTR = "";
        for (IDatosCierre cierre : objTransactAPITarjetas.CierreLote().getRespuesta().getDatosCierre()) {
            //'***********************************************************************************************
            //'***ACÁ SE DEBE IMPACTAR TODOS LOS PAGOS DEL LOTE EN EL SOFTWARE DE FACTURACIÓN COMO CERRADOS***
            //'***NOTA: SI CORRESPONDE, AQUÍ SE PROCEDERA A IMPRIMIR EL VOUCHER                            ***
            // '***********************************************************************************************
            CierresSTR += "Lote: " + cierre.getLote()
                    + "\nProc: " + cierre.getProcesadorId()
                    + "\nAprobado: " + cierre.getAprobado()
                    + "\nProc: " + cierre.getProcesadorId()
                    + "\n--------------------------------";
        }
        JOptionPane.showMessageDialog(this, CierresSTR);
        for (String lineaVoucher : objTransactAPITarjetas.CierreLote().getRespuesta().getVoucher()) {
            System.out.println(lineaVoucher);
         }
       VentanaRespuestaTransaccion ventanaResp = new VentanaRespuestaTransaccion(this,true);
       ventanaResp.SetCierreRespuesta(objTransactAPITarjetas.CierreLote().getRespuesta());
       ventanaResp.show();
        
    }

    private void ReiniciarDispositivo() {
        //#############################################################################################
        //###################       REGISTRA LISTENER PARA RECIBIR EVENTOS DEL API  ###################
        //#############################################################################################
        TransActAPI_Fabrica objFabrica = new TransActAPI_Fabrica();
        ITransActAPI_Tarjetas_v400 objTransactAPITarjetas = objFabrica.ObtenerTransactAPITarjetas(new IAPIListener() {
            @Override
            public void evtNotificadAvance(String mensaje) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### MUESTRA UNA VENTANA DE NOTIFICACION DE AVANCE###################");
                System.out.println("####   " + mensaje + "   ####");
                System.out.println("###################################################################################################");
            }

            @Override
            public void evtSolicitarCampoEnAPI(IMetadatosSolicitudCampo datosSolicitudCampo) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### INGRESA EL DATO ###################");
                System.out.println("####   " + datosSolicitudCampo.getWfCampoMensaje() + "   ####");
                System.out.println("###################################################################################################");
            }
        });

        //#############################################################################################
        //###################       CONFIGURACION      ################################################
        //#############################################################################################
        objTransactAPITarjetas.Configuracion().setModoEmulacion(jCheckBoxModoEmulacion.isSelected());
        objTransactAPITarjetas.Configuracion().setPOSTipo(0);
         //>>>>>>>>>>> DATOS GUI
        int GUITipo = 9;
        if(jCheckBoxUsarGUI.isSelected()){
            GUITipo = 0;
        }
        objTransactAPITarjetas.Configuracion().setGUITipo(GUITipo);
        // >>>>>>>>>>>> DATOS DE CONEXION <<<<<<<<<<<<<<<<<<<<<
        int tipoConexion = 1;
        if (radioPosTipoConexionTCP.isSelected()) {
            tipoConexion = 1;
        } else if (radioPosTipoConexionBridge.isSelected()) {
            tipoConexion = 0;
        }
        objTransactAPITarjetas.Configuracion().setPOSTipoCnx(tipoConexion);
        objTransactAPITarjetas.Configuracion().setPOSDireccionIP(textPosDireccionIP.getText());
        objTransactAPITarjetas.Configuracion().setPOSPuerto(Integer.parseInt(textPOSPuerto.getText()));
        objTransactAPITarjetas.Configuracion().setPOSSegsTimeout(Integer.parseInt(textPosTimeout.getText()));
        objTransactAPITarjetas.Configuracion().setURLConcentrador("http://wwwi.transact.com.uy/Concentrador");

        // >>>>>>>>>>>> DATOS DE IMPRESION <<<<<<<<<<<<<<<<<<<<<            
        objTransactAPITarjetas.Configuracion().setImpresionTipo(ObtenerTipoImpresion());
        objTransactAPITarjetas.Transaccion().setEmpCod(textEmpCod.getText());
        objTransactAPITarjetas.Transaccion().setTermCod(textTermCod.getText());

        //#############################################################################################
        //###################       DATOS TRANSACCION        ##########################################
        //#############################################################################################
        if (objTransactAPITarjetas.Transaccion().DispositivoReiniciar()) {
            JOptionPane.showMessageDialog(this, "Reiniciando dispositivo");
        } else {
            JOptionPane.showMessageDialog(this, "Error al intentar reiniciar dispositivo");
        }

    }

    private void StatusDispositivo() {
        //#############################################################################################
        //###################       REGISTRA LISTENER PARA RECIBIR EVENTOS DEL API  ###################
        //#############################################################################################
        TransActAPI_Fabrica objFabrica = new TransActAPI_Fabrica();
        ITransActAPI_Tarjetas_v400 objTransactAPITarjetas = objFabrica.ObtenerTransactAPITarjetas(new IAPIListener() {
            @Override
            public void evtNotificadAvance(String mensaje) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### MUESTRA UNA VENTANA DE NOTIFICACION DE AVANCE###################");
                System.out.println("####   " + mensaje + "   ####");
                System.out.println("###################################################################################################");
            }

            @Override
            public void evtSolicitarCampoEnAPI(IMetadatosSolicitudCampo datosSolicitudCampo) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### INGRESA EL DATO ###################");
                System.out.println("####   " + datosSolicitudCampo.getWfCampoMensaje() + "   ####");
                System.out.println("###################################################################################################");
            }
        });

        //#############################################################################################
        //###################       CONFIGURACION      ################################################
        //#############################################################################################
       
        objTransactAPITarjetas.Configuracion().setModoEmulacion(jCheckBoxModoEmulacion.isSelected());
        objTransactAPITarjetas.Configuracion().setPOSTipo(0);
        //>>>>>>>>>>> DATOS GUI
        int GUITipo = 9;
        if(jCheckBoxUsarGUI.isSelected()){
            GUITipo = 0;
        }
        objTransactAPITarjetas.Configuracion().setGUITipo(GUITipo);
        // >>>>>>>>>>>> DATOS DE CONEXION <<<<<<<<<<<<<<<<<<<<<
        int tipoConexion = 1;
        if (radioPosTipoConexionTCP.isSelected()) {
            tipoConexion = 1;
        } else if (radioPosTipoConexionBridge.isSelected()) {
            tipoConexion = 0;
        }
        objTransactAPITarjetas.Configuracion().setPOSTipoCnx(tipoConexion);
        objTransactAPITarjetas.Configuracion().setPOSDireccionIP(textPosDireccionIP.getText());
        objTransactAPITarjetas.Configuracion().setPOSPuerto(Integer.parseInt(textPOSPuerto.getText()));
        objTransactAPITarjetas.Configuracion().setPOSSegsTimeout(Integer.parseInt(textPosTimeout.getText()));
        objTransactAPITarjetas.Configuracion().setURLConcentrador("http://wwwi.transact.com.uy/Concentrador");

        // >>>>>>>>>>>> DATOS DE IMPRESION <<<<<<<<<<<<<<<<<<<<<            
        objTransactAPITarjetas.Configuracion().setImpresionTipo(ObtenerTipoImpresion());
        objTransactAPITarjetas.Transaccion().setEmpCod(textEmpCod.getText());
        objTransactAPITarjetas.Transaccion().setTermCod(textTermCod.getText());

        //#############################################################################################
        //###################       DATOS TRANSACCION        ##########################################
        //#############################################################################################
        if (objTransactAPITarjetas.Transaccion().DispositivoEstado()) {
            JOptionPane.showMessageDialog(this, "Dispositivo esta listo");
        } else {
            JOptionPane.showMessageDialog(this, "Dispositivo no esta listo");
        }
    }

    private void ReversarTransaccion() {
        //#############################################################################################
        //###################       REGISTRA LISTENER PARA RECIBIR EVENTOS DEL API  ###################
        //#############################################################################################
        TransActAPI_Fabrica objFabrica = new TransActAPI_Fabrica();
        ITransActAPI_Tarjetas_v400 objTransactAPITarjetas = objFabrica.ObtenerTransactAPITarjetas(new IAPIListener() {
            @Override
            public void evtNotificadAvance(String mensaje) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### MUESTRA UNA VENTANA DE NOTIFICACION DE AVANCE###################");
                System.out.println("####   " + mensaje + "   ####");
                System.out.println("###################################################################################################");
            }

            @Override
            public void evtSolicitarCampoEnAPI(IMetadatosSolicitudCampo datosSolicitudCampo) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### INGRESA EL DATO ###################");
                System.out.println("####   " + datosSolicitudCampo.getWfCampoMensaje() + "   ####");
                System.out.println("###################################################################################################");
            }
        });

        //#############################################################################################
        //###################       CONFIGURACION      ################################################
        //#############################################################################################
        objTransactAPITarjetas.Configuracion().setModoEmulacion(jCheckBoxModoEmulacion.isSelected());
        objTransactAPITarjetas.Configuracion().setPOSTipo(0);
       //>>>>>>>>>>> DATOS GUI
        int GUITipo = 9;
        if(jCheckBoxUsarGUI.isSelected()){
            GUITipo = 0;
        }
        objTransactAPITarjetas.Configuracion().setGUITipo(GUITipo);
         // >>>>>>>>>>>> DATOS DE CONEXION <<<<<<<<<<<<<<<<<<<<<
        int tipoConexion = 1;
        if (radioPosTipoConexionTCP.isSelected()) {
            tipoConexion = 1;
        } else if (radioPosTipoConexionBridge.isSelected()) {
            tipoConexion = 0;
        }
        objTransactAPITarjetas.Configuracion().setPOSTipoCnx(tipoConexion);
        objTransactAPITarjetas.Configuracion().setPOSDireccionIP(textPosDireccionIP.getText());
        objTransactAPITarjetas.Configuracion().setPOSPuerto(Integer.parseInt(textPOSPuerto.getText()));
        objTransactAPITarjetas.Configuracion().setPOSSegsTimeout(Integer.parseInt(textPosTimeout.getText()));
        objTransactAPITarjetas.Configuracion().setURLConcentrador("http://wwwi.transact.com.uy/Concentrador");

        // >>>>>>>>>>>> DATOS DE IMPRESION <<<<<<<<<<<<<<<<<<<<<            
        objTransactAPITarjetas.Configuracion().setImpresionTipo(ObtenerTipoImpresion());
        objTransactAPITarjetas.Transaccion().setEmpCod(textEmpCod.getText());
        objTransactAPITarjetas.Transaccion().setTermCod(textTermCod.getText());

        //#############################################################################################
        //###################       DATOS TRANSACCION        ##########################################
        //#############################################################################################
        if (objTransactAPITarjetas.Transaccion().ReversarTransaccion(ObtenerTrnIdReversar())) {
            JOptionPane.showMessageDialog(this, "Reverso de transaccion procesado");
        } else {
            JOptionPane.showMessageDialog(this, "Reverso de transaccion no procesado\n" + objTransactAPITarjetas.Transaccion().getRespuesta().getMsgRespuesta());
        }
    }
     private void Reimprimir() {
        //#############################################################################################
        //###################       REGISTRA LISTENER PARA RECIBIR EVENTOS DEL API  ###################
        //#############################################################################################
        TransActAPI_Fabrica objFabrica = new TransActAPI_Fabrica();
        ITransActAPI_Tarjetas_v400 objTransactAPITarjetas = objFabrica.ObtenerTransactAPITarjetas(new IAPIListener() {
            @Override
            public void evtNotificadAvance(String mensaje) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### MUESTRA UNA VENTANA DE NOTIFICACION DE AVANCE###################");
                System.out.println("####   " + mensaje + "   ####");
                System.out.println("###################################################################################################");
            }

            @Override
            public void evtSolicitarCampoEnAPI(IMetadatosSolicitudCampo datosSolicitudCampo) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### INGRESA EL DATO ###################");
                System.out.println("####   " + datosSolicitudCampo.getWfCampoMensaje() + "   ####");
                System.out.println("###################################################################################################");
            }
        });

        //#############################################################################################
        //###################       CONFIGURACION      ################################################
        //#############################################################################################
        objTransactAPITarjetas.Configuracion().setModoEmulacion(jCheckBoxModoEmulacion.isSelected());
        objTransactAPITarjetas.Configuracion().setPOSTipo(0);
         //>>>>>>>>>>> DATOS GUI
        int GUITipo = 9;
        if(jCheckBoxUsarGUI.isSelected()){
            GUITipo = 0;
        }
        objTransactAPITarjetas.Configuracion().setGUITipo(GUITipo);
        // >>>>>>>>>>>> DATOS DE CONEXION <<<<<<<<<<<<<<<<<<<<<
        int tipoConexion = 1;
        if (radioPosTipoConexionTCP.isSelected()) {
            tipoConexion = 1;
        } else if (radioPosTipoConexionBridge.isSelected()) {
            tipoConexion = 0;
        }
        objTransactAPITarjetas.Configuracion().setPOSTipoCnx(tipoConexion);
        objTransactAPITarjetas.Configuracion().setPOSDireccionIP(textPosDireccionIP.getText());
        objTransactAPITarjetas.Configuracion().setPOSPuerto(Integer.parseInt(textPOSPuerto.getText()));
        objTransactAPITarjetas.Configuracion().setPOSSegsTimeout(Integer.parseInt(textPosTimeout.getText()));
        objTransactAPITarjetas.Configuracion().setURLConcentrador("http://wwwi.transact.com.uy/Concentrador");

        // >>>>>>>>>>>> DATOS DE IMPRESION <<<<<<<<<<<<<<<<<<<<<            
        objTransactAPITarjetas.Configuracion().setImpresionTipo(ObtenerTipoImpresion());
        objTransactAPITarjetas.Impresion().setEmpCod(textEmpCod.getText());
        objTransactAPITarjetas.Impresion().setTermCod(textTermCod.getText());

        //#############################################################################################
        //###################       DATOS TRANSACCION        ##########################################
        //#############################################################################################
        if (objTransactAPITarjetas.Impresion().ReImprimir()) {
            JOptionPane.showMessageDialog(this, "Reimpresion OK ");
        } else {
            JOptionPane.showMessageDialog(this, objTransactAPITarjetas.Impresion().getRespuesta().getMensaje());
        }

    }
      private void LeerTarjeta(){
         TransActAPI_Fabrica objFabrica = new TransActAPI_Fabrica();
        ITransActAPI_Tarjetas_v400 objTransactAPITarjetas = objFabrica.ObtenerTransactAPITarjetas(new IAPIListener() {
            @Override
            public void evtNotificadAvance(String mensaje) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### MUESTRA UNA VENTANA DE NOTIFICACION DE AVANCE###################");
                System.out.println("####   " + mensaje + "   ####");
                System.out.println("###################################################################################################");
            }

            @Override
            public void evtSolicitarCampoEnAPI(IMetadatosSolicitudCampo datosSolicitudCampo) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### INGRESA EL DATO ###################");
                System.out.println("####   " + datosSolicitudCampo.getWfCampoMensaje() + "   ####");
                System.out.println("###################################################################################################");
            }
        });

        //#############################################################################################
        //###################       CONFIGURACION      ################################################
        //#############################################################################################
        objTransactAPITarjetas.Configuracion().setModoEmulacion(jCheckBoxModoEmulacion.isSelected());
        objTransactAPITarjetas.Configuracion().setPOSTipo(0);
         //>>>>>>>>>>> DATOS GUI
        int GUITipo = 9;
        if(jCheckBoxUsarGUI.isSelected()){
            GUITipo = 0;
        }
        Boolean IngresaPropinaEnPos = jCheckBoxPropinaEnPos.isSelected();
        objTransactAPITarjetas.Configuracion().setGUITipo(GUITipo);
        objTransactAPITarjetas.Configuracion().setIngresaPropinaEnPos(IngresaPropinaEnPos);
        // >>>>>>>>>>>> DATOS DE CONEXION <<<<<<<<<<<<<<<<<<<<<
        int tipoConexion = 1;
        if (radioPosTipoConexionTCP.isSelected()) {
            tipoConexion = 1;
        } else if (radioPosTipoConexionBridge.isSelected()) {
            tipoConexion = 0;
        }
        objTransactAPITarjetas.Configuracion().setPOSTipoCnx(tipoConexion);
        objTransactAPITarjetas.Configuracion().setPOSDireccionIP(textPosDireccionIP.getText());
        objTransactAPITarjetas.Configuracion().setPOSPuerto(Integer.parseInt(textPOSPuerto.getText()));
        objTransactAPITarjetas.Configuracion().setPOSSegsTimeout(Integer.parseInt(textPosTimeout.getText()));
        objTransactAPITarjetas.Configuracion().setURLConcentrador("http://wwwi.transact.com.uy/Concentrador");

        // >>>>>>>>>>>> DATOS DE IMPRESION <<<<<<<<<<<<<<<<<<<<<            
        objTransactAPITarjetas.Configuracion().setImpresionTipo(ObtenerTipoImpresion());
        objTransactAPITarjetas.LectorTarjeta().setEmpCod(textEmpCod.getText());
        objTransactAPITarjetas.LectorTarjeta().setTermCod(textTermCod.getText());

        //#############################################################################################
        //###################       DATOS TRANSACCION        ##########################################
        //#############################################################################################
        if (objTransactAPITarjetas.LectorTarjeta().LeerTarjeta()) {
            String Msg= "";
            Msg+= "CodResp=" + objTransactAPITarjetas.LectorTarjeta().getRespuesta().getCodRespuesta() + "\n";
            Msg+= "MsgResp=" + objTransactAPITarjetas.LectorTarjeta().getRespuesta().getMsgRespuesta()+ "\n";
            Msg+= "getTrack1=" + objTransactAPITarjetas.LectorTarjeta().getRespuesta().getTrack1()+ "\n";
            Msg+= "Track2=" + objTransactAPITarjetas.LectorTarjeta().getRespuesta().getTrack2() + "\n";
            Msg+= "Track3=" + objTransactAPITarjetas.LectorTarjeta().getRespuesta().getTrack3() + "\n";
            JOptionPane.showMessageDialog(this, Msg);
        } else {
            JOptionPane.showMessageDialog(this, objTransactAPITarjetas.LectorTarjeta().getRespuesta().getMsgRespuesta());
        }
      }

////////////////UTILES 
    private String ObtenerOperacion() {
        String operacion = "VTA";

        if (jRadioButtonDevolucion.isSelected()) {
            operacion = "DEV";
        } else if (jRadioButtonDevolucionAutomatica.isSelected()) {
            operacion = "DVA";
        }

        return operacion;
    }

    private String ObtenerMonedaOperacion() {
        String moneda = "0858";

        if (jRadioButtonMonedaUSD.isSelected()) {
            moneda = "0840";
        }

        return moneda;
    }

    private Long ObtenerMontoOperacion() {
        Double montoOperacion = new Double(100);

        if (jRadioButtonDevolucionAutomatica.isSelected()) {
            if (jTextFieldMontoDevolucionAutomatica.getText().length() > 0) {
                montoOperacion = Double.parseDouble(jTextFieldMontoDevolucionAutomatica.getText()) * 100;
            }
        } else {
            if (jTextFieldMonto.getText().length() > 0) {
                montoOperacion = Double.parseDouble(jTextFieldMonto.getText()) * 100;
            }
        }
        return montoOperacion.longValue();
    }

    private Long ObtenerPropinaOperacion() {
        Double propinaOperacion = new Double(0);
        if (jTextFieldPropina.getText().length() > 0) {
            propinaOperacion = Double.parseDouble(jTextFieldPropina.getText()) * 100;
        }else{
            propinaOperacion = -1.0;
        }
        return propinaOperacion.longValue();
    }

    private Integer ObtenerCuotasOperacion() {
        Integer cuotasOperacion = 0;

        if (jTextFieldPropina.getText().length() > 0) {
            cuotasOperacion = Integer.parseInt(jTextFieldCuotas.getText());
        }
        return cuotasOperacion;
    }

    private Integer ObtenerDecretoLey() {
        Integer decretoLeyOperacion = 0;

        if (jTextFieldIdDecretoLey.getText().length() > 0) {
            decretoLeyOperacion = Integer.parseInt(jTextFieldIdDecretoLey.getText());
        }
        return decretoLeyOperacion;
    }

    private Long ObtenerFacturaNro() {
        Long FacturaNroOperacion = new Long(0);
        if (jTextFieldNroFactura.getText().length() > 0) {
            FacturaNroOperacion = Long.parseLong(jTextFieldNroFactura.getText());
        }
        return FacturaNroOperacion;
    }
    
    private Long ObtenerTrnIdReversar() {
        Long TrnIdReversar = new Long(0);
        if (jTextFieldTrnIdReversar.getText().length() > 0) {
            TrnIdReversar = Long.parseLong(jTextFieldTrnIdReversar.getText());
        }
        return TrnIdReversar;
    }

    private Long ObtenerFacturaMontoGravado() {
        Double facturaMontoGravadoOperacion = new Double(0);
        if (jTextFieldMontoGravado.getText().length() > 0) {
            facturaMontoGravadoOperacion = Double.parseDouble(jTextFieldMontoGravado.getText()) * 100;
        }
        return facturaMontoGravadoOperacion.longValue();
    }

    private Long ObtenerFacturaMontoIVA() {
        Double facturaMontoIVAOperacion = new Double(0);
        if (jTextFieldMontoIVA.getText().length() > 0) {
            facturaMontoIVAOperacion = Double.parseDouble(jTextFieldMontoIVA.getText()) * 100;
        }
        return facturaMontoIVAOperacion.longValue();
    }

    private Integer ObtenerTicketOriginal() {
        Integer ticketOriginalOperacion = 0;
        if (jTextFieldTicketOriginal.getText().length() > 0) {
            ticketOriginalOperacion = Integer.parseInt(jTextFieldTicketOriginal.getText());
        }
        return ticketOriginalOperacion;
    }

    private Long ObtenerFacturaMonto() {
        Double facturaMontoOperacion = new Double(0);

        if (jTextFieldMontoFactura.getText().length() > 0) {
            facturaMontoOperacion = Double.parseDouble(jTextFieldMontoFactura.getText()) * 100;
        }
        return facturaMontoOperacion.longValue();
    }

    private Integer ObtenerTipoImpresion() {
        Integer tipoImpresionOperacion = 9;

        if (jRadioButtonImprimirPOS.isSelected()) {
            tipoImpresionOperacion = 0;
        } else if (jRadioButtonImprimirERP.isSelected()) {
            tipoImpresionOperacion = 1;
        }
        return tipoImpresionOperacion;
    }

    private Integer ObtenerTarjetaTipo() {
        Integer tarjetaTipoOperacion = 0;

        tarjetaTipoOperacion = Integer.parseInt(jTextFieldAdquirenteId.getText());
        return tarjetaTipoOperacion;
    }

    
    
    
    private Void EstresarAplicacionUX(){
       int Iteraciones = 0;
       int maximoOperacionesReinicar = Integer.parseInt(jTextFieldmaximoOperacionesReiniciar.getText());
       int totalIteraciones = Integer.parseInt(jTextFieldEstresTotalOperaciones.getText());
       int ContadorPorReinicio = 0; 
        while(Iteraciones < totalIteraciones){    
            Iteraciones++;
            ContadorPorReinicio++;
            if(salirHilo){
             break;
            }
           
            jLabelEstres.setText("Total Iteraciones (" + Iteraciones + ")" );
              //#############################################################################################
        //###################       REGISTRA LISTENER PARA RECIBIR EVENTOS DEL API  ###################
        //#############################################################################################
            TransActAPI_Fabrica objFabrica = new TransActAPI_Fabrica();
        ITransActAPI_Tarjetas_v400 objTransactAPITarjetas = objFabrica.ObtenerTransactAPITarjetas(new IAPIListener() {
            @Override
            public void evtNotificadAvance(String mensaje) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### MUESTRA UNA VENTANA DE NOTIFICACION DE AVANCE###################");
                System.out.println("####   " + mensaje + "   ####");
                System.out.println("###################################################################################################");
            }

            @Override
            public void evtSolicitarCampoEnAPI(IMetadatosSolicitudCampo datosSolicitudCampo) {
                System.out.println("###################################################################################################");
                System.out.println("#################################### INGRESA EL DATO ###################");
                System.out.println("####   " + datosSolicitudCampo.getWfCampoMensaje() + "   ####");
                System.out.println("###################################################################################################");
            }
        });

        //#############################################################################################
        //###################       CONFIGURACION      ################################################
        //#############################################################################################
       
        objTransactAPITarjetas.Configuracion().setModoEmulacion(jCheckBoxModoEmulacion.isSelected());
        objTransactAPITarjetas.Configuracion().setPOSTipo(0);
        //>>>>>>>>>>> DATOS GUI
        int GUITipo = 9;
        if(jCheckBoxUsarGUI.isSelected()){
            GUITipo = 0;
        }
        objTransactAPITarjetas.Configuracion().setGUITipo(GUITipo);
         // >>>>>>>>>>>> DATOS DE CONEXION <<<<<<<<<<<<<<<<<<<<<
        int tipoConexion = 1;
        if (radioPosTipoConexionTCP.isSelected()) {
            tipoConexion = 1;
        } else if (radioPosTipoConexionBridge.isSelected()) {
            tipoConexion = 0;
        }
        objTransactAPITarjetas.Configuracion().setPOSTipoCnx(tipoConexion);
        objTransactAPITarjetas.Configuracion().setPOSDireccionIP(textPosDireccionIP.getText());
        objTransactAPITarjetas.Configuracion().setPOSPuerto(Integer.parseInt(textPOSPuerto.getText()));
        objTransactAPITarjetas.Configuracion().setPOSSegsTimeout(Integer.parseInt(textPosTimeout.getText()));
        objTransactAPITarjetas.Configuracion().setURLConcentrador("http://wwwi.transact.com.uy/Concentrador");

        // >>>>>>>>>>>> DATOS DE IMPRESION <<<<<<<<<<<<<<<<<<<<<            
        objTransactAPITarjetas.Configuracion().setImpresionTipo(ObtenerTipoImpresion());

        //#############################################################################################
        //###################       DATOS TRANSACCION        ##########################################
        //#############################################################################################
        objTransactAPITarjetas.Transaccion().NuevaTransaccion();

        objTransactAPITarjetas.Transaccion().setEmpCod(textEmpCod.getText());
        objTransactAPITarjetas.Transaccion().setTermCod(textTermCod.getText());
        objTransactAPITarjetas.Transaccion().setOperacion(ObtenerOperacion());
        objTransactAPITarjetas.Transaccion().setMonedaISO(ObtenerMonedaOperacion());
        objTransactAPITarjetas.Transaccion().setMonto(ObtenerMontoOperacion());
        objTransactAPITarjetas.Transaccion().setMontoPropina(ObtenerPropinaOperacion());
        objTransactAPITarjetas.Transaccion().setMontoCashBack(new Long(0));

        objTransactAPITarjetas.Transaccion().setFacturaNro(ObtenerFacturaNro());
        objTransactAPITarjetas.Transaccion().setFacturaMontoGravado(ObtenerFacturaMontoGravado());
        objTransactAPITarjetas.Transaccion().setFacturaMontoIVA(ObtenerFacturaMontoIVA());
        objTransactAPITarjetas.Transaccion().setTicketOriginal(ObtenerTicketOriginal());
        objTransactAPITarjetas.Transaccion().setFacturaMonto(ObtenerFacturaMonto());
         
          objTransactAPITarjetas.Transaccion().setEmpCod(textEmpCod.getText());
        objTransactAPITarjetas.Transaccion().setTermCod(textTermCod.getText());
        objTransactAPITarjetas.Transaccion().setOperacion(ObtenerOperacion());
        objTransactAPITarjetas.Transaccion().setMonedaISO(ObtenerMonedaOperacion());
        objTransactAPITarjetas.Transaccion().setMonto(ObtenerMontoOperacion());
        
        if (jTextFieldPropina.getText().length() > 0){
             objTransactAPITarjetas.Transaccion().setMontoPropina(ObtenerPropinaOperacion());
        }
        if (jTextFieldCashback.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setMontoCashBack(new Long(0));
        }
        if (jTextFieldNroFactura.getText().length() > 0){
           objTransactAPITarjetas.Transaccion().setFacturaNro(ObtenerFacturaNro()); 
        }
        if (jTextFieldMontoGravado.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setFacturaMontoGravado(ObtenerFacturaMontoGravado());
        }
        if (jTextFieldMontoIVA.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setFacturaMontoIVA(ObtenerFacturaMontoIVA());
        }
        if (jTextFieldTicketOriginal.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setTicketOriginal(ObtenerTicketOriginal());
        }
        if (jTextFieldMontoFactura.getText().length() > 0){
            objTransactAPITarjetas.Transaccion().setFacturaMonto(ObtenerFacturaMonto());
        }
       
        //#############################################################################################
        //###################       TRANSACCION EXTENDIDA       #######################################
        //#############################################################################################
        objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setCuotas(ObtenerCuotasOperacion());
        if (jCheckBoxConsumidorFinal.isSelected()) {
            objTransactAPITarjetas.Transaccion().setFacturaConsumidorFinal(true);
        } else {
            objTransactAPITarjetas.Transaccion().setFacturaConsumidorFinal(false);
        }

        objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setDecretoLeyId(ObtenerDecretoLey());
        if(!jTextFieldPlanId.getText().isEmpty()){
             Integer planID = Integer.parseInt(jTextFieldPlanId.getText());
            if(planID > 0){
                objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setPlanId(planID);
            }
        }
        if(!jTextFieldPlanVtaId.getText().isEmpty()){
            Integer planvtaID = Integer.parseInt(jTextFieldPlanVtaId.getText());
            if(planvtaID > 0){
                objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setPlanVtaId(planvtaID);
            }
        }

        //#############################################################################################
        //###################       CASO DEVOLUCION AUTOMATICA, ESPECIFICAR ADQID       ###############
        //#############################################################################################
        if (jRadioButtonDevolucionAutomatica.isSelected()) {
            objTransactAPITarjetas.Transaccion().setTarjetaId(ObtenerTarjetaTipo());
        }

        //#############################################################################################
        //###################       TRANSACCION EXTENDIDA       #######################################
        //#############################################################################################
        objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setCuotas(ObtenerCuotasOperacion());
        if (jCheckBoxConsumidorFinal.isSelected()) {
            objTransactAPITarjetas.Transaccion().setFacturaConsumidorFinal(true);
        } else {
            objTransactAPITarjetas.Transaccion().setFacturaConsumidorFinal(false);
        }

        objTransactAPITarjetas.Transaccion().getTransaccionExtendida().setDecretoLeyId(ObtenerDecretoLey());
         try
        {
             if(maximoOperacionesReinicar > 0){
             if(ContadorPorReinicio > maximoOperacionesReinicar){
              objTransactAPITarjetas.Transaccion().DispositivoReiniciar();
              ContadorPorReinicio = 0;
              Thread.sleep(180 *1000);
              continue;
             }
            }
        }catch (Exception ex) {
            Logger.getLogger(IntegracionExtendida.class.getName()).log(Level.SEVERE, null, ex);
        }

        //#############################################################################################
        //###################       PROCESAR TRANSACCION         #######################################
        //#############################################################################################
        objTransactAPITarjetas.Transaccion().ProcesarTransaccion();

        //Verifico Respuesta del Autorizador
        if (!objTransactAPITarjetas.Transaccion().getRespuesta().getAprobada()) {
            //#############################################################################################
            //###################       SE SOLICITA EN CASO DE DEVOLUCION AUTOMATICA EN DEPENDENCIA DEL 
            //###################       CODIGO DE SALIDA SI SE DESEA APLICAR DEVOLUCION MANUAL O NO    ####
            //#############################################################################################
            if (ObtenerOperacion().equals("DVA")) {
                if (objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().compareTo(DevolucionAutomaticaRespuesta.enumDevolucionAutoCodigo.DEV_AUTO_POR_DEVOLUCION) == 0) {
                    if (JOptionPane.showConfirmDialog(this, "Se solicita realizar la devolucion manual, desea efectuarla?", "Confirmar", JOptionPane.OK_CANCEL_OPTION) == 0) {
                        try {
                            jRadioButtonDevolucion.setSelected(true);
                            jRadioButtonDevolucionAutomatica.setSelected(false);
                            ProcesarTransaccion();
                        } catch (Exception ex) {
                            Logger.getLogger(IntegracionExtendida.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "DEV AUTO - Respuesta: \n" + objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().toString() + " - " + objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoMensaje());
                    }
                } else if (objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().compareTo(DevolucionAutomaticaRespuesta.enumDevolucionAutoCodigo.DEV_AUTO_POR_CARTA) == 0) {
                    JOptionPane.showMessageDialog(this, "Debe solicitar la devolución por carta de la operación.");
                } else if (objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().compareTo(DevolucionAutomaticaRespuesta.enumDevolucionAutoCodigo.DEV_AUTO_ERROR) == 0) {
                    JOptionPane.showMessageDialog(this, "Error en la operación.");
                } else if (objTransactAPITarjetas.Transaccion().getRespuesta().getDevolucionAutomaticaRespuesta().getDevolucionAutoCodigo().compareTo(DevolucionAutomaticaRespuesta.enumDevolucionAutoCodigo.DEV_AUTO_OK) == 0) {
                    JOptionPane.showMessageDialog(this, "Operación exitosa.");
                }
               
                continue;
            } else {
                AgregarTraza( "Denegada-Respuesta: " + objTransactAPITarjetas.Transaccion().getRespuesta().getCodRespuesta()+ " - " + objTransactAPITarjetas.Transaccion().getRespuesta().getMsgRespuesta());
                //JOptionPane.showMessageDialog(this, "Denegada - Respuesta: \n" + objTransactAPITarjetas.Transaccion().getRespuesta().getCodRespuesta() + " - " + objTransactAPITarjetas.Transaccion().getRespuesta().getMsgRespuesta());
                continue;
            }
        }

        //#############################################################################################
        //###################       ACÁ SE DEBE PERSISTIR LOS DATOS DEL PAGO (TRNID,TICKET,LOTE Y NROAUTORIZACION) 
        //###################       EN EL SOFTWARE DE FACTURACIÓN COMO PENDIENTES DE CONFIRMACIÓN  ####
        //#############################################################################################
        Long TrnId = objTransactAPITarjetas.Transaccion().getRespuesta().getTransaccionId();
        Long ticket = objTransactAPITarjetas.Transaccion().getRespuesta().getTicket();
        Long lote = objTransactAPITarjetas.Transaccion().getRespuesta().getLote();
        String NroAutorizacion = objTransactAPITarjetas.Transaccion().getRespuesta().getNroAutorizacion();

        //************************************************************************************************************
        //***GUARDO EL TRNID PARA POSIBLE REVERSO
        //************************************************************************************************************
        jTextFieldTrnIdReversar.setText(objTransactAPITarjetas.Transaccion().getRespuesta().getTransaccionId().toString());

        //#############################################################################################
        //###################       CONFIRMACION DE LA TRANSACCION#####################################
        //#############################################################################################
        Boolean ConfirmadaOK = false;

        if (jCheckBoxSolicitarConfirmarTrn.isSelected()) {
            if (JOptionPane.showConfirmDialog(this, "Desea confirmar la transacción?", "Confirmar", JOptionPane.OK_CANCEL_OPTION) == 0) {
                ConfirmadaOK = objTransactAPITarjetas.Transaccion().ConfirmarTransaccion(TrnId);
            }
        } else {
           
            ConfirmadaOK = objTransactAPITarjetas.Transaccion().ConfirmarTransaccion(TrnId);
        }

        if (!ConfirmadaOK) {
            //****************************************************************************************************
            //*** ACÁ SE DEBE BORRAR DEL SOFTWARE DE FACTURACIÓN EL PAGO QUE ESTABA PENDIENTE DE CONFIRMACIÓN  ***
            //***NOTA: LA AUTORIZACIÓN CUYA CONFIRMACIÓN FALLÓ, SE REVERSARÁ AL REALIZAR LA PROXIMA TRANSACCIÓN***
            //****************************************************************************************************
            JOptionPane.showMessageDialog(this, "Error Confirmando Transacción: \n"
                    + objTransactAPITarjetas.Transaccion().getRespuesta().getCodRespuesta()
                    + " - " + objTransactAPITarjetas.Transaccion().getRespuesta().getMsgRespuesta());
        } else {

            //************************************************************************************************************
            //***ACÁ SE DEBE MARCAR EL PAGO EN EL SOFTWARE DE FACTURACIÓN COMO CONFIRMADO***
            //***NOTA: EN EL CASO EN QUE LA IMPRESIÓN LA HACE EL SOFTWARE DE FACTURACIÓN, AQUÍ SE PROCEDERA A IMPRIMIR EL VOUCHER 
            //************************************************************************************************************
            System.out.println("Aprobada" + objTransactAPITarjetas.Transaccion().getRespuesta().getAprobada());
            System.out.println("TarjetaID" + objTransactAPITarjetas.Transaccion().getRespuesta().getTarjetaId());
            System.out.println("Nro Autorizacion" + objTransactAPITarjetas.Transaccion().getRespuesta().getNroAutorizacion());

            //************************************************************************************************************
            //***ACA GUARDO EL ADQID Y NRO FACTURA DE LA ULTIMA OPERACION PARA POSIBLE DEVOLUCION AUTOMATICA
            //************************************************************************************************************
            jTextFieldAdquirenteId.setText(objTransactAPITarjetas.Transaccion().getRespuesta().getTarjetaId().toString());
            jTextFieldTicketOriginal.setText(objTransactAPITarjetas.Transaccion().getRespuesta().getDatosTransaccion().getDatosTransaccionExtendida().getFacturaNro().toString());

            //JOptionPane.showMessageDialog(this, "Aprobada - Nro. Autorización: " + objTransactAPITarjetas.Transaccion().getRespuesta().getNroAutorizacion());
        try {
            Thread.sleep(Integer.parseInt(jTextFieldDelayTransacciones.getText())*1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(IntegracionExtendida.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        
        
        }
    
        
     return null;   
    }

    void AgregarTraza(String linea){
    BufferedWriter bw = null;
    FileWriter fw = null;

    try {
        String data = linea + "\n";
        File file = new File("LOG.txt");
        // Si el archivo no existe, se crea!
        if (!file.exists()) {
            file.createNewFile();
        }
        // flag true, indica adjuntar información al archivo.
        fw = new FileWriter(file.getAbsoluteFile(), true);
        bw = new BufferedWriter(fw);
        bw.write(data);
        System.out.println("información agregada!");
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        try {
                        //Cierra instancias de FileWriter y BufferedWriter
            if (bw != null)
                bw.close();
            if (fw != null)
                fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    }
}
