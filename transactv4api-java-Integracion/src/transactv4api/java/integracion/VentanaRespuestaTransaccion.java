/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transactv4api.java.integracion;

import com.uy.nad.transact4.api.ICierreLoteRespuesta;
import com.uy.nad.transact4.api.IDatosCierre;
import com.uy.nad.transact4.api.IDatosProducto;
import com.uy.nad.transact4.api.ITransaccionRespuesta;

/**
 *
 * @author Yunior
 */
public class VentanaRespuestaTransaccion extends javax.swing.JDialog {

    /**
     * Creates new form VentanaRespuestaTransaccion
     */
    private int tipoRespuesta = 0;
    ITransaccionRespuesta objRespTransaccion;
    ICierreLoteRespuesta objRespCierre;
    public VentanaRespuestaTransaccion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }
    void SetTransaccionRespuesta(ITransaccionRespuesta resp){
        tipoRespuesta = 1;
        jTextResultado.setText("");
        String ficha = "";
        ficha = ficha.concat("getCodRespuesta        ->" + resp.getCodRespuesta() + "\n");
        ficha = ficha.concat("getCodRespuestaComando ->" + resp.getCodRespuestaComando()+ "\n");
        ficha = ficha.concat("getMsgRespuesta        ->" + resp.getMsgRespuesta()+ "\n");
        ficha = ficha.concat("getNroAutorizacion     ->" + resp.getNroAutorizacion()+ "\n");
        ficha = ficha.concat("getTarjetaTipo         ->" + resp.getTarjetaTipo()+ "\n");
        ficha = ficha.concat("getAprobada            ->" + resp.getAprobada()+ "\n");
        ficha = ficha.concat("getEsOffline           ->" + resp.getEsOffline()+ "\n");
        ficha = ficha.concat("getLote                ->" + resp.getLote()+ "\n");
        ficha = ficha.concat("getTarjetaId           ->" + resp.getTarjetaId()+ "\n");
        ficha = ficha.concat("getTicket              ->" + resp.getTicket()+ "\n");
        ficha = ficha.concat("getTransaccionId       ->" + resp.getTransaccionId()+ "\n");
        ficha = ficha.concat("getVoucher             ->" + resp.getVoucher()+ "\n");
        if(resp.getDatosTransaccion()!=null){
        ficha = ficha.concat("getDecretoLeyNro       ->" + resp.getDatosTransaccion().getDecretoLeyNro()+ "\n");
        ficha = ficha.concat("getMonedaISO           ->" + resp.getDatosTransaccion().getMonedaISO() + "\n");
        ficha = ficha.concat("getOperacion           ->" + resp.getDatosTransaccion().getOperacion()+ "\n");
        ficha = ficha.concat("getTarjetaIIN          ->" + resp.getDatosTransaccion().getTarjetaIIN() + "\n");
        ficha = ficha.concat("getTarjetaNro          ->" + resp.getDatosTransaccion().getTarjetaNro() + "\n");
        ficha = ficha.concat("getCuotas              ->" + resp.getDatosTransaccion().getCuotas() + "\n");
        ficha = ficha.concat("getDecretoLeyAplicado  ->" + resp.getDatosTransaccion().getDecretoLeyAplicado()+ "\n");
        ficha = ficha.concat("getDecretoLeyMonto     ->" + resp.getDatosTransaccion().getDecretoLeyMonto()+ "\n");
        ficha = ficha.concat("getEmisorId            ->" + resp.getDatosTransaccion().getEmisorId() + "\n");
        ficha = ficha.concat("getMonto               ->" + resp.getDatosTransaccion().getMonto() + "\n");
        ficha = ficha.concat("getMontoCashBack       ->" + resp.getDatosTransaccion().getMontoCashBack() + "\n");
        ficha = ficha.concat("getMontoPropina        ->" + resp.getDatosTransaccion().getMontoPropina() + "\n");
        ficha = ficha.concat("getTarjetaAlimentacion ->" + resp.getDatosTransaccion().getTarjetaAlimentacion()+ "\n");
        ficha = ficha.concat("getTarjetaExtranjera   ->" + resp.getDatosTransaccion().getTarjetaExtranjera() + "\n");
        ficha = ficha.concat("getTarjetaPrestaciones ->" + resp.getDatosTransaccion().getTarjetaPrestaciones() + "\n");
        
         if( resp.getDatosTransaccion().getDatosTransaccionExtendida()!=null){
        ficha = ficha.concat("getCuentaNro           ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getCuentaNro()+ "\n");
        ficha = ficha.concat("getDecretoLeyAdqId     ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getDecretoLeyAdqId()+ "\n");
        ficha = ficha.concat("getDecretoLeyNom       ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getDecretoLeyNom()+ "\n");
        ficha = ficha.concat("getDecretoLeyVoucher   ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getDecretoLeyVoucher()+ "\n");
        ficha = ficha.concat("getEmisorNombre        ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getEmisorNombre()+ "\n");
        ficha = ficha.concat("getEmpresaNombre       ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getEmpresaNombre()+ "\n");
        ficha = ficha.concat("getEmpresaRUT          ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getEmpresaRUT()+ "\n");
        ficha = ficha.concat("getEmvAppId            ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getEmvAppId()+ "\n");
        ficha = ficha.concat("getEmvAppName          ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getEmvAppName()+ "\n");
        ficha = ficha.concat("getMerchantID          ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getMerchantID()+ "\n");
        ficha = ficha.concat("getPlanNombre          ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getPlanNombre()+ "\n");
        ficha = ficha.concat("getSucursalDireccion   ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getSucursalDireccion()+ "\n");
        ficha = ficha.concat("getSucursalNombre      ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getSucursalNombre()+ "\n");
        ficha = ficha.concat("getTarjetaDocIdentidad ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTarjetaDocIdentidad()+ "\n");
        ficha = ficha.concat("getTarjetaMedio        ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTarjetaMedio() + "\n");
        ficha = ficha.concat("getTarjetaNombre       ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTarjetaNombre() + "\n");
        ficha = ficha.concat("getTarjetaTitular      ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTarjetaTitular() + "\n");
        ficha = ficha.concat("getTarjetaVencimiento  ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTarjetaVencimiento() + "\n");
        ficha = ficha.concat("getTerminalID          ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTerminalID() + "\n");
        ficha = ficha.concat("getTextoAdicional      ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTextoAdicional() + "\n");
        ficha = ficha.concat("getTipoCuentaNombre    ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTipoCuentaNombre() + "\n");
        ficha = ficha.concat("getDecretoLeyId        ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getDecretoLeyId()+ "\n");
        ficha = ficha.concat("getFacturaMonto        ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getFacturaMonto()+ "\n");
        ficha = ficha.concat("getFacturaMontoGravado ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getFacturaMontoGravado() + "\n");
        ficha = ficha.concat("getFacturaMonGravadoTrn->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getFacturaMontoGravadoTrn()+ "\n");
        ficha = ficha.concat("getFacturaMontoIVA     ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getFacturaMontoIVA()+ "\n");
        ficha = ficha.concat("getFacturaMontoIVATrn  ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getFacturaMontoIVATrn()+ "\n");
        ficha = ficha.concat("getFacturaNro          ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getFacturaNro()+ "\n");
        ficha = ficha.concat("getFirmarVoucher       ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getFirmarVoucher()+ "\n");
        ficha = ficha.concat("getPlanId              ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getPlanId()+ "\n");
        ficha = ficha.concat("getPlanNroPlan         ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getPlanNroPlan()+ "\n");
        ficha = ficha.concat("getPlanNroTipoPlan     ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getPlanNroTipoPlan()+ "\n");
        ficha = ficha.concat("getTipoCuentaId        ->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTipoCuentaId()+ "\n");
        ficha = ficha.concat("getTransaccionFechaHora->" + resp.getDatosTransaccion().getDatosTransaccionExtendida().getTransaccionFechaHora()+ "\n");
        }
        }
        jTextResultado.setText(ficha);
    }
     void SetCierreRespuesta(ICierreLoteRespuesta resp){
     tipoRespuesta = 2;
     jTextResultado.setText("");
      String ficha = "";
      ficha = ficha.concat("getEstado                ->" + resp.getEstado()+ "\n");
      ficha = ficha.concat("getTokenCierre           ->" + resp.getTokenCierre()+ "\n");
      ficha = ficha.concat("getFinalizado            ->" + resp.getFinalizado()+ "\n");
      for(IDatosCierre dat:resp.getDatosCierre()){
         ficha = ficha.concat("###################################################################\n");
         ficha = ficha.concat("getCodRespuesta          ->" +dat.getCodRespuesta()+ "\n");
         ficha = ficha.concat("getMsgRespuesta          ->" +dat.getMsgRespuesta()+ "\n");
         ficha = ficha.concat("getNroAutorizacion       ->" +dat.getNroAutorizacion()+ "\n");
         ficha = ficha.concat("getAprobado              ->" +dat.getAprobado()+ "\n");
         ficha = ficha.concat("getLote                  ->" +dat.getLote()+ "\n");
         ficha = ficha.concat("getProcesadorId          ->" +dat.getProcesadorId()+ "\n");
         
         ficha = ficha.concat("getEmpresaNombre         ->" +dat.getExtendida().getEmpresaNombre()+ "\n");
         ficha = ficha.concat("getEmpresaRUT            ->" +dat.getExtendida().getEmpresaRUT()+ "\n");
         ficha = ficha.concat("getMerchantID            ->" +dat.getExtendida().getMerchantID()+ "\n");
         ficha = ficha.concat("getSucursalDireccion     ->" +dat.getExtendida().getSucursalDireccion()+ "\n");
         ficha = ficha.concat("getSucursalNombre        ->" +dat.getExtendida().getSucursalNombre()+ "\n");
         ficha = ficha.concat("getTerminalID            ->" +dat.getExtendida().getTerminalID()+ "\n");
         ficha = ficha.concat("getCantAnulacion         ->" +dat.getExtendida().getCantAnulacion()+ "\n");
         ficha = ficha.concat("getCantDevolucion        ->" +dat.getExtendida().getCantDevolucion()+ "\n");
         ficha = ficha.concat("getCantReversoAnulacion  ->" +dat.getExtendida().getCantReversoAnulacion()+ "\n");
         ficha = ficha.concat("getCantReversoDevolucion ->" +dat.getExtendida().getCantReversoDevolucion()+ "\n");
         ficha = ficha.concat("getCantReversoVenta      ->" +dat.getExtendida().getCantReversoVenta()+ "\n");
         ficha = ficha.concat("getCantVenta             ->" +dat.getExtendida().getCantVenta()+ "\n");
         ficha = ficha.concat("getCierreFechaHora       ->" +dat.getExtendida().getCierreFechaHora()+ "\n");
         ficha = ficha.concat("getMontoAnulacion        ->" +dat.getExtendida().getMontoAnulacion()+ "\n");
         ficha = ficha.concat("getMontoDevolucion       ->" +dat.getExtendida().getMontoDevolucion()+ "\n");
         ficha = ficha.concat("getMontoReversoAnulacion ->" +dat.getExtendida().getMontoReversoAnulacion()+ "\n");
         ficha = ficha.concat("getMontoReversoDevolucion ->" +dat.getExtendida().getMontoReversoDevolucion() + "\n");
         ficha = ficha.concat("getMontoReversoVenta      ->" +dat.getExtendida().getMontoReversoVenta()+ "\n");
         ficha = ficha.concat("getMontoVenta             ->" +dat.getExtendida().getMontoVenta()+ "\n");
         for(IDatosProducto datProd:dat.getExtendida().getProductos()){
         ficha = ficha.concat("getProductoNombre         ->" +datProd.getProductoNombre() + "\n");
         ficha = ficha.concat("getTarjetaNombre          ->" +datProd.getTarjetaNombre()+ "\n");
         ficha = ficha.concat("getTarjetaTipo            ->" +datProd.getTarjetaTipo()+ "\n");
         ficha = ficha.concat("getProductoId             ->" +datProd.getProductoId()+ "\n");
         ficha = ficha.concat("getTarjetaAlimentacion    ->" +datProd.getTarjetaAlimentacion()+ "\n");
         ficha = ficha.concat("getTarjetaId              ->" +datProd.getTarjetaId()+ "\n");
         ficha = ficha.concat("getTarjetaPrestaciones    ->" +datProd.getTarjetaPrestaciones()+ "\n");
         }
      }
    
      
      
      
     jTextResultado.setText(ficha);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextResultado = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jTextResultado.setColumns(20);
        jTextResultado.setLineWrap(true);
        jTextResultado.setRows(5);
        jScrollPane1.setViewportView(jTextResultado);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 609, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(83, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 708, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaRespuestaTransaccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaRespuestaTransaccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaRespuestaTransaccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaRespuestaTransaccion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                VentanaRespuestaTransaccion dialog = new VentanaRespuestaTransaccion(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextResultado;
    // End of variables declaration//GEN-END:variables
}
