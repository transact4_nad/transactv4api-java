/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface IDatosProductoMonedaPlanDecretosVentas {

    public Integer getCantVenta();

    public Long getMontoVenta();

    public Long getMontoPropinaVenta();

    public Long getMontoCashbackVenta();

    public Long getMontoDecretoLeyVenta();

    public Integer getCantReversoVenta();

    public Long getMontoReversoVenta();

    public Long getMontoPropinaReversoVenta();

    public Long getMontoCashbackReversoVenta();

    public Long getMontoDecretoLeyReversoVenta();
}
