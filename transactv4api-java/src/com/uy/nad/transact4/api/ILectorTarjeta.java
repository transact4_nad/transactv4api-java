/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public interface ILectorTarjeta {

    void setEmpCod(String EmpCod);

    void setTermCod(String TermCod);

    void setMultiEmp(Integer MultiEmp);

    ILecturaTarjetaRespuesta getRespuesta();

    Boolean LeerTarjeta();

    void CancelarOperacion();
}
