/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

import java.util.List;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public interface ICierreLoteRespuesta {

    public String getTokenCierre();

    public Boolean getFinalizado();

    public String getEstado();

    public List<String> getVoucher();

    public List<IDatosCierre> getDatosCierre();
}
