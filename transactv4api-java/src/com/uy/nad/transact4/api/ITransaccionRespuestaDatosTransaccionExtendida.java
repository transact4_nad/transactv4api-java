/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

import java.util.Date;

/**
 *
 * @author yunior
 */
public interface ITransaccionRespuestaDatosTransaccionExtendida {

    public Date getTransaccionFechaHora();

    public String getEmpresaRUT();

    public String getEmpresaNombre();

    public String getSucursalNombre();

    public String getSucursalDireccion();

    public String getMerchantID();

    public String getTerminalID();

    public String getEmisorNombre();

    public String getTarjetaMedio();

    public String getTarjetaNombre();

    public String getTarjetaTitular();

    public String getTarjetaVencimiento();

    public String getTarjetaDocIdentidad();

    public Long getFacturaNro();

    public Long getFacturaMonto();

    public Long getFacturaMontoGravado();

    public Long getFacturaMontoGravadoTrn();

    public Long getFacturaMontoIVA();

    public Long getFacturaMontoIVATrn();

    public Integer getDecretoLeyId();

    public String getDecretoLeyNom();

    public String getDecretoLeyAdqId();

    public String getDecretoLeyVoucher();

    public Integer getPlanId();
    
    public Integer getPlanVtaId();

    public String getPlanNombre();

    public Integer getPlanNroPlan();

    public Integer getPlanNroTipoPlan();

    public Integer getTipoCuentaId();

    public String getTipoCuentaNombre();

    public String getCuentaNro();

    public String getEmvAppId();

    public String getEmvAppName();

    public Boolean getFirmarVoucher();

    public String getTextoAdicional();
}
