/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

import java.util.List;

/**
 *
 * @author yunior
 */
public interface ITransaccionRespuesta {

    public Long getTransaccionId();

    public Boolean getAprobada();

    public String getNroAutorizacion();

    public String getCodRespuesta();

    public String getMsgRespuesta();

    public Long getLote();

    public Long getTicket();

    public Integer getTarjetaId();

    public String getTarjetaTipo();

    public Boolean getEsOffline();

    public List<String> getVoucher();
    
     public ITransaccionRespuestaDatosTransaccion getDatosTransaccion();
     
    public String getCodRespuestaComando();

    public String getMsgRespuestaComando();
    
    public DevolucionAutomaticaRespuesta getDevolucionAutomaticaRespuesta();
}
