/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

import com.uy.nad.transact4.api.impl.TransActAPI_Tarjetas_v400Impl;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class TransActAPI_Fabrica {
   public ITransActAPI_Tarjetas_v400 ObtenerTransactAPITarjetas(IAPIListener listener){
        return new TransActAPI_Tarjetas_v400Impl(listener);
    }
}
