/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface ITransaccionRespuestaDatosTransaccion {

    public String getOperacion();

    public String getMonedaISO();

    public Long getMonto();

    public Long getMontoPropina();

    public Long getMontoCashBack();

    public Integer getCuotas();

    public String getTarjetaNro();

    public String getTarjetaIIN();

    public Boolean getTarjetaExtranjera();

    public Boolean getTarjetaPrestaciones();

    public Boolean getTarjetaAlimentacion();

    public Integer getEmisorId();

    public Boolean getDecretoLeyAplicado();

    public String getDecretoLeyNro();

    public Long getDecretoLeyMonto();
    
    public ITransaccionRespuestaDatosTransaccionExtendida getDatosTransaccionExtendida();
}
