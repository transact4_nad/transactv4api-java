/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface IDatosProductoMonedaPlanDecretosDevoluciones {

    public Integer getCantDevolucion();

    public Long getMontoDevolucion();

    public Long getMontoPropinaDevolucion();

    public Long getMontoCashbackDevolucion();

    public Long getMontoDecretoLeyDevolucion();

    public Integer getCantReversoDevolucion();

    public Long getMontoReversoDevolucion();

    public Long getMontoPropinaReversoDevolucion();

    public Long getMontoCashbackReversoDevolucion();

    public Long getMontoDecretoLeyReversoDevolucion();
}
