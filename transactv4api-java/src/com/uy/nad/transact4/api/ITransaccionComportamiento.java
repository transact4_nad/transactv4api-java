/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface ITransaccionComportamiento {

    public void setModificarMoneda(Boolean ModificarMoneda);

    public void setModificarMontos(Boolean ModificarMontos);

    public void setModificarCuotas(Boolean ModificarCuotas);

    public void setModificarFactura(Boolean ModificarFactura);

    public void setModificarTarjeta(Boolean ModificarTarjeta);

    public void setModificarPlan(Boolean ModificarPlan);

    public void setModificarDecretoLey(Boolean ModificarDecretoLey);

    public void setModificarTipoCuenta(Boolean ModificarTipoCuenta);
}
