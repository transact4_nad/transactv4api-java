/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface ITransaccionExtendida {

    void setCuotas(Integer Cuotas);

    void setTarjetaNro(Long TarjetaNro);

    void setTarjetaTitular(String TarjetaTitular);

    void setTarjetaVencimento(String TarjetaVencimento);

    void setTarjetaControl(String TarjetaControl);

    void setTarjetaCVC(String TarjetaCVC);

    void setTarjetaDocIdentidad(String TarjetaDocIdentidad);

    void setPlanId(Integer PlanId);

    void setPlanVtaId(Integer PlanVtaId);

    void setDecretoLeyId(Integer DecretoLeyId);

    void setTipoCuentaId(Integer TipoCuentaId);

    void setT3NroDoc(String T3NroDoc);
}
