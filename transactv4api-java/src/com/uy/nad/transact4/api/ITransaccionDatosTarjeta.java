/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface ITransaccionDatosTarjeta {

    public Integer getEmisorId();

    public Integer getTarjetaId();

    public String getTarjetaTipo();

    public String getTarjetaIIN();

    public Integer getTarjetaLargo();

    public String getTarjetaUlt4();

    public Boolean getTarjetaPrestaciones();

    public Boolean getTarjetaAlimentacion();

    public Boolean getTarjetaPrepaga();

    public String getTarjetaNro();
}
