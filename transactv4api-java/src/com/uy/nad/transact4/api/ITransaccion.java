/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface ITransaccion {

    void setEmpCod(String EmpCod);

    public void setTermCod(String TermCod);

    public void setMultiEmp(Integer MultiEmp);

    public void setOperacion(String Operacion);

    public void setMonedaISO(String MonedaISO);

    public void setMonto(Long Monto);
    public void setMontoTap(Long Monto);

    public void setMontoPropina(Long MontoPropina);

    public void setMontoCashBack(Long MontoCashBack);

    public void setFacturaNro(Long FacturaNro);

    public void setFacturaMonto(Long FacturaMonto);

    public void setFacturaMontoGravado(Long FacturaMontoGravado);

    public void setFacturaMontoIVA(Long FacturaMontoIVA);

    public void setFacturaConsumidorFinal(Boolean FacturaConsumidorFinal);

    public void setTarjetaId(Integer TarjetaId);

    public void setTarjetaTipo(String TarjetaTipo);

    public void setTarjetaAlimentacion(Boolean TarjetaAlimentacion);

    public void setEmisorId(Integer EmisorId);

    public void setTicketOriginal(Integer TicketOriginal);
    
    public ITransaccionExtendida getTransaccionExtendida();
    public ITransaccionComportamiento getComportamiento();

    
    public ITransaccionRespuesta getRespuesta();
    public ITransaccionDatosTarjeta getDatosTarjeta();
    
    
    public void NuevaTransaccion();
    public Boolean CargarDatosTarjeta();
    public void ProcesarTransaccion();
    public Boolean ConfirmarTransaccion(Long TransaccionId);
    public Boolean ReversarTransaccion(Long TransaccionId);
    public void Cancelar();
    public Boolean DispositivoReiniciar();
    public Boolean DispositivoEstado();
}
