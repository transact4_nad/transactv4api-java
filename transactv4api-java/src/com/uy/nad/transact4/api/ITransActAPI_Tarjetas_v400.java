/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public interface ITransActAPI_Tarjetas_v400 {

    IConfiguracion Configuracion();
    ITransaccion Transaccion();
    ICierreLote CierreLote();
    IImpresion Impresion();
    ILectorTarjeta LectorTarjeta();
    public String getVersion();

}
