/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

import java.util.Date;
import java.util.List;

/**
 *
 * @author yunior
 */
public interface IDatosCierreExtendida {

    public Date getCierreFechaHora();

    public String getEmpresaRUT();

    public String getEmpresaNombre();

    public String getSucursalNombre();

    public String getSucursalDireccion();

    public String getMerchantID();

    public String getTerminalID();

    public Integer getCantVenta();

    public Long getMontoVenta();

    public Integer getCantReversoVenta();

    public Long getMontoReversoVenta();

    public Integer getCantAnulacion();

    public Long getMontoAnulacion();

    public Integer getCantReversoAnulacion();

    public Long getMontoReversoAnulacion();

    public Integer getCantDevolucion();

    public Long getMontoDevolucion();

    public Integer getCantReversoDevolucion();

    public Long getMontoReversoDevolucion();

    public List<IDatosProducto> getProductos();
}
