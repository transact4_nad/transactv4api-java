/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface IDatosProductoMonedaPlanDecretosAnulaciones {

    public Integer getCantAnulacion();

    public Long getMontoAnulacion();

    public Long getMontoPropinaAnulacion();

    public Long getMontoCashbackAnulacion();

    public Long getMontoDecretoLeyAnulacion();

    public Integer getCantReversoAnulacion();

    public Long getMontoReversoAnulacion();

    public Long getMontoPropinaReversoAnulacion();

    public Long getMontoCashbackReversoAnulacion();

    public Long getMontoDecretoLeyReversoAnulacion();

}
