/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public class DevolucionAutomaticaRespuesta {

    public enum enumDevolucionAutoCodigo {
        DEV_AUTO_OK("0"),
        DEV_AUTO_ERROR("1"),
        DEV_AUTO_POR_DEVOLUCION("2"),
        DEV_AUTO_POR_CARTA("3");

        private final String text;

        /**
         * @param text
         */
        enumDevolucionAutoCodigo(final String text) {
            this.text = text;
        }

        /* (non-Javadoc)
     * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return text;
        }
    }

    private enumDevolucionAutoCodigo DevolucionAutoCodigo;
    private String DevolucionAutoMensaje = "";

    /**
     * @return the DevolucionAutoCodigo
     */
    public enumDevolucionAutoCodigo getDevolucionAutoCodigo() {
        return DevolucionAutoCodigo;
    }

    /**
     * @return the DevolucionAutoMensaje
     */
    public String getDevolucionAutoMensaje() {
        return DevolucionAutoMensaje;
    }

    /**
     * @param DevolucionAutoCodigo the DevolucionAutoCodigo to set
     */
    public void setDevolucionAutoCodigo(enumDevolucionAutoCodigo DevolucionAutoCodigo) {
        this.DevolucionAutoCodigo = DevolucionAutoCodigo;
    }
    
    /**
     * @param DevolucionAutoCodigo the DevolucionAutoCodigo to set
     */
    public void setDevolucionAutoCodigo(String DevolucionAutoCodigo) {
        if(DevolucionAutoCodigo.equals("2"))
        this.DevolucionAutoCodigo = enumDevolucionAutoCodigo.DEV_AUTO_POR_DEVOLUCION;
        else if(DevolucionAutoCodigo.equals("3"))
            this.DevolucionAutoCodigo = enumDevolucionAutoCodigo.DEV_AUTO_POR_CARTA;
        else if(DevolucionAutoCodigo.equals("1"))
            this.DevolucionAutoCodigo = enumDevolucionAutoCodigo.DEV_AUTO_ERROR;
        else
            this.DevolucionAutoCodigo = enumDevolucionAutoCodigo.DEV_AUTO_OK;
    }

    /**
     * @param DevolucionAutoMensaje the DevolucionAutoMensaje to set
     */
    public void setDevolucionAutoMensaje(String DevolucionAutoMensaje) {
        this.DevolucionAutoMensaje = DevolucionAutoMensaje;
    }
}
