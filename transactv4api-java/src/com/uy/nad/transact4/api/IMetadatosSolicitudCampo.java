/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

import java.util.List;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public interface IMetadatosSolicitudCampo {

    public String getWfCampoKey();

    public String getWfCampoTpoDatos();

    public String getWfCampoDescripcion();

    public String getWfCampoMensaje();

    public Integer getWfCampoLargoMin();

    public Integer getWfCampoLargoMax();

    public Integer getWfCampoValorDesde();

    public Integer getWfCampoValorHasta();

    public String getWfCampoMascara();

    public Boolean getWfCampoRequerido();

    public Boolean getWfCampoNoLegible();

    public List<IMetadatosSolicitudCampoPosiblesValores> getListaPosiblesValores();

    public Object getValorPorDefecto();

    public Boolean getMostrarTouchPad();

    public Integer getPosicionX();

    public Integer getPosicionY();

    public Boolean getSoloLectura();

    public Integer getSegundosTimeoutIngreso();

    public void setValor(Object valor);

    public void setCancelado(Boolean cancelado);
}
