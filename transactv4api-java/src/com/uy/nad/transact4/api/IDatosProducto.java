/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

import java.util.List;

/**
 *
 * @author yunior
 */
public interface IDatosProducto {

    public Integer getTarjetaId();

    public String getTarjetaNombre();

    public Integer getProductoId();

    public String getProductoNombre();

    public String getTarjetaTipo();

    public Boolean getTarjetaPrestaciones();

    public Boolean getTarjetaAlimentacion();

    public List<IDatosProductoMoneda> getMonedas();
}
