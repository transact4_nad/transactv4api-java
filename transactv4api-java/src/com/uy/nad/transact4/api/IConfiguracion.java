/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface IConfiguracion {

    public void setModoEmulacion(Boolean ModoEmulacion);

    public void setPOSTipo(Integer POSTipo);

    public void setPOSTipoCnx(Integer POSTipoCnx);

    public void setPOSDireccionIP(String POSDireccionIP);

    public void setPOSPuerto(int POSPuerto);

    public void setPOSSegsTimeout(Integer POSSegsTimeout);

    public void setURLConcentrador(String URLConcentrador);

    public void setGUITipo(Integer GUITipo);

    public void setGUIModo(Integer GUIModo);

    public void setGUIMostrarTouchPad(Boolean GUIMostrarTouchPad);

    public void setGUIPosicionX(Integer GUIPosicionX);

    public void setGUIPosicionY(Integer GUIPosicionY);

    public void setImpresionTipo(Integer ImpresionTipo);

    public void setImpresionModo(Integer ImpresionModo);

    public void setImpresionTipoImpresora(Integer ImpresionTipoImpresora);

    public void setImpresionNombreImpresora(String ImpresionNombreImpresora);

    public void setImpresionCopias(Integer ImpresionCopias);

    public void setImpresionDireccionIP(String ImpresionDireccionIP);

    public void setImpresionPuerto(Integer ImpresionPuerto);

    public void setImpresionSegsTimeout(Integer ImpresionSegsTimeout);
    
    public void setSegTimeoutLecturaTarjFactura(Integer egTimeoutLecturaTarjFactura);
    
    public void setIngresaPropinaEnPos(Boolean SolicitarPropinaEnPos);
}
