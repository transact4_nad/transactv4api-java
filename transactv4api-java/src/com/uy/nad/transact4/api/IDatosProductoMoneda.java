/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

import java.util.List;

/**
 *
 * @author yunior
 */
public interface IDatosProductoMoneda {

    public String getMonedaISO();

    public Integer getSubTotalCantMoneda();

    public Long getSubTotalMontoMoneda();

    public List<IDatosProductoMonedaPlan> getPlanes();
}
