/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.ITransaccionDatosTarjeta;

/**
 *
 * @author yunior
 */
public class TransaccionDatosTarjeta implements ITransaccionDatosTarjeta{
    Integer EmisorId;
    Integer TarjetaId;
    String TarjetaTipo;
    String TarjetaIIN;
    Integer TarjetaLargo;
    String TarjetaUlt4;
    Boolean TarjetaPrestaciones;
    Boolean TarjetaAlimentacion;
    Boolean TarjetaPrepaga;
    String TarjetaNro;

    @Override
    public Integer getEmisorId() {
        return EmisorId;
    }

    @Override
    public Integer getTarjetaId() {
        return TarjetaId;
    }

    @Override
    public String getTarjetaTipo() {
        return TarjetaTipo;
    }

    @Override
    public String getTarjetaIIN() {
        return TarjetaIIN;
    }

    @Override
    public Integer getTarjetaLargo() {
        return TarjetaLargo;
    }

    @Override
    public String getTarjetaUlt4() {
        return TarjetaUlt4;
    }

    @Override
    public Boolean getTarjetaPrestaciones() {
        return TarjetaPrestaciones;
    }

    @Override
    public Boolean getTarjetaAlimentacion() {
        return TarjetaAlimentacion;
    }

    @Override
    public Boolean getTarjetaPrepaga() {
        return TarjetaPrepaga;
    }

    @Override
    public String getTarjetaNro() {
        return TarjetaNro;
    }
    
    public void setEmisorId(Integer EmisorId) {
        this.EmisorId = EmisorId;
    }

    public void setTarjetaId(Integer TarjetaId) {
        this.TarjetaId = TarjetaId;
    }

    public void setTarjetaTipo(String TarjetaTipo) {
        this.TarjetaTipo = TarjetaTipo;
    }

    public void setTarjetaIIN(String TarjetaIIN) {
        this.TarjetaIIN = TarjetaIIN;
    }

    public void setTarjetaLargo(Integer TarjetaLargo) {
        this.TarjetaLargo = TarjetaLargo;
    }

    public void setTarjetaUlt4(String TarjetaUlt4) {
        this.TarjetaUlt4 = TarjetaUlt4;
    }

    public void setTarjetaPrestaciones(Boolean TarjetaPrestaciones) {
        this.TarjetaPrestaciones = TarjetaPrestaciones;
    }

    public void setTarjetaAlimentacion(Boolean TarjetaAlimentacion) {
        this.TarjetaAlimentacion = TarjetaAlimentacion;
    }

    public void setTarjetaPrepaga(Boolean TarjetaPrepaga) {
        this.TarjetaPrepaga = TarjetaPrepaga;
    }

    public void setTarjetaNro(String TarjetaNro) {
        this.TarjetaNro = TarjetaNro;
    }
            
            
            
}
