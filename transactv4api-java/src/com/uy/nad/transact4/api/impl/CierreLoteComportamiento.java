/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.ICierreLoteComportamiento;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class CierreLoteComportamiento implements  ICierreLoteComportamiento{
      private Boolean ModificarProcesadorId;
      private Boolean ModificarProcesadorIdSeteado;

    public CierreLoteComportamiento() {
    ModificarProcesadorId = false;
    ModificarProcesadorIdSeteado = false;
    }
      
@Override
    public void setModificarProcesadorId(Boolean ModificarProcesadorId) {
        this.ModificarProcesadorId = ModificarProcesadorId;
        ModificarProcesadorIdSeteado = true;
        
    }
    //------------------

    public Boolean getModificarProcesadorId() {
        return ModificarProcesadorId;
    }

    public Boolean getModificarProcesadorIdSeteado() {
        return ModificarProcesadorIdSeteado;
    }
    
    
    
      
}
