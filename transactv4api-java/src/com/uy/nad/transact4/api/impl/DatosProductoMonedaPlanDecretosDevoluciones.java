/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IDatosProductoMonedaPlanDecretosDevoluciones;

/**
 *
 * @author yunior
 */
public class DatosProductoMonedaPlanDecretosDevoluciones implements IDatosProductoMonedaPlanDecretosDevoluciones {

    private Integer CantDevolucion;
    private Long MontoDevolucion;
    private Long MontoPropinaDevolucion;
    private Long MontoCashbackDevolucion;
    private Long MontoDecretoLeyDevolucion;
    private Integer CantReversoDevolucion;
    private Long MontoReversoDevolucion;
    private Long MontoPropinaReversoDevolucion;
    private Long MontoCashbackReversoDevolucion;
    private Long MontoDecretoLeyReversoDevolucion;
@Override
    public Integer getCantDevolucion() {
        return CantDevolucion;
    }
@Override
    public Long getMontoDevolucion() {
        return MontoDevolucion;
    }
@Override
    public Long getMontoPropinaDevolucion() {
        return MontoPropinaDevolucion;
    }

    @Override
    public Long getMontoCashbackDevolucion() {
        return MontoCashbackDevolucion;
    }
@Override
    public Long getMontoDecretoLeyDevolucion() {
        return MontoDecretoLeyDevolucion;
    }
@Override
    public Integer getCantReversoDevolucion() {
        return CantReversoDevolucion;
    }
@Override
    public Long getMontoReversoDevolucion() {
        return MontoReversoDevolucion;
    }
@Override
    public Long getMontoPropinaReversoDevolucion() {
        return MontoPropinaReversoDevolucion;
    }
@Override
    public Long getMontoCashbackReversoDevolucion() {
        return MontoCashbackReversoDevolucion;
    }
@Override
    public Long getMontoDecretoLeyReversoDevolucion() {
        return MontoDecretoLeyReversoDevolucion;
    }
    
    
    //-----------------------------------------------

    public void setCantDevolucion(Integer CantDevolucion) {
        this.CantDevolucion = CantDevolucion;
    }

    public void setMontoDevolucion(Long MontoDevolucion) {
        this.MontoDevolucion = MontoDevolucion;
    }

    public void setMontoPropinaDevolucion(Long MontoPropinaDevolucion) {
        this.MontoPropinaDevolucion = MontoPropinaDevolucion;
    }

    public void setMontoCashbackDevolucion(Long MontoCashbackDevolucion) {
        this.MontoCashbackDevolucion = MontoCashbackDevolucion;
    }

    public void setMontoDecretoLeyDevolucion(Long MontoDecretoLeyDevolucion) {
        this.MontoDecretoLeyDevolucion = MontoDecretoLeyDevolucion;
    }

    public void setCantReversoDevolucion(Integer CantReversoDevolucion) {
        this.CantReversoDevolucion = CantReversoDevolucion;
    }

    public void setMontoReversoDevolucion(Long MontoReversoDevolucion) {
        this.MontoReversoDevolucion = MontoReversoDevolucion;
    }

    public void setMontoPropinaReversoDevolucion(Long MontoPropinaReversoDevolucion) {
        this.MontoPropinaReversoDevolucion = MontoPropinaReversoDevolucion;
    }

    public void setMontoCashbackReversoDevolucion(Long MontoCashbackReversoDevolucion) {
        this.MontoCashbackReversoDevolucion = MontoCashbackReversoDevolucion;
    }

    public void setMontoDecretoLeyReversoDevolucion(Long MontoDecretoLeyReversoDevolucion) {
        this.MontoDecretoLeyReversoDevolucion = MontoDecretoLeyReversoDevolucion;
    }
    
    
}
