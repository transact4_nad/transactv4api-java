/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IDatosProductoMonedaPlanDecretos;
import com.uy.nad.transact4.api.IDatosProductoMonedaPlanDecretosAnulaciones;
import com.uy.nad.transact4.api.IDatosProductoMonedaPlanDecretosDevoluciones;
import com.uy.nad.transact4.api.IDatosProductoMonedaPlanDecretosVentas;

/**
 *
 * @author yunior
 */
public class DatosProductoMonedaPlanDecretos implements IDatosProductoMonedaPlanDecretos{

    private Boolean DecretoLeyAplicado;
    private String DecretoLeyNro;
    private Integer SubTotalCantDecreto;
    private Long SubTotalMontoDecreto;
    private DatosProductoMonedaPlanDecretosVentas Venta;
    private DatosProductoMonedaPlanDecretosAnulaciones Anulacion;
    private DatosProductoMonedaPlanDecretosDevoluciones Devolucion;
    @Override
    public Boolean getDecretoLeyAplicado() {
        return DecretoLeyAplicado;
    }
@Override
    public String getDecretoLeyNro() {
        return DecretoLeyNro;
    }
@Override
    public Integer getSubTotalCantDecreto() {
        return SubTotalCantDecreto;
    }
@Override
    public Long getSubTotalMontoDecreto() {
        return SubTotalMontoDecreto;
    }
@Override
    public IDatosProductoMonedaPlanDecretosVentas getVenta() {
        return Venta;
    }
@Override
    public IDatosProductoMonedaPlanDecretosAnulaciones getAnulacion() {
        return Anulacion;
    }
@Override
    public IDatosProductoMonedaPlanDecretosDevoluciones getDevolucion() {
        return Devolucion;
    }
//--------------------------------------------------
    
    public void setDecretoLeyAplicado(Boolean DecretoLeyAplicado) {
        this.DecretoLeyAplicado = DecretoLeyAplicado;
    }

    public void setDecretoLeyNro(String DecretoLeyNro) {
        this.DecretoLeyNro = DecretoLeyNro;
    }

    public void setSubTotalCantDecreto(Integer SubTotalCantDecreto) {
        this.SubTotalCantDecreto = SubTotalCantDecreto;
    }

    public void setSubTotalMontoDecreto(Long SubTotalMontoDecreto) {
        this.SubTotalMontoDecreto = SubTotalMontoDecreto;
    }

    public void setVenta(DatosProductoMonedaPlanDecretosVentas Venta) {
        this.Venta = Venta;
    }

    public void setAnulacion(DatosProductoMonedaPlanDecretosAnulaciones Anulacion) {
        this.Anulacion = Anulacion;
    }

    public void setDevolucion(DatosProductoMonedaPlanDecretosDevoluciones Devolucion) {
        this.Devolucion = Devolucion;
    }
    
    

    
    
    
}
