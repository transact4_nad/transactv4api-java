/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IDatosCierreExtendida;
import com.uy.nad.transact4.api.IDatosProducto;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author yunior
 */
public class DatosCierreExtendida implements IDatosCierreExtendida{

    private Date CierreFechaHora;

    private String EmpresaRUT;
    private String EmpresaNombre;
    private String SucursalNombre;
    private String SucursalDireccion;
    private String MerchantID;
    private String TerminalID;

    private Integer CantVenta;
    private Long MontoVenta;
    private Integer CantReversoVenta;
    private Long MontoReversoVenta;

    private Integer CantAnulacion;
    private Long MontoAnulacion;
    private Integer CantReversoAnulacion;
    private Long MontoReversoAnulacion;

    private Integer CantDevolucion;
    private Long MontoDevolucion;
    private Integer CantReversoDevolucion;
    private Long MontoReversoDevolucion;

    private List<IDatosProducto> Productos;

    public DatosCierreExtendida() {
        Productos = new ArrayList<IDatosProducto>();
    }
    
    
@Override
    public Date getCierreFechaHora() {
        return CierreFechaHora;
    }
@Override
    public String getEmpresaRUT() {
        return EmpresaRUT;
    }
@Override
    public String getEmpresaNombre() {
        return EmpresaNombre;
    }
@Override
    public String getSucursalNombre() {
        return SucursalNombre;
    }
@Override
    public String getSucursalDireccion() {
        return SucursalDireccion;
    }
@Override
    public String getMerchantID() {
        return MerchantID;
    }
@Override
    public String getTerminalID() {
        return TerminalID;
    }
@Override
    public Integer getCantVenta() {
        return CantVenta;
    }
@Override
    public Long getMontoVenta() {
        return MontoVenta;
    }
@Override
    public Integer getCantReversoVenta() {
        return CantReversoVenta;
    }
@Override
    public Long getMontoReversoVenta() {
        return MontoReversoVenta;
    }
@Override
    public Integer getCantAnulacion() {
        return CantAnulacion;
    }
@Override
    public Long getMontoAnulacion() {
        return MontoAnulacion;
    }
@Override
    public Integer getCantReversoAnulacion() {
        return CantReversoAnulacion;
    }
@Override
    public Long getMontoReversoAnulacion() {
        return MontoReversoAnulacion;
    }
@Override
    public Integer getCantDevolucion() {
        return CantDevolucion;
    }
@Override
    public Long getMontoDevolucion() {
        return MontoDevolucion;
    }
@Override
    public Integer getCantReversoDevolucion() {
        return CantReversoDevolucion;
    }
@Override
    public Long getMontoReversoDevolucion() {
        return MontoReversoDevolucion;
    }
@Override
    public List<IDatosProducto> getProductos() {
        return Productos;
    }

//----------------------------------------------------

    public void setCierreFechaHora(Date CierreFechaHora) {
        this.CierreFechaHora = CierreFechaHora;
    }

    public void setEmpresaRUT(String EmpresaRUT) {
        this.EmpresaRUT = EmpresaRUT;
    }

    public void setEmpresaNombre(String EmpresaNombre) {
        this.EmpresaNombre = EmpresaNombre;
    }

    public void setSucursalNombre(String SucursalNombre) {
        this.SucursalNombre = SucursalNombre;
    }

    public void setSucursalDireccion(String SucursalDireccion) {
        this.SucursalDireccion = SucursalDireccion;
    }

    public void setMerchantID(String MerchantID) {
        this.MerchantID = MerchantID;
    }

    public void setTerminalID(String TerminalID) {
        this.TerminalID = TerminalID;
    }

    public void setCantVenta(Integer CantVenta) {
        this.CantVenta = CantVenta;
    }

    public void setMontoVenta(Long MontoVenta) {
        this.MontoVenta = MontoVenta;
    }

    public void setCantReversoVenta(Integer CantReversoVenta) {
        this.CantReversoVenta = CantReversoVenta;
    }

    public void setMontoReversoVenta(Long MontoReversoVenta) {
        this.MontoReversoVenta = MontoReversoVenta;
    }

    public void setCantAnulacion(Integer CantAnulacion) {
        this.CantAnulacion = CantAnulacion;
    }

    public void setMontoAnulacion(Long MontoAnulacion) {
        this.MontoAnulacion = MontoAnulacion;
    }

    public void setCantReversoAnulacion(Integer CantReversoAnulacion) {
        this.CantReversoAnulacion = CantReversoAnulacion;
    }

    public void setMontoReversoAnulacion(Long MontoReversoAnulacion) {
        this.MontoReversoAnulacion = MontoReversoAnulacion;
    }

    public void setCantDevolucion(Integer CantDevolucion) {
        this.CantDevolucion = CantDevolucion;
    }

    public void setMontoDevolucion(Long MontoDevolucion) {
        this.MontoDevolucion = MontoDevolucion;
    }

    public void setCantReversoDevolucion(Integer CantReversoDevolucion) {
        this.CantReversoDevolucion = CantReversoDevolucion;
    }

    public void setMontoReversoDevolucion(Long MontoReversoDevolucion) {
        this.MontoReversoDevolucion = MontoReversoDevolucion;
    }

    public void setProductos(List<IDatosProducto> Productos) {
        this.Productos = Productos;
    }
    
    
    
}
