/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IDatosProductoMonedaPlan;
import com.uy.nad.transact4.api.IDatosProductoMonedaPlanDecretos;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yunior
 */
public class DatosProductoMonedaPlan implements IDatosProductoMonedaPlan {

    private Integer PlanId;
    private String PlanNombre;
    private Integer PlanNroPlan;
    private Integer PlanNroTipoPlan;
    private String MerchantID;
    private List<IDatosProductoMonedaPlanDecretos> Nacionales;
    private List<IDatosProductoMonedaPlanDecretos> Extranjeras;
    private Integer SubTotalCantPlan;
    private Long SubTotalMontoPlan;
    private Long SubTotalDecretoLeyPlan;

    public DatosProductoMonedaPlan() {
         Nacionales = new ArrayList<IDatosProductoMonedaPlanDecretos>();
         Extranjeras = new ArrayList<IDatosProductoMonedaPlanDecretos>();
    }

    
    @Override
    public Integer getPlanId() {
        return PlanId;
    }

    @Override
    public String getPlanNombre() {
        return PlanNombre;
    }

    @Override
    public Integer getPlanNroPlan() {
        return PlanNroPlan;
    }

    @Override
    public Integer getPlanNroTipoPlan() {
        return PlanNroTipoPlan;
    }

    @Override
    public String getMerchantID() {
        return MerchantID;
    }

    @Override
    public List<IDatosProductoMonedaPlanDecretos> getNacionales() {
        return Nacionales;
    }

    @Override
    public List<IDatosProductoMonedaPlanDecretos> getExtranjeras() {
        return Extranjeras;
    }

    @Override
    public Integer getSubTotalCantPlan() {
        return SubTotalCantPlan;
    }

    @Override
    public Long getSubTotalMontoPlan() {
        return SubTotalMontoPlan;
    }

    @Override
    public Long getSubTotalDecretoLeyPlan() {
        return SubTotalDecretoLeyPlan;
    }
//---------------------------------------------------------
    public void setPlanId(Integer PlanId) {
        this.PlanId = PlanId;
    }

    public void setPlanNombre(String PlanNombre) {
        this.PlanNombre = PlanNombre;
    }

    public void setPlanNroPlan(Integer PlanNroPlan) {
        this.PlanNroPlan = PlanNroPlan;
    }

    public void setPlanNroTipoPlan(Integer PlanNroTipoPlan) {
        this.PlanNroTipoPlan = PlanNroTipoPlan;
    }

    public void setMerchantID(String MerchantID) {
        this.MerchantID = MerchantID;
    }

    public void setNacionales(List<IDatosProductoMonedaPlanDecretos> Nacionales) {
        this.Nacionales = Nacionales;
    }

    public void setExtranjeras(List<IDatosProductoMonedaPlanDecretos> Extranjeras) {
        this.Extranjeras = Extranjeras;
    }

    public void setSubTotalCantPlan(Integer SubTotalCantPla) {
        this.SubTotalCantPlan = SubTotalCantPla;
    }

    public void setSubTotalMontoPlan(Long SubTotalMontoPlan) {
        this.SubTotalMontoPlan = SubTotalMontoPlan;
    }

    public void setSubTotalDecretoLeyPlan(Long SubTotalDecretoLeyPlan) {
        this.SubTotalDecretoLeyPlan = SubTotalDecretoLeyPlan;
    }
    
    
    

}
