/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.ICierreLoteRespuesta;
import com.uy.nad.transact4.api.IDatosCierre;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class CierreLoteRespuesta implements ICierreLoteRespuesta {

    private String TokenCierre;
    private Boolean Finalizado;
    private String Estado;
    private List<String> Voucher;
    private List<IDatosCierre> LstDatosCierre;

    public CierreLoteRespuesta() {
       Limpiar();    }

    @Override
    public String getTokenCierre() {
        return TokenCierre;
    }

    @Override
    public Boolean getFinalizado() {
        return Finalizado;
    }

    @Override
    public String getEstado() {
        return Estado;
    }

    @Override
    public List<String> getVoucher() {
        return Voucher;
    }

    @Override
    public List<IDatosCierre> getDatosCierre() {
        return LstDatosCierre;
    }

    //--------------------------------------------
    public void setTokenCierre(String TokenCierre) {
        this.TokenCierre = TokenCierre;
    }

    public void setFinalizado(Boolean Finalizado) {
        this.Finalizado = Finalizado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public void setVoucher(List<String> Voucher) {
        this.Voucher = Voucher;
    }

    public void setDatosCierre(List<IDatosCierre> DatosCierre) {
        this.LstDatosCierre = DatosCierre;
    }

    public void Limpiar() {
        this.TokenCierre = "";
        this.Finalizado = false;
        this.Estado = "";
        this.Voucher = new ArrayList<String>();
        this.LstDatosCierre = new ArrayList<IDatosCierre>();
    }
}
