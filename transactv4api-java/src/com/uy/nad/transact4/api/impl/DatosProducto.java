/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IDatosProducto;
import com.uy.nad.transact4.api.IDatosProductoMoneda;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yunior
 */
public class DatosProducto implements IDatosProducto {

    private Integer TarjetaId;
    private String TarjetaNombre;
    private Integer ProductoId;
    private String ProductoNombre;
    private String TarjetaTipo;
    private Boolean TarjetaPrestaciones;
    private Boolean TarjetaAlimentacion;
    private List<IDatosProductoMoneda> Monedas;

    public DatosProducto() {
        Monedas = new ArrayList<IDatosProductoMoneda>();
    }
    
@Override
    public Integer getTarjetaId() {
        return TarjetaId;
    }
@Override
    public String getTarjetaNombre() {
        return TarjetaNombre;
    }
@Override
    public Integer getProductoId() {
        return ProductoId;
    }
@Override
    public String getProductoNombre() {
        return ProductoNombre;
    }
@Override
    public String getTarjetaTipo() {
        return TarjetaTipo;
    }
@Override
    public Boolean getTarjetaPrestaciones() {
        return TarjetaPrestaciones;
    }
@Override
    public Boolean getTarjetaAlimentacion() {
        return TarjetaAlimentacion;
    }
@Override
    public List<IDatosProductoMoneda> getMonedas() {
        return Monedas;
    }
//---------------------------------------------------

    public void setTarjetaId(Integer TarjetaId) {
        this.TarjetaId = TarjetaId;
    }

    public void setTarjetaNombre(String TarjetaNombre) {
        this.TarjetaNombre = TarjetaNombre;
    }

    public void setProductoId(Integer ProductoId) {
        this.ProductoId = ProductoId;
    }

    public void setProductoNombre(String ProductoNombre) {
        this.ProductoNombre = ProductoNombre;
    }

    public void setTarjetaTipo(String TarjetaTipo) {
        this.TarjetaTipo = TarjetaTipo;
    }

    public void setTarjetaPrestaciones(Boolean TarjetaPrestaciones) {
        this.TarjetaPrestaciones = TarjetaPrestaciones;
    }

    public void setTarjetaAlimentacion(Boolean TarjetaAlimentacion) {
        this.TarjetaAlimentacion = TarjetaAlimentacion;
    }

    public void setMonedas(List<IDatosProductoMoneda> Monedas) {
        this.Monedas = Monedas;
    }


    
    
    
}
