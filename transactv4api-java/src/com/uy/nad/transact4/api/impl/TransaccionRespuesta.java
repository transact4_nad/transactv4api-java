/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.DevolucionAutomaticaRespuesta;
import com.uy.nad.transact4.api.ITransaccionRespuesta;
import com.uy.nad.transact4.api.ITransaccionRespuestaDatosTransaccion;
import java.util.List;

/**
 *
 * @author yunior
 */
public class TransaccionRespuesta implements ITransaccionRespuesta {

    private Long TransaccionId;
    private Boolean Aprobada;
    private String NroAutorizacion;
    private String CodRespuesta;
    private String MsgRespuesta;
    private Long Lote;
    private Long Ticket;
    private Integer TarjetaId;
    private String TarjetaTipo;
    private Boolean EsOffline;
    private List<String> Voucher;
    private ITransaccionRespuestaDatosTransaccion _DatosTransaccion = new TransaccionRespuestaDatosTransaccion();

    private String CodRespuestaComando;
    private String MsgRespuestaComando;

    private final DevolucionAutomaticaRespuesta _DevolucionAutomaticaRespuesta = new DevolucionAutomaticaRespuesta();
    
    @Override
    public Long getTransaccionId() {
        return TransaccionId;
    }

    @Override
    public Boolean getAprobada() {
        return Aprobada;
    }

    @Override
    public String getNroAutorizacion() {
        return NroAutorizacion;
    }

    @Override
    public String getCodRespuesta() {
        return CodRespuesta;
    }

    @Override
    public String getMsgRespuesta() {
        return MsgRespuesta;
    }

    @Override
    public Long getLote() {
        return Lote;
    }

    @Override
    public Long getTicket() {
        return Ticket;
    }

    @Override
    public Integer getTarjetaId() {
        return TarjetaId;
    }

    @Override
    public String getTarjetaTipo() {
        return TarjetaTipo;
    }

    @Override
    public Boolean getEsOffline() {
        return EsOffline;
    }

    @Override
    public List<String> getVoucher() {
        return Voucher;
    }

    @Override
    public ITransaccionRespuestaDatosTransaccion getDatosTransaccion() {
        return _DatosTransaccion;
    }
    
    @Override
    public String getCodRespuestaComando(){
    return CodRespuestaComando;
    }
    
    @Override
    public String getMsgRespuestaComando(){
    return MsgRespuestaComando;
    }
    
    @Override
    public DevolucionAutomaticaRespuesta getDevolucionAutomaticaRespuesta(){
      return this._DevolucionAutomaticaRespuesta;
    }
    //----------------

    public void setTransaccionId(Long TransaccionId) {
        this.TransaccionId = TransaccionId;
    }

    public void setAprobada(Boolean Aprobada) {
        this.Aprobada = Aprobada;
    }

    public void setNroAutorizacion(String NroAutorizacion) {
        this.NroAutorizacion = NroAutorizacion;
    }

    public void setCodRespuesta(String CodRespuesta) {
        this.CodRespuesta = CodRespuesta;
    }

    public void setMsgRespuesta(String MsgRespuesta) {
        this.MsgRespuesta = MsgRespuesta;
    }

    public void setLote(Long Lote) {
        this.Lote = Lote;
    }

    public void setTicket(Long Ticket) {
        this.Ticket = Ticket;
    }

    public void setTarjetaId(Integer TarjetaId) {
        this.TarjetaId = TarjetaId;
    }

    public void setTarjetaTipo(String TarjetaTipo) {
        this.TarjetaTipo = TarjetaTipo;
    }

    public void setEsOffline(Boolean EsOffline) {
        this.EsOffline = EsOffline;
    }

    public void setVoucher(List<String> Voucher) {
        this.Voucher = Voucher;
    }

    public void setDatosTransaccion(ITransaccionRespuestaDatosTransaccion _DatosTransaccion) {
        this._DatosTransaccion = _DatosTransaccion;
    }
    
    public void setCodRespuestaComando(String CodRespuestaComando){
        this.CodRespuestaComando = CodRespuestaComando;
    }
    
    public void setMsgRespuestaComando(String MsgRespuestaComando){
        this.MsgRespuestaComando = MsgRespuestaComando;
    }
}
