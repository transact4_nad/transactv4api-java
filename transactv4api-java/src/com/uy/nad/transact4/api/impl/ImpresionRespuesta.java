/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IImpresionRespuesta;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class ImpresionRespuesta implements IImpresionRespuesta{
    private Boolean Aprobado;
    private String Mensaje;
    public ImpresionRespuesta() {
    }
    
    @Override
    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String Mensaje) {
        this.Mensaje = Mensaje;
    }
    @Override
    public Boolean getAprobado() {
        return Aprobado;
    }

    public void setAprobado(Boolean Aprobado) {
        this.Aprobado = Aprobado;
    }
    
    
    
}
