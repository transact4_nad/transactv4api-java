/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.ITransaccionRespuestaDatosTransaccion;
import com.uy.nad.transact4.api.ITransaccionRespuestaDatosTransaccionExtendida;

/**
 *
 * @author yunior
 */
public class TransaccionRespuestaDatosTransaccion implements ITransaccionRespuestaDatosTransaccion {

    private String Operacion;
    private String MonedaISO;
    private Long Monto;
    private Long MontoPropina;
    private Long MontoCashBack;
    private Integer Cuotas;
    private String TarjetaNro;
    private String TarjetaIIN;
    private Boolean TarjetaExtranjera;
    private Boolean TarjetaPrestaciones;
    private Boolean TarjetaAlimentacion;
    private Integer EmisorId;
    private Boolean DecretoLeyAplicado;
    private String DecretoLeyNro;
    private Long DecretoLeyMonto;
    private TransaccionRespuestaDatosTransaccionExtendida DatosTransaccionExtendida = new TransaccionRespuestaDatosTransaccionExtendida();

    @Override
    public String getOperacion() {
        return Operacion;
    }

    @Override
    public String getMonedaISO() {
        return MonedaISO;
    }

    @Override
    public Long getMonto() {
        return Monto;
    }

    @Override
    public Long getMontoPropina() {
        return MontoPropina;
    }

    @Override
    public Long getMontoCashBack() {
        return MontoCashBack;
    }

    @Override
    public Integer getCuotas() {
        return Cuotas;
    }

    @Override
    public String getTarjetaNro() {
        return TarjetaNro;
    }

    @Override
    public String getTarjetaIIN() {
        return TarjetaIIN;
    }

    @Override
    public Boolean getTarjetaExtranjera() {
        return TarjetaExtranjera;
    }

    @Override
    public Boolean getTarjetaPrestaciones() {
        return TarjetaPrestaciones;
    }

    @Override
    public Boolean getTarjetaAlimentacion() {
        return TarjetaAlimentacion;
    }

    @Override
    public Integer getEmisorId() {
        return EmisorId;
    }

    @Override
    public Boolean getDecretoLeyAplicado() {
        return DecretoLeyAplicado;
    }

    @Override
    public String getDecretoLeyNro() {
        return DecretoLeyNro;
    }

    @Override
    public Long getDecretoLeyMonto() {
        return DecretoLeyMonto;
    }

    @Override
    public ITransaccionRespuestaDatosTransaccionExtendida getDatosTransaccionExtendida() {
        return DatosTransaccionExtendida;
    }

    //-------------------------
    public void setOperacion(String Operacion) {
        this.Operacion = Operacion;
    }

    public void setMonedaISO(String MonedaISO) {
        this.MonedaISO = MonedaISO;
    }

    public void setMonto(Long Monto) {
        this.Monto = Monto;
    }

    public void setMontoPropina(Long MontoPropina) {
        this.MontoPropina = MontoPropina;
    }

    public void setMontoCashBack(Long MontoCashBack) {
        this.MontoCashBack = MontoCashBack;
    }

    public void setCuotas(Integer Cuotas) {
        this.Cuotas = Cuotas;
    }

    public void setTarjetaNro(String TarjetaNro) {
        this.TarjetaNro = TarjetaNro;
    }

    public void setTarjetaIIN(String TarjetaIIN) {
        this.TarjetaIIN = TarjetaIIN;
    }

    public void setTarjetaExtranjera(Boolean TarjetaExtranjera) {
        this.TarjetaExtranjera = TarjetaExtranjera;
    }

    public void setTarjetaPrestaciones(Boolean TarjetaPrestaciones) {
        this.TarjetaPrestaciones = TarjetaPrestaciones;
    }

    public void setTarjetaAlimentacion(Boolean TarjetaAlimentacion) {
        this.TarjetaAlimentacion = TarjetaAlimentacion;
    }

    public void setEmisorId(Integer EmisorId) {
        this.EmisorId = EmisorId;
    }

    public void setDecretoLeyAplicado(Boolean DecretoLeyAplicado) {
        this.DecretoLeyAplicado = DecretoLeyAplicado;
    }

    public void setDecretoLeyNro(String DecretoLeyNro) {
        this.DecretoLeyNro = DecretoLeyNro;
    }

    public void setDecretoLeyMonto(Long DecretoLeyMonto) {
        this.DecretoLeyMonto = DecretoLeyMonto;
    }

    public void setDatosTransaccionExtendida(TransaccionRespuestaDatosTransaccionExtendida DatosTransaccionExtendida) {
        this.DatosTransaccionExtendida = DatosTransaccionExtendida;
    }

}
