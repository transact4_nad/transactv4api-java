/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.ITransaccionExtendida;

/**
 *
 * @author yunior
 */
public class TransaccionExtendida implements ITransaccionExtendida {

    private Integer Cuotas;
    private Long TarjetaNro;
    private String TarjetaTitular;
    private String TarjetaVencimento;
    private String TarjetaControl;
    private String TarjetaCVC;
    private String TarjetaDocIdentidad;
    private Integer PlanId;
    private Integer PlanVtaId;
    private Integer DecretoLeyId;
    private Integer TipoCuentaId;
    private String T3NroDoc;

    private Boolean CuotasSeteada = false;
    private Boolean TarjetaNroSeteada = false;
    private Boolean TarjetaTitularSeteada = false;
    private Boolean TarjetaVencimentoSeteada = false;
    private Boolean TarjetaControlSeteada = false;
    private Boolean TarjetaCVCSeteada = false;
    private Boolean TarjetaDocIdentidadSeteada = false;
    private Boolean PlanIdSeteada = false;
    private Boolean PlanVtaIdSeteada = false;
    private Boolean DecretoLeyIdSeteada = false;
    private Boolean TipoCuentaIdSeteada = false;
    private Boolean T3NroDocSeteada = false;
    
    @Override
    public void setCuotas(Integer Cuotas) {
        this.Cuotas = Cuotas;
        this.CuotasSeteada = true;
    }

    @Override
    public void setTarjetaNro(Long TarjetaNro) {
        this.TarjetaNro = TarjetaNro;
        this.TarjetaNroSeteada = true;
    }

    @Override
    public void setTarjetaTitular(String TarjetaTitular) {
        this.TarjetaTitular = TarjetaTitular;
        this.TarjetaTitularSeteada = true;
    }

    @Override
    public void setTarjetaVencimento(String TarjetaVencimento) {
        this.TarjetaVencimento = TarjetaVencimento;
        this.TarjetaVencimentoSeteada = true;
    }

    @Override
    public void setTarjetaControl(String TarjetaControl) {
        this.TarjetaControl = TarjetaControl;
        this.TarjetaControlSeteada = true;
    }

    @Override
    public void setTarjetaCVC(String TarjetaCVC) {
        this.TarjetaCVC = TarjetaCVC;
        this.TarjetaCVCSeteada = true;
    }

    @Override
    public void setTarjetaDocIdentidad(String TarjetaDocIdentidad) {
        this.TarjetaDocIdentidad = TarjetaDocIdentidad;
        this.TarjetaDocIdentidadSeteada = true;
    }

    @Override
    public void setPlanId(Integer PlanId) {
        this.PlanId = PlanId;
        this.PlanIdSeteada = true;
    }
    @Override
    public void setPlanVtaId(Integer PlanVtaId) {
        this.PlanVtaId = PlanVtaId;
        this.PlanVtaIdSeteada = true;
    }

    @Override
    public void setDecretoLeyId(Integer DecretoLeyId) {
        this.DecretoLeyId = DecretoLeyId;
        this.DecretoLeyIdSeteada = true;
    }

    @Override
    public void setTipoCuentaId(Integer TipoCuentaId) {
        this.TipoCuentaId = TipoCuentaId;
        this.TipoCuentaIdSeteada = true;
    }

    @Override
    public void setT3NroDoc(String T3NroDoc) {
        this.T3NroDoc = T3NroDoc;
        this.T3NroDocSeteada = true;
    }
    
    /**
     * @return the CuotasSeteada
     */
    public Boolean getCuotasSeteada() {
        return CuotasSeteada;
    }

    /**
     * @return the DecretoLeyIdSeteada
     */
    public Boolean getDecretoLeyIdSeteada() {
        return DecretoLeyIdSeteada;
    }

    /**
     * @return the PlanIdSeteada
     */
    public Boolean getPlanIdSeteada() {
        return PlanIdSeteada;
    }
    
    /**
     * @return the PlanVtaIdSeteada
     */
    public Boolean getPlanVtaIdSeteada() {
        return PlanVtaIdSeteada;
    }
    
    /**
     * @return the T3NroDocSeteada
     */
    public Boolean getT3NroDocSeteada() {
        return T3NroDocSeteada;
    }

    /**
     * @return the TarjetaCVCSeteada
     */
    public Boolean getTarjetaCVCSeteada() {
        return TarjetaCVCSeteada;
    }

    /**
     * @return the TarjetaControlSeteada
     */
    public Boolean getTarjetaControlSeteada() {
        return TarjetaControlSeteada;
    }

    /**
     * @return the TarjetaDocIdentidadSeteada
     */
    public Boolean getTarjetaDocIdentidadSeteada() {
        return TarjetaDocIdentidadSeteada;
    }

    /**
     * @return the TarjetaNroSeteada
     */
    public Boolean getTarjetaNroSeteada() {
        return TarjetaNroSeteada;
    }

    /**
     * @return the TarjetaTitularSeteada
     */
    public Boolean getTarjetaTitularSeteada() {
        return TarjetaTitularSeteada;
    }

    /**
     * @return the TarjetaVencimentoSeteada
     */
    public Boolean getTarjetaVencimentoSeteada() {
        return TarjetaVencimentoSeteada;
    }

    /**
     * @return the TipoCuentaIdSeteada
     */
    public Boolean getTipoCuentaIdSeteada() {
        return TipoCuentaIdSeteada;
    }
    
    /**
     * @return the Cuotas
     */
    public Integer getCuotas() {
        return Cuotas;
    }

    /**
     * @return the DecretoLeyId
     */
    public Integer getDecretoLeyId() {
        return DecretoLeyId;
    }

    /**
     * @return the PlanId
     */
    public Integer getPlanId() {
        return PlanId;
    }
    /**
     * @return the PlanVtaId
     */
    public Integer getPlanVtaId() {
        return PlanVtaId;
    }
    /**
     * @return the T3NroDoc
     */
    public String getT3NroDoc() {
        return T3NroDoc;
    }

    /**
     * @return the TarjetaCVC
     */
    public String getTarjetaCVC() {
        return TarjetaCVC;
    }

    /**
     * @return the TarjetaControl
     */
    public String getTarjetaControl() {
        return TarjetaControl;
    }

    /**
     * @return the TarjetaDocIdentidad
     */
    public String getTarjetaDocIdentidad() {
        return TarjetaDocIdentidad;
    }

    /**
     * @return the TarjetaNro
     */
    public Long getTarjetaNro() {
        return TarjetaNro;
    }

    /**
     * @return the TarjetaTitular
     */
    public String getTarjetaTitular() {
        return TarjetaTitular;
    }

    /**
     * @return the TarjetaVencimento
     */
    public String getTarjetaVencimento() {
        return TarjetaVencimento;
    }

    /**
     * @return the TipoCuentaId
     */
    public Integer getTipoCuentaId() {
        return TipoCuentaId;
    }
}
