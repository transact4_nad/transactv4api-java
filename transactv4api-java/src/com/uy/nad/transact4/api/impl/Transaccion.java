/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.ITransaccion;
import com.uy.nad.transact4.api.ITransaccionComportamiento;
import com.uy.nad.transact4.api.ITransaccionDatosTarjeta;
import com.uy.nad.transact4.api.ITransaccionExtendida;
import com.uy.nad.transact4.api.ITransaccionRespuesta;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Tarjetas;
import java.util.ArrayList;
import java.util.List;
import com.uy.nad.transact4.api.IAPIListener;

/**
 *
 * @author yunior
 */
public class Transaccion implements ITransaccion {

    private TransActAPI_Tarjetas_v400Impl _fachada;
    private IAPIListener _listener;
    private Configuracion _configuracion;
    //datos 
    private Boolean Procesada = false;
    private Boolean Confirmada = false;

    private String EmpCod;
    private Boolean EmpCodSeteado = false;
    private String TermCod;
    private Boolean TermCodSeteado = false;
    private Integer MultiEmp;
    private Boolean MultiEmpSeteado = false;
    private String Operacion;
    private Boolean OperacionSeteado = false;
    private String MonedaISO;
    private Boolean MonedaISOSeteado = false;
    private Long Monto;
    private Boolean MontoSeteado = false;
    private Long MontoTap;
    private Boolean MontoTapSeteado = false;
    private Long MontoPropina;
    private Boolean MontoPropinaSeteado = false;
    private Long MontoCashBack;
    private Boolean MontoCashBackSeteado = false;
    private Long FacturaNro;
    private Boolean FacturaNroSeteado = false;
    private Long FacturaMonto;
    private Boolean FacturaMontoSeteado = false;
    private Long FacturaMontoGravado;
    private Boolean FacturaMontoGravadoSeteado = false;
    private Long FacturaMontoIVA;
    private Boolean FacturaMontoIVASeteado = false;
    private Boolean FacturaConsumidorFinal;
    private Boolean FacturaConsumidorFinalSeteado = false;
    private Integer TarjetaId;
    private Boolean TarjetaIdSeteado = false;
    private String TarjetaTipo;
    private Boolean TarjetaTipoSeteado = false;
    private Boolean TarjetaAlimentacion;
    private Boolean TarjetaAlimentacionSeteado = false;
    private Integer EmisorId;
    private Boolean EmisorIdSeteado = false;
    private Integer TicketOriginal;
    private Boolean TicketOriginalSeteado = false;
    //Datos Entrada
    private TransaccionExtendida objTransaccionExtendida = new TransaccionExtendida();
    private TransaccionComportamiento objTransaccionComportamiento = new TransaccionComportamiento();

    //Datos Salida
    private TransaccionRespuesta objTransaccionRespuesta = new TransaccionRespuesta();
    private TransaccionDatosTarjeta objTransaccionDatosTarjeta = new TransaccionDatosTarjeta();

    public Transaccion(TransActAPI_Tarjetas_v400Impl _fachada, IAPIListener _listener, Configuracion configuracion) {
        this._fachada = _fachada;
        this._listener = _listener;
        this._configuracion = configuracion;
    }

    @Override
    public void setEmpCod(String EmpCod) {
        this.EmpCod = EmpCod;
        this.EmpCodSeteado = true;
    }

    @Override
    public void setTermCod(String TermCod) {
        this.TermCod = TermCod;
        this.TermCodSeteado = true;
    }

    @Override
    public void setMultiEmp(Integer MultiEmp) {
        this.MultiEmp = MultiEmp;
        this.MultiEmpSeteado = true;
    }

    @Override
    public void setOperacion(String Operacion) {
        this.Operacion = Operacion;
        this.OperacionSeteado = true;
    }

    @Override
    public void setMonedaISO(String MonedaISO) {
        this.MonedaISO = MonedaISO;
        this.MonedaISOSeteado = true;
    }

    @Override
    public void setMonto(Long Monto) {
        this.Monto = Monto;
        this.MontoSeteado = true;
    }
     @Override
    public void setMontoTap(Long MontoTap) {
        this.MontoTap = MontoTap;
        this.MontoTapSeteado = true;
    }

    @Override
    public void setMontoPropina(Long MontoPropina) {
        this.MontoPropina = MontoPropina;
        this.MontoPropinaSeteado = true;
    }

    @Override
    public void setMontoCashBack(Long MontoCashBack) {
        this.MontoCashBack = MontoCashBack;
        this.MontoCashBackSeteado = true;
    }

    @Override
    public void setFacturaNro(Long FacturaNro) {
        this.FacturaNro = FacturaNro;
        this.FacturaNroSeteado = true;
    }

    @Override
    public void setFacturaMonto(Long FacturaMonto) {
        this.FacturaMonto = FacturaMonto;
        this.FacturaMontoSeteado = true;
    }

    @Override
    public void setFacturaMontoGravado(Long FacturaMontoGravado) {
        this.FacturaMontoGravado = FacturaMontoGravado;
        this.FacturaMontoGravadoSeteado = true;
    }

    @Override
    public void setFacturaMontoIVA(Long FacturaMontoIVA) {
        this.FacturaMontoIVA = FacturaMontoIVA;
        this.FacturaMontoIVASeteado = true;
    }

    @Override
    public void setFacturaConsumidorFinal(Boolean FacturaConsumidorFinal) {
        this.FacturaConsumidorFinal = FacturaConsumidorFinal;
        this.FacturaConsumidorFinalSeteado = true;
    }

    @Override
    public void setTarjetaId(Integer TarjetaId) {
        this.TarjetaId = TarjetaId;
        this.TarjetaIdSeteado = true;
    }

    @Override
    public void setTarjetaTipo(String TarjetaTipo) {
        this.TarjetaTipo = TarjetaTipo;
        this.TarjetaTipoSeteado = true;
    }

    @Override
    public void setTarjetaAlimentacion(Boolean TarjetaAlimentacion) {
        this.TarjetaAlimentacion = TarjetaAlimentacion;
        this.TarjetaAlimentacionSeteado = true;
    }

    @Override
    public void setEmisorId(Integer EmisorId) {
        this.EmisorId = EmisorId;
        this.EmisorIdSeteado = true;
    }

    @Override
    public void setTicketOriginal(Integer TicketOriginal) {
        this.TicketOriginal = TicketOriginal;
        this.TicketOriginalSeteado = true;
    }

    @Override
    public ITransaccionExtendida getTransaccionExtendida() {
        return objTransaccionExtendida;
    }

    @Override
    public ITransaccionComportamiento getComportamiento() {
        return objTransaccionComportamiento;
    }

    @Override
    public ITransaccionRespuesta getRespuesta() {
        return objTransaccionRespuesta;
    }

    @Override
    public ITransaccionDatosTarjeta getDatosTarjeta() {
        return objTransaccionDatosTarjeta;
    }

    @Override
    public void NuevaTransaccion() {
        Procesada = false;
        Confirmada = false;
        LimpiarTransaccion();
    }

    @Override
    public Boolean CargarDatosTarjeta() {
        Tarjetas objCore = null;
        Boolean resultado = false;
        try {
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SeteoOperacion(objCore);
            SetearTransaccion(objCore);
            SetearTransaccionExtendida(objCore);
            objCore.CargarDatosTarjeta();
            CargarRespCargarDatosTarjeta(objCore, objTransaccionDatosTarjeta);
            resultado = true;
        } catch (Exception ex) {
            CargarRespuestaByException(ex, objTransaccionRespuesta);
            resultado = false;
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }
        return resultado;
    }

    @Override
    public void ProcesarTransaccion() {
        Tarjetas objCore = null;
        try {
            if (this.Procesada) {
                throw new Exception("YA SE HABIA INVOCADO EL METODO PROCESAR TRANSACCION, DEBE CREAR UNA NUEVA TRANSACCION");
            }
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SeteoOperacion(objCore);
            SetearTransaccion(objCore);
            SetearTransaccionExtendida(objCore);
            objCore.ProcesarTransaccion();
            CargarRespTransaccion(objCore, objTransaccionRespuesta);
            Procesada = true;
        } catch (Exception ex) {
            CargarRespuestaByException(ex, objTransaccionRespuesta);
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }

    }

    @Override
    public Boolean ConfirmarTransaccion(Long TransaccionId) {
        Tarjetas objCore = null;
        try {
            if (this.Confirmada) {
                throw new Exception("YA SE HABIA INVOCADO EL METODO CONFIRMARTRANSACCION, DEBE CREAR UNA NUEVATRANSACCION");
            }
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SeteoOperacion(objCore);
            SetearTransaccion(objCore);
            try {
                objCore.ConfirmarTransaccion(TransaccionId);
            } catch (Exception ex) {
                if (!IntentoConsultarConfirmacionTransaccion(TransaccionId)) {
                    throw ex;
                }
            }
            Confirmada = true;
        } catch (Exception ex) {
            CargarRespuestaByException(ex, objTransaccionRespuesta);
        }
        return this.Confirmada;
    }

    @Override
    public Boolean ReversarTransaccion(Long TransaccionId) {
        Tarjetas objCore = null;
        try {
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SetearTransaccion(objCore);
            try {
                objCore.ReversarTransaccion(TransaccionId);
            } catch (Exception ex) {
                throw ex;
            }
            Confirmada = true;
        } catch (Exception ex) {
            CargarRespuestaByException(ex, objTransaccionRespuesta);
        }
        return this.Confirmada;
    }

    @Override
    public Boolean DispositivoEstado() {
        Tarjetas objCore = null;
        Boolean resultado = false;
        try {
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SetearTransaccion(objCore);
            objCore.DispositivoStatus();
            CargarRespTransaccion(objCore, objTransaccionRespuesta);
            if (objTransaccionRespuesta.getCodRespuestaComando().equals("0")) {
                resultado = true;
            }
        } catch (Exception ex) {
            CargarRespuestaByException(ex, objTransaccionRespuesta);
            resultado = false;
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }
        return resultado;
    }

    @Override
    public Boolean DispositivoReiniciar() {
        Tarjetas objCore = null;
        Boolean resultado = false;
        try {
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SetearTransaccion(objCore);
            objCore.DispositivoReiniciar();
            CargarRespTransaccion(objCore, objTransaccionRespuesta);
            if (objTransaccionRespuesta.getCodRespuestaComando().equals("0")) {
                resultado = true;
            }
        } catch (Exception ex) {
            CargarRespuestaByException(ex, objTransaccionRespuesta);
            resultado = false;
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }
        return resultado;
    }

    @Override 
    public void Cancelar(){
       Tarjetas objCore = null;
        try {
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SeteoOperacion(objCore);
            SetearTransaccion(objCore);
            objCore.Cancelar();
        } catch (Exception ex) {
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }
    }
    
//########## REGION PRIVADO
    void LimpiarTransaccion() {
        this.EmpCod = "";
        EmpCodSeteado = false;
        TermCod = "";
        TermCodSeteado = false;
        MultiEmp = 0;
        MultiEmpSeteado = false;
        Operacion = "";
        OperacionSeteado = false;
        MonedaISO = "";
        MonedaISOSeteado = false;
        Monto = new Long(0);
        MontoSeteado = false;
        MontoPropina = new Long(0);
        MontoPropinaSeteado = false;
        MontoCashBack = new Long(0);
        MontoCashBackSeteado = false;
        FacturaNro = new Long(0);
        FacturaNroSeteado = false;
        FacturaMonto = new Long(0);
        FacturaMontoSeteado = false;
        FacturaMontoGravado = null;
        FacturaMontoGravadoSeteado = false;
        FacturaMontoIVA = new Long(0);
        FacturaMontoIVASeteado = false;
        FacturaConsumidorFinal = false;
        FacturaConsumidorFinalSeteado = false;
        TarjetaId = 0;
        TarjetaIdSeteado = false;
        TarjetaTipo = "";
        TarjetaTipoSeteado = false;
        TarjetaAlimentacion = false;
        TarjetaAlimentacionSeteado = false;
        EmisorId = 0;
        EmisorIdSeteado = false;
        TicketOriginal = 0;
        TicketOriginalSeteado = false;
        objTransaccionComportamiento.Limpiar();
        objTransaccionExtendida = new TransaccionExtendida();
        objTransaccionRespuesta = new TransaccionRespuesta();
    }

    void SeteoOperacion(Tarjetas objCore) throws Exception {
        if (OperacionSeteado) {
            if (Operacion.equals("DEV") || Operacion.equals("DEVOLUCION")) {
                Operacion = "DEVOLUCION";
            } else if (Operacion.equals("DVA") || Operacion.equals("DEVOLUCIONAUTOMATICA")) {
                Operacion = "DEVOLUCION AUTOMATICA";
            } else {
                Operacion = "VENTA";
            }
        } else {
            throw new Exception("OPERACION NO SETEADA");
        }
    }

    private String ObtenerOperacion(String Operacion) {
        String ret = "VTA";
        if (Operacion.equals("DEV") || Operacion.equals("DEVOLUCION")) {
            ret = "DEV";
        } else if (Operacion.equals("ANU") || Operacion.equals("ANULACION")) {
            ret = "ANU";
        } else if (Operacion.equals("DVA") || Operacion.equals("DEVOLUCIONAUTOMATICA")) {
            ret = "DVA";
        } else {
            ret = "VTA";
        }
        return ret;
    }

    void SetearTransaccion(Tarjetas objCore) {
        _fachada.SeteaConfig(objCore);
        if (objTransaccionComportamiento.getModificarCuotasSeteado()) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_MODIFICARCUOTAS, objTransaccionComportamiento.getModificarCuotas(), false);
        }
        if (objTransaccionComportamiento.getModificarDecretoLeySeteado()) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_MODIFICARDECRETOLEY, objTransaccionComportamiento.getModificarDecretoLey(), false);
        }
        if (objTransaccionComportamiento.getModificarFacturaSeteado()) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_MODIFICARFACTURA, objTransaccionComportamiento.getModificarFactura(), false);
        }
        if (objTransaccionComportamiento.getModificarMonedaSeteado()) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_MODIFICARMONEDA, objTransaccionComportamiento.getModificarMoneda(), false);
        }
        if (objTransaccionComportamiento.getModificarMontosSeteado()) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_MODIFICARMONTOS, objTransaccionComportamiento.getModificarMontos(), false);
        }
        if (objTransaccionComportamiento.getModificarPlanSeteado()) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_MODIFICARPLAN, objTransaccionComportamiento.getModificarPlan(), false);
        }
        if (objTransaccionComportamiento.getModificarTarjetaSeteado()) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_MODIFICARTARJETA, objTransaccionComportamiento.getModificarTarjeta(), false);
        }
        if (objTransaccionComportamiento.getModificarTipoCuentaSeteado()) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_MODIFICARTIPOCUENTA, objTransaccionComportamiento.getModificarTipoCuenta(), false);
        }
        if (EmpCodSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_EMPCOD, EmpCod, false);
        }
        if (TermCodSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_TERMCOD, TermCod, false);
        }

        if (MultiEmpSeteado) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP, MultiEmp, false);
        }
        if (OperacionSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_OPERACION, Operacion, false);
        }
        if (MonedaISOSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_MONEDA, MonedaISO, objTransaccionComportamiento.getModificarMoneda());
        }
        if (MontoSeteado) {
            objCore.SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_MONTO, Monto, objTransaccionComportamiento.getModificarMontos());
        }
         if (MontoTapSeteado) {
            objCore.SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_MONTO_TAP, MontoTap, false);
        }
        if (MontoPropinaSeteado) {
            objCore.SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_PROPINA, MontoPropina, objTransaccionComportamiento.getModificarMontos());
        }
        if (MontoCashBackSeteado) {
            objCore.SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CASHBACK, MontoCashBack, objTransaccionComportamiento.getModificarMontos());
        }
        if (FacturaNroSeteado) {
            objCore.SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_FACTURA, FacturaNro, objTransaccionComportamiento.getModificarFactura());
        }
        if (FacturaMontoSeteado) {
            objCore.SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_MONTOFACTURA, FacturaMonto, objTransaccionComportamiento.getModificarFactura());
        }
        if (FacturaMontoGravadoSeteado) {
            objCore.SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_MONTOGRAVADO, FacturaMontoGravado, objTransaccionComportamiento.getModificarFactura());
        }
        if (FacturaMontoIVASeteado) {
            objCore.SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_MONTOIVA, FacturaMontoIVA, objTransaccionComportamiento.getModificarFactura());
        }
        if (FacturaConsumidorFinalSeteado) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CONSUMIDORFINAL, FacturaConsumidorFinal, objTransaccionComportamiento.getModificarFactura());
        }
        if (TarjetaIdSeteado) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_ADQID, TarjetaId, false);
        }
        if (TarjetaTipoSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_TIPOTARJETA, TarjetaTipo, false);
        }
        if (TarjetaAlimentacionSeteado) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_TARJETAALIMENTACION, TarjetaAlimentacion, false);
        }
        if (EmisorIdSeteado) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_EMISORID, EmisorId, false);
        }
        if (TicketOriginalSeteado) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_TICKETORIGINAL, TicketOriginal, false);
        }

    }

    void SetearTransaccionExtendida(Tarjetas objCore) {
        if (objTransaccionExtendida.getCuotasSeteada()) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CUOTAS, objTransaccionExtendida.getCuotas(), false);
        }
        if (objTransaccionExtendida.getDecretoLeyIdSeteada()) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_DECRETOLEY, objTransaccionExtendida.getDecretoLeyId(), false);
        }
        if (objTransaccionExtendida.getPlanIdSeteada()) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_PLAN, objTransaccionExtendida.getPlanId(), false);
        }
          if (objTransaccionExtendida.getPlanVtaIdSeteada()) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_PLANVTAID, objTransaccionExtendida.getPlanVtaId(), false);
        }
        if (objTransaccionExtendida.getTarjetaCVCSeteada()) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CVC, objTransaccionExtendida.getTarjetaCVC(), false);
        }
        if (objTransaccionExtendida.getTarjetaControlSeteada()) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CONTROL, objTransaccionExtendida.getTarjetaControl(), false);
        }
        if (objTransaccionExtendida.getTarjetaDocIdentidadSeteada()) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_DOCIDENTIDAD, objTransaccionExtendida.getTarjetaDocIdentidad(), false);
        }
        if (objTransaccionExtendida.getTipoCuentaIdSeteada()) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_TIPOCUENTA, objTransaccionExtendida.getTipoCuentaId(), false);
        }
    }

    private void CargarRespTransaccion(Tarjetas objCore, TransaccionRespuesta objResp) throws Exception {
        objResp.setTransaccionId(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TRNID));
        objResp.setAprobada(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_APROBADA));
        objResp.setNroAutorizacion(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_NROAUTORIZACION));
        objResp.setCodRespuesta(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESPADQ));
        objResp.setMsgRespuesta(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        objResp.setLote(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_LOTE));
        objResp.setTicket(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TICKET));
        objResp.setTarjetaId(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_ADQID));
        objResp.setTarjetaTipo(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TARJETATIPO));
        objResp.setEsOffline(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_ESOFFLINE));
        objResp.setVoucher(ObtenerLineasVoucherByString(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DISVOUCHER)));
        objResp.setDatosTransaccion(CargarDatosTransaccion(objCore));

        objResp.setCodRespuestaComando(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP));
        objResp.setMsgRespuestaComando(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));

        objResp.getDevolucionAutomaticaRespuesta().setDevolucionAutoCodigo(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DVA_CODIGO));
        objResp.getDevolucionAutomaticaRespuesta().setDevolucionAutoMensaje(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DVA_MENSAJE));
    }

    private void CargarRespCargarDatosTarjeta(Tarjetas objCore, TransaccionDatosTarjeta objResp) throws Exception {
        objResp.setEmisorId(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_EMISORID));
        objResp.setTarjetaAlimentacion(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TARJETAALIMENTACION));
        objResp.setTarjetaId(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_ADQID));
        objResp.setTarjetaIIN(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PANIIN));
        objResp.setTarjetaLargo(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PANLARGO));
        objResp.setTarjetaPrepaga(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TARJETAPREPAGA));
        objResp.setTarjetaPrestaciones(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TARJETAPRESTACIONES));
        objResp.setTarjetaTipo(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TARJETATIPO));
        objResp.setTarjetaUlt4(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PAN4));
        objResp.setTarjetaNro(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PANENMASCARADO));
    }

    private List<String> ObtenerLineasVoucherByString(String Vaucher) {
        List<String> Lst = new ArrayList<String>();
        if (Vaucher.trim().equals("")) {
            return Lst;
        }
        String[] split = Vaucher.split("\n");
        for (String linea : split) {
            Lst.add(linea);
        }
        return Lst;
    }

    private TransaccionRespuestaDatosTransaccion CargarDatosTransaccion(Tarjetas objCore) throws Exception {
        TransaccionRespuestaDatosTransaccion objResp = new TransaccionRespuestaDatosTransaccion();
        objResp.setOperacion(ObtenerOperacion(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_OPERACION)));
        objResp.setMonedaISO(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MONEDA));
        objResp.setMonto(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MONTO));
        objResp.setMontoPropina(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PROPINA));
        objResp.setMontoCashBack(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CASHBACK));
        objResp.setCuotas(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CUOTAS));
        objResp.setTarjetaNro(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PAN));
        objResp.setTarjetaIIN(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PANIIN));
        objResp.setTarjetaExtranjera(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TARJETAEXTRANJERA));
        objResp.setTarjetaPrestaciones(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TARJETAPRESTACIONES));
        objResp.setTarjetaAlimentacion(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TARJETAALIMENTACION));
        objResp.setEmisorId(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_EMISORID));
        objResp.setDecretoLeyAplicado(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DECRETOLEYAPLICADO));
        objResp.setDecretoLeyNro(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DECRETOLEYNRO));
        objResp.setDecretoLeyMonto(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DECRETOLEYMONTO));
        objResp.setDatosTransaccionExtendida(ObtenerRespuestaDatosTransaccionExtendida(objCore));
        return objResp;
    }

    private TransaccionRespuestaDatosTransaccionExtendida ObtenerRespuestaDatosTransaccionExtendida(Tarjetas objCore) throws Exception {
        TransaccionRespuestaDatosTransaccionExtendida objResp = new TransaccionRespuestaDatosTransaccionExtendida();
        objResp.setTransaccionFechaHora(objCore.GetCampo_DateTime(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_FCHHORA));
        objResp.setEmpresaRUT(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_EMPRUT));
        objResp.setEmpresaNombre(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_EMPNOM));
        objResp.setSucursalNombre(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_SUCNOM));
        objResp.setSucursalDireccion(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_SUCDIR));
        objResp.setMerchantID(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MID));
        objResp.setTerminalID(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TID));
        objResp.setEmisorNombre(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_EMISORNOMBRE));
        objResp.setTarjetaMedio(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MEDIO));
        objResp.setTarjetaNombre(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_ADQNOM));
        objResp.setTarjetaTitular(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TITULAR));
        objResp.setTarjetaVencimiento(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_VENCIMIENTO));
        objResp.setTarjetaDocIdentidad(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DOCIDENTIDAD));
        objResp.setFacturaNro(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_FACTURA));
        objResp.setFacturaMonto(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MONTOFACTURA));
        objResp.setFacturaMontoGravado(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MONTOGRAVADO));
        objResp.setFacturaMontoGravadoTrn(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MONTOGRAVADOTRN));
        objResp.setFacturaMontoIVA(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MONTOIVA));
        objResp.setFacturaMontoIVATrn(objCore.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MONTOIVATRN));
        objResp.setDecretoLeyId(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DECRETOLEY));
        objResp.setDecretoLeyNom(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DECRETOLEYNOM));
        objResp.setDecretoLeyAdqId(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DECRETOLEYADQID));
        objResp.setDecretoLeyVoucher(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DECRETOLEYVOUCHER));
        objResp.setPlanId(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PLAN));
        objResp.setPlanVtaId(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PLANVTAID));
        objResp.setPlanNombre(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PLANNOM));
        objResp.setPlanNroPlan(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PLANADQPLAN));
        objResp.setPlanNroTipoPlan(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_PLANADQTIPOPLAN));
        objResp.setTipoCuentaId(objCore.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TIPOCUENTA));
        objResp.setTipoCuentaNombre(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TIPOCUENTANOM));
        objResp.setCuentaNro(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CUENTA));
        objResp.setEmvAppId(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_EMVAPPID));
        objResp.setEmvAppName(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_EMVAPPNAME));
        objResp.setFirmarVoucher(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_FIRMARVOUCHER));
        objResp.setTextoAdicional(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TEXTOADICIONAL));
        return objResp;
    }

    private void CargarRespuestaByException(Exception ex, TransaccionRespuesta objResp) {
        ex.printStackTrace();
        objResp.setAprobada(false);
        objResp.setCodRespuesta("TS");
        objResp.setMsgRespuesta(ex.getMessage().toUpperCase());
    }

    private boolean IntentoConsultarConfirmacionTransaccion(Long TransaccionId) {
        Boolean EstaConfirmada = false;
        int cantidadIntentos = 0;
        while (cantidadIntentos < 3) {
            try {
                ConsultarConfirmacionTransaccion(TransaccionId, EstaConfirmada);
                cantidadIntentos = 3;
            } catch (Exception ex) {
                cantidadIntentos++;
            }
        }
        return EstaConfirmada;
    }

    private boolean ConsultarConfirmacionTransaccion(Long TransaccionId, Boolean EstaConfirmada) throws Exception {
        Tarjetas objCore = null;
        try {
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SeteoOperacion(objCore);
            SetearTransaccion(objCore);
            SetearTransaccionExtendida(objCore);
            try {
                objCore.ConsultarConfirmacionTransaccion(TransaccionId);
                EstaConfirmada = objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CONFIRMADA);
                return true;
            } catch (Exception ex) {
                return false;
            }
        } catch (Exception ex) {
            throw ex;
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }
    }

}
