/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IAPIListener;
import com.uy.nad.transact4.api.ICierreLote;
import com.uy.nad.transact4.api.IDatosCierre;
import com.uy.nad.transact4.api.IDatosCierreExtendida;
import com.uy.nad.transact4.api.IDatosProducto;
import com.uy.nad.transact4.api.IDatosProductoMoneda;
import com.uy.nad.transact4.api.IDatosProductoMonedaPlan;
import com.uy.nad.transact4.api.IDatosProductoMonedaPlanDecretos;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Tarjetas;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class CierreLote implements ICierreLote {

    private TransActAPI_Tarjetas_v400Impl _fachada;
    private IAPIListener _listener;
    private Configuracion _configuracion;
    //-------------- DATOS
    private String EmpCod;
    private Boolean EmpCodSeteado;
    private String TermCod;
    private Boolean TermCodSeteado;
    private Integer MultiEmp;
    private Boolean MultiEmpSeteado;
    private Integer ProcesadorId;
    private Boolean ProcesadorIdSeteado;
    private Boolean CierreCentralizado;
    private Boolean CierreCentralizadoSeteado;
    private String EmpHash;
    private Boolean EmpHashSeteado;
    private CierreLoteComportamiento Comportamiento;
    private CierreLoteRespuesta Respuesta;

    public CierreLote(TransActAPI_Tarjetas_v400Impl _fachada, IAPIListener _listener, Configuracion _configuracion) {
        this._fachada = _fachada;
        this._listener = _listener;
        this._configuracion = _configuracion;
        this.Comportamiento = new CierreLoteComportamiento();
        this.Respuesta = new CierreLoteRespuesta();
        this.EmpCodSeteado = false;
        this.TermCodSeteado = false;
        this.MultiEmpSeteado = false;
        this.ProcesadorIdSeteado = false;
        this.CierreCentralizadoSeteado = false;
        this.EmpHashSeteado = false;
    }

    @Override
    public void setEmpCod(String EmpCod) {
        this.EmpCod = EmpCod;
        this.EmpCodSeteado = true;
    }

    @Override
    public void setTermCod(String TermCod) {
        this.TermCod = TermCod;
        this.TermCodSeteado = true;
    }

    @Override
    public void setMultiEmp(Integer MultiEmp) {
        this.MultiEmp = MultiEmp;
        this.MultiEmpSeteado = true;
    }

    @Override
    public void setProcesadorId(Integer ProcesadorId) {
        this.ProcesadorId = ProcesadorId;
        this.ProcesadorIdSeteado = true;
    }

    @Override
    public void setCierreCentralizado(Boolean CierreCentralizado) {
        this.CierreCentralizado = CierreCentralizado;
        this.CierreCentralizadoSeteado = true;
    }

    @Override
    public void setEmpHash(String EmpHash) {
        this.EmpHash = EmpHash;
        this.EmpHashSeteado = true;
    }

    @Override
    public CierreLoteComportamiento getComportamiento() {
        return Comportamiento;
    }

    @Override
    public CierreLoteRespuesta getRespuesta() {
        return Respuesta;
    }

//----------------------------------------------------------
    public String getEmpCod() {
        return EmpCod;
    }

    public String getTermCod() {
        return TermCod;
    }

    public Integer getMultiEmp() {
        return MultiEmp;
    }

    public Integer getProcesadorId() {
        return ProcesadorId;
    }

    public Boolean getCierreCentralizado() {
        return CierreCentralizado;
    }

    public String getEmpHash() {
        return EmpHash;
    }
//--------------------- METODOS

    @Override
    public void ProcesarCierre() throws Exception {
        Tarjetas objCore = null;
        try {

            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            LimpiarCierre();
            SetearCierre(objCore);
            objCore.ProcesarCierre();
            CargarRespProcesarCierre(objCore);
        } catch (Exception ex) {
            CargarRespuestaByException(ex);
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }
    }

    @Override
    public Boolean ConsultarEstadoCierre(String TokenCierre) throws Exception {
        Tarjetas objCore = null;
        try {
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SetearCierre(objCore);
            objCore.ConsultarEstadoCierre(TokenCierre);
            CargarRespConsultarCierre(objCore);
            if (Respuesta.getFinalizado()) {
                LimpiarParamsCierre();
            }
            return true;
        } catch (Exception ex) {
            CargarRespuestaByException(ex);
            return false;
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }
    }

    //PRIVADOS
    private void LimpiarCierre() {
        Respuesta.Limpiar();
    }

    private void SetearCierre(Tarjetas objCore) {
        _fachada.SeteaConfig(objCore);
        if (Comportamiento.getModificarProcesadorIdSeteado()) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_MODIFICARCIERRE, Comportamiento.getModificarProcesadorId(), false);
        }
        if (EmpCodSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD, EmpCod, false);
        }
        if (TermCodSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD, TermCod, false);
        }
        if (MultiEmpSeteado) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_MULTIEMP, MultiEmp, false);
        }
        if (ProcesadorIdSeteado) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_PROCESADORID, ProcesadorId, false);
        }
        if (CierreCentralizadoSeteado) {
            objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_CIERRECENTRALIZADO, CierreCentralizado, false);
        }
        if (EmpHashSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPHASH, EmpHash, false);
        }
    }

    private void CargarRespProcesarCierre(Tarjetas objCore) throws Exception {
        Respuesta.Limpiar();
        Respuesta.setFinalizado(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_FINALIZADO));
        Respuesta.setEstado(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ESTADO));
        Respuesta.setTokenCierre(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TOKENCIERRE));
        Respuesta.setVoucher(ObtenerLineasVoucherByString(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_DISVOUCHER)));

    }

    private void CargarRespuestaByException(Exception ex) {
        Respuesta.Limpiar();
        Respuesta.setFinalizado(true);
        Respuesta.setEstado(ex.getMessage().toUpperCase());

    }

    private List<String> ObtenerLineasVoucherByString(String Voucher) {
        if (Voucher.isEmpty()) {
            return new ArrayList<String>();
        }
        List<String> lista = new ArrayList<String>(Arrays.asList(Voucher.split("\n")));
        return lista;
    }

    private void CargarRespConsultarCierre(Tarjetas objCore) throws Exception {
        Respuesta.Limpiar();
        Respuesta.setFinalizado(objCore.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_FINALIZADO));
        Respuesta.setEstado(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ESTADO));
        Respuesta.setVoucher(ObtenerLineasVoucherByString(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_DISVOUCHER)));
        for (com.uy.nad.transact4.api.internal.core.Transaccion trnColeccion : objCore.GetCampo_Coleccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_DATOSCIERRE)) {
            Respuesta.getDatosCierre().add(ObtenerRespCierreDatosCierre(trnColeccion));
        }
    }

    private void LimpiarParamsCierre() {
        EmpCod = "";
        EmpCodSeteado = false;
        TermCod = "";
        TermCodSeteado = false;
        MultiEmp = 0;
        MultiEmpSeteado = false;
        ProcesadorId = 0;
        ProcesadorIdSeteado = false;
        CierreCentralizado = false;
        CierreCentralizadoSeteado = false;
        EmpHash = "";
        EmpHashSeteado = false;
    }

    private IDatosCierre ObtenerRespCierreDatosCierre(com.uy.nad.transact4.api.internal.core.Transaccion trnColeccion) throws Exception {
        DatosCierre objResp = new DatosCierre();
        objResp.setProcesadorId(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PROCESADORID));
        objResp.setLote(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_LOTE));
        objResp.setAprobado(trnColeccion.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_APROBADO));
        objResp.setNroAutorizacion(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_NROAUTORIZACION));
        objResp.setCodRespuesta(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CODRESPADQ));
        objResp.setMsgRespuesta(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MSGRESP));
        objResp.setExtendida(ObtenerRespCierreDatosCierreExtendida(trnColeccion));
        return objResp;
    }

    private IDatosCierreExtendida ObtenerRespCierreDatosCierreExtendida(Transaccion trnColeccion) throws Exception {
        DatosCierreExtendida objResp = new DatosCierreExtendida();
        objResp.setCierreFechaHora(trnColeccion.GetCampo_DateTime(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_FCHHORA));
        objResp.setEmpresaRUT(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_EMPRUT));
        objResp.setEmpresaNombre(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_EMPNOM));
        objResp.setSucursalNombre(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUCNOM));
        objResp.setSucursalDireccion(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUCDIR));
        objResp.setMerchantID(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MID));
        objResp.setTerminalID(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TID));
        objResp.setCantVenta(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTVENTA));
        objResp.setMontoVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOVENTA));
        objResp.setCantReversoVenta(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTREVERSOVENTA));
        objResp.setMontoReversoVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOREVERSOVENTA));
        objResp.setCantAnulacion(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTANULACION));
        objResp.setMontoAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOANULACION));
        objResp.setCantReversoAnulacion(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTREVERSOANULACION));
        objResp.setMontoReversoAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOREVERSOANULACION));
        objResp.setCantDevolucion(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTDEVOLUCION));
        objResp.setMontoDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTODEVOLUCION));
        objResp.setCantReversoDevolucion(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTREVERSODEVOLUCION));
        objResp.setMontoReversoDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOREVERSODEVOLUCION));

        for (com.uy.nad.transact4.api.internal.core.Transaccion productoCierre : trnColeccion.GetCampo_Coleccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PRODUCTOSCIERRE)) {
            objResp.getProductos().add(ObtenerRespCierreDatosCierreExtendidaProducto(productoCierre));
        }

        return objResp;
    }

    IDatosProducto ObtenerRespCierreDatosCierreExtendidaProducto(Transaccion trnColeccion) throws Exception {
        DatosProducto objResp = new DatosProducto();

        objResp.setTarjetaId(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ADQID));
        objResp.setTarjetaNombre(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ADQNOM));
        objResp.setProductoId(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ADQPRODID));
        objResp.setProductoNombre(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ADQPRODNOM));
        objResp.setTarjetaTipo(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TARJETATIPO));
        objResp.setTarjetaPrestaciones(trnColeccion.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TARJETAPRESTACIONES));
        objResp.setTarjetaAlimentacion(trnColeccion.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TARJETAALIMENTACION));
        for (com.uy.nad.transact4.api.internal.core.Transaccion monedaCierre : trnColeccion.GetCampo_Coleccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONEDASCIERRE)) {
            objResp.getMonedas().add(ObtenerRespCierreDatosCierreExtendidaMoneda(monedaCierre));
        }
        return objResp;
    }

    IDatosProductoMoneda ObtenerRespCierreDatosCierreExtendidaMoneda(Transaccion trnColeccion) throws Exception {
        DatosProductoMoneda objResp = new DatosProductoMoneda();
        objResp.setMonedaISO(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONEDA));
        objResp.setSubTotalCantMoneda(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALCANTMONEDA));
        objResp.setSubTotalMontoMoneda(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALMONTOMONEDA));
        for (com.uy.nad.transact4.api.internal.core.Transaccion planCierre : trnColeccion.GetCampo_Coleccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANESCIERRE)) {
            objResp.getPlanes().add(ObtenerRespCierreDatosCierreExtendidaPlan(planCierre));
        }
        return objResp;
    }

    IDatosProductoMonedaPlan ObtenerRespCierreDatosCierreExtendidaPlan(Transaccion trnColeccion) throws Exception {
        DatosProductoMonedaPlan objResp = new DatosProductoMonedaPlan();
        objResp.setPlanId(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANID));
        objResp.setPlanNombre(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANNOMBRE));
        objResp.setPlanNroPlan(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANADQPLAN));
        objResp.setPlanNroTipoPlan(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANADQTIPOPLAN));
        objResp.setMerchantID(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MID));
        objResp.setSubTotalCantPlan(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALCANTPLAN));
        objResp.setSubTotalMontoPlan(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALMONTOPLAN));
        objResp.setSubTotalDecretoLeyPlan(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALDECRETOLEYPLAN));

        for (com.uy.nad.transact4.api.internal.core.Transaccion decretoLeyCierre : trnColeccion.GetCampo_Coleccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_NACIONALESCIERRE)) {
            objResp.getNacionales().add(ObtenerRespCierreDatosCierreExtendidaDecretoLey(decretoLeyCierre));
        }
        for (com.uy.nad.transact4.api.internal.core.Transaccion decretoLeyCierre : trnColeccion.GetCampo_Coleccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_EXTRANJERASCIERRE)) {
            objResp.getExtranjeras().add(ObtenerRespCierreDatosCierreExtendidaDecretoLey(decretoLeyCierre));
        }
        return objResp;
    }

    IDatosProductoMonedaPlanDecretos ObtenerRespCierreDatosCierreExtendidaDecretoLey(Transaccion trnColeccion) throws Exception {
        DatosProductoMonedaPlanDecretos objResp = new DatosProductoMonedaPlanDecretos();
        objResp.setDecretoLeyAplicado(trnColeccion.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_DECRETOLEYAPLICADO));
        objResp.setDecretoLeyNro(trnColeccion.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_DECRETOLEYNRO));
        objResp.setSubTotalCantDecreto(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALCANTDECRETO));
        objResp.setSubTotalMontoDecreto(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALMONTODECRETO));
        objResp.setVenta(ObtenerRespCierreDatosCierreExtendidaDecretoLeyVenta(trnColeccion));
        objResp.setAnulacion(ObtenerRespCierreDatosCierreExtendidaDecretoLeyAnulacion(trnColeccion));
        objResp.setDevolucion(ObtenerRespCierreDatosCierreExtendidaDecretoLeyDevolucion(trnColeccion));
        return objResp;
    }

    private DatosProductoMonedaPlanDecretosVentas ObtenerRespCierreDatosCierreExtendidaDecretoLeyVenta(Transaccion trnColeccion) throws Exception {
        DatosProductoMonedaPlanDecretosVentas objResp = new DatosProductoMonedaPlanDecretosVentas();
        objResp.setCantVenta(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTVENTA));
        objResp.setMontoVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOVENTA));
        objResp.setMontoPropinaVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAVENTA));
        objResp.setMontoCashbackVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKVENTA));
        objResp.setMontoDecretoLeyVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYVENTA));
        objResp.setCantReversoVenta(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTREVERSOVENTA));
        objResp.setMontoReversoVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOREVERSOVENTA));
        objResp.setMontoPropinaReversoVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAREVERSOVENTA));
        objResp.setMontoCashbackReversoVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKREVERSOVENTA));
        objResp.setMontoDecretoLeyReversoVenta(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYREVERSOVENTA));
        return objResp;
    }

    private DatosProductoMonedaPlanDecretosAnulaciones ObtenerRespCierreDatosCierreExtendidaDecretoLeyAnulacion(Transaccion trnColeccion) throws Exception {
        DatosProductoMonedaPlanDecretosAnulaciones objResp = new DatosProductoMonedaPlanDecretosAnulaciones();
        objResp.setCantAnulacion(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTANULACION));
        objResp.setMontoAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOANULACION));
        objResp.setMontoPropinaAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAANULACION));
        objResp.setMontoCashbackAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKANULACION));
        objResp.setMontoDecretoLeyAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYANULACION));
        objResp.setCantReversoAnulacion(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTREVERSOANULACION));
        objResp.setMontoReversoAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOREVERSOANULACION));
        objResp.setMontoPropinaReversoAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAREVERSOANULACION));
        objResp.setMontoCashbackReversoAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKREVERSOANULACION));
        objResp.setMontoDecretoLeyReversoAnulacion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYREVERSOANULACION));
        return objResp;
    }

    private DatosProductoMonedaPlanDecretosDevoluciones ObtenerRespCierreDatosCierreExtendidaDecretoLeyDevolucion(Transaccion trnColeccion) throws Exception {
        DatosProductoMonedaPlanDecretosDevoluciones objResp = new DatosProductoMonedaPlanDecretosDevoluciones();
        objResp.setCantDevolucion(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTDEVOLUCION));
        objResp.setMontoDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODEVOLUCION));
        objResp.setMontoPropinaDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINADEVOLUCION));
        objResp.setMontoCashbackDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKDEVOLUCION));
        objResp.setMontoDecretoLeyDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYDEVOLUCION));
        objResp.setCantReversoDevolucion(trnColeccion.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTREVERSODEVOLUCION));
        objResp.setMontoReversoDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOREVERSODEVOLUCION));
        objResp.setMontoPropinaReversoDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAREVERSODEVOLUCION));
        objResp.setMontoCashbackReversoDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKREVERSODEVOLUCION));
        objResp.setMontoDecretoLeyReversoDevolucion(trnColeccion.GetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYREVERSODEVOLUCION));
        return objResp;
    }

}
