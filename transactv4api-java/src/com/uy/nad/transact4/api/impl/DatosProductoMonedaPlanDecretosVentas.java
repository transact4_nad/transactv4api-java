/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IDatosProductoMonedaPlanDecretosVentas;

/**
 *
 * @author yunior
 */
public class DatosProductoMonedaPlanDecretosVentas implements IDatosProductoMonedaPlanDecretosVentas {

    private Integer CantVenta;
    private Long MontoVenta;
    private Long MontoPropinaVenta;
    private Long MontoCashbackVenta;
    private Long MontoDecretoLeyVenta;
    private Integer CantReversoVenta;
    private Long MontoReversoVenta;
    private Long MontoPropinaReversoVenta;
    private Long MontoCashbackReversoVenta;
    private Long MontoDecretoLeyReversoVenta;

    @Override
    public Integer getCantVenta() {
        return CantVenta;
    }

    @Override
    public Long getMontoVenta() {
        return MontoVenta;
    }

    @Override
    public Long getMontoPropinaVenta() {
        return MontoPropinaVenta;
    }

    @Override
    public Long getMontoCashbackVenta() {
        return MontoCashbackVenta;
    }

    @Override
    public Long getMontoDecretoLeyVenta() {
        return MontoDecretoLeyVenta;
    }

    @Override
    public Integer getCantReversoVenta() {
        return CantReversoVenta;
    }

    @Override
    public Long getMontoReversoVenta() {
        return MontoReversoVenta;
    }

    @Override
    public Long getMontoPropinaReversoVenta() {
        return MontoPropinaReversoVenta;
    }

    @Override
    public Long getMontoCashbackReversoVenta() {
        return MontoCashbackReversoVenta;
    }

    @Override
    public Long getMontoDecretoLeyReversoVenta() {
        return MontoDecretoLeyReversoVenta;
    }
    
    //-------------------

    public void setCantVenta(Integer CantVenta) {
        this.CantVenta = CantVenta;
    }

    public void setMontoVenta(Long MontoVenta) {
        this.MontoVenta = MontoVenta;
    }

    public void setMontoPropinaVenta(Long MontoPropinaVenta) {
        this.MontoPropinaVenta = MontoPropinaVenta;
    }

    public void setMontoCashbackVenta(Long MontoCashbackVenta) {
        this.MontoCashbackVenta = MontoCashbackVenta;
    }

    public void setMontoDecretoLeyVenta(Long MontoDecretoLeyVenta) {
        this.MontoDecretoLeyVenta = MontoDecretoLeyVenta;
    }

    public void setCantReversoVenta(Integer CantReversoVenta) {
        this.CantReversoVenta = CantReversoVenta;
    }

    public void setMontoReversoVenta(Long MontoReversoVenta) {
        this.MontoReversoVenta = MontoReversoVenta;
    }

    public void setMontoPropinaReversoVenta(Long MontoPropinaReversoVenta) {
        this.MontoPropinaReversoVenta = MontoPropinaReversoVenta;
    }

    public void setMontoCashbackReversoVenta(Long MontoCashbackReversoVenta) {
        this.MontoCashbackReversoVenta = MontoCashbackReversoVenta;
    }

    public void setMontoDecretoLeyReversoVenta(Long MontoDecretoLeyReversoVenta) {
        this.MontoDecretoLeyReversoVenta = MontoDecretoLeyReversoVenta;
    }
    

}
