/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IDatosProductoMonedaPlanDecretosAnulaciones;

/**
 *
 * @author yunior
 */
public class DatosProductoMonedaPlanDecretosAnulaciones implements IDatosProductoMonedaPlanDecretosAnulaciones {

    private Integer CantAnulacion;
    private Long MontoAnulacion;
    private Long MontoPropinaAnulacion;
    private Long MontoCashbackAnulacion;
    private Long MontoDecretoLeyAnulacion;
    private Integer CantReversoAnulacion;
    private Long MontoReversoAnulacion;
    private Long MontoPropinaReversoAnulacion;
    private Long MontoCashbackReversoAnulacion;
    private Long MontoDecretoLeyReversoAnulacion;

    @Override
    public Integer getCantAnulacion() {
        return CantAnulacion;
    }

    @Override
    public Long getMontoAnulacion() {
        return MontoAnulacion;
    }

    @Override
    public Long getMontoPropinaAnulacion() {
        return MontoPropinaAnulacion;
    }

    @Override
    public Long getMontoCashbackAnulacion() {
        return MontoCashbackAnulacion;
    }

    @Override
    public Long getMontoDecretoLeyAnulacion() {
        return MontoDecretoLeyAnulacion;
    }

    @Override
    public Integer getCantReversoAnulacion() {
        return CantReversoAnulacion;
    }

    @Override
    public Long getMontoReversoAnulacion() {
        return MontoReversoAnulacion;
    }

    @Override
    public Long getMontoPropinaReversoAnulacion() {
        return MontoPropinaReversoAnulacion;
    }

    @Override
    public Long getMontoCashbackReversoAnulacion() {
        return MontoCashbackReversoAnulacion;
    }

    @Override
    public Long getMontoDecretoLeyReversoAnulacion() {
        return MontoDecretoLeyReversoAnulacion;
    }
    
//---------------------------------------

    public void setCantAnulacion(Integer CantAnulacion) {
        this.CantAnulacion = CantAnulacion;
    }

    public void setMontoAnulacion(Long MontoAnulacion) {
        this.MontoAnulacion = MontoAnulacion;
    }

    public void setMontoPropinaAnulacion(Long MontoPropinaAnulacion) {
        this.MontoPropinaAnulacion = MontoPropinaAnulacion;
    }

    public void setMontoCashbackAnulacion(Long MontoCashbackAnulacion) {
        this.MontoCashbackAnulacion = MontoCashbackAnulacion;
    }

    public void setMontoDecretoLeyAnulacion(Long MontoDecretoLeyAnulacion) {
        this.MontoDecretoLeyAnulacion = MontoDecretoLeyAnulacion;
    }

    public void setCantReversoAnulacion(Integer CantReversoAnulacion) {
        this.CantReversoAnulacion = CantReversoAnulacion;
    }

    public void setMontoReversoAnulacion(Long MontoReversoAnulacion) {
        this.MontoReversoAnulacion = MontoReversoAnulacion;
    }

    public void setMontoPropinaReversoAnulacion(Long MontoPropinaReversoAnulacion) {
        this.MontoPropinaReversoAnulacion = MontoPropinaReversoAnulacion;
    }

    public void setMontoCashbackReversoAnulacion(Long MontoCashbackReversoAnulacion) {
        this.MontoCashbackReversoAnulacion = MontoCashbackReversoAnulacion;
    }

    public void setMontoDecretoLeyReversoAnulacion(Long MontoDecretoLeyReversoAnulacion) {
        this.MontoDecretoLeyReversoAnulacion = MontoDecretoLeyReversoAnulacion;
    }
    
    
}
