/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IDatosProductoMoneda;
import com.uy.nad.transact4.api.IDatosProductoMonedaPlan;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yunior
 */
public class DatosProductoMoneda implements IDatosProductoMoneda {

    private String MonedaISO;
    private Integer SubTotalCantMoneda;
    private Long SubTotalMontoMoneda;
    private List<IDatosProductoMonedaPlan> Planes;

    public DatosProductoMoneda() {
        Planes = new ArrayList<IDatosProductoMonedaPlan>();
    }
    
@Override
    public String getMonedaISO() {
        return MonedaISO;
    }
@Override
    public Integer getSubTotalCantMoneda() {
        return SubTotalCantMoneda;
    }
@Override
    public Long getSubTotalMontoMoneda() {
        return SubTotalMontoMoneda;
    }
@Override
    public List<IDatosProductoMonedaPlan> getPlanes() {
        return Planes;
    }
//------------------------------------------------------------
    public void setMonedaISO(String MonedaISO) {
        this.MonedaISO = MonedaISO;
    }

    public void setSubTotalCantMoneda(Integer SubTotalCantMoneda) {
        this.SubTotalCantMoneda = SubTotalCantMoneda;
    }

    public void setSubTotalMontoMoneda(Long SubTotalMontoMoneda) {
        this.SubTotalMontoMoneda = SubTotalMontoMoneda;
    }

    public void setPlanes(List<IDatosProductoMonedaPlan> Planes) {
        this.Planes = Planes;
    }

    
    
}
