/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IDatosCierre;
import com.uy.nad.transact4.api.IDatosCierreExtendida;

/**
 *
 * @author yunior
 */
public class DatosCierre implements IDatosCierre{

    private Integer ProcesadorId;
    private Long Lote;
    private String NroAutorizacion;
    private Boolean Aprobado;
    private String CodRespuesta;
    private String MsgRespuesta;
    private IDatosCierreExtendida Extendida;
    
@Override
    public Integer getProcesadorId() {
        return ProcesadorId;
    }
@Override
    public Long getLote() {
        return Lote;
    }
@Override
    public String getNroAutorizacion() {
        return NroAutorizacion;
    }
@Override
    public Boolean getAprobado() {
        return Aprobado;
    }
@Override
    public String getCodRespuesta() {
        return CodRespuesta;
    }
@Override
    public String getMsgRespuesta() {
        return MsgRespuesta;
    }
@Override
    public IDatosCierreExtendida getExtendida() {
        return Extendida;
    }
//-------------------------------------------------------------
    public void setProcesadorId(Integer ProcesadorId) {
        this.ProcesadorId = ProcesadorId;
    }

    public void setLote(Long Lote) {
        this.Lote = Lote;
    }

    public void setNroAutorizacion(String NroAutorizacion) {
        this.NroAutorizacion = NroAutorizacion;
    }

    public void setAprobado(Boolean Aprobado) {
        this.Aprobado = Aprobado;
    }

    public void setCodRespuesta(String CodRespuesta) {
        this.CodRespuesta = CodRespuesta;
    }

    public void setMsgRespuesta(String MsgRespuesta) {
        this.MsgRespuesta = MsgRespuesta;
    }

    public void setExtendida(IDatosCierreExtendida Extendida) {
        this.Extendida = Extendida;
    }
    
    
    
}
