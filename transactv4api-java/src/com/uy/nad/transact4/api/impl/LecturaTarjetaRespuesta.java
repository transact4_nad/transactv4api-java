/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.ILecturaTarjetaRespuesta;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public final class LecturaTarjetaRespuesta implements ILecturaTarjetaRespuesta {

    private String CodRespuesta;
    private String MsgRespuesta;
    private String Track2;
    private String Track1;
    private String Track3;

    public LecturaTarjetaRespuesta() {
        Limpiar();
    }

    @Override
    public String getCodRespuesta() {
        return CodRespuesta;
    }

    @Override
    public String getMsgRespuesta() {
        return MsgRespuesta;
    }

    @Override
    public String getTrack2() {
        return Track2;
    }

    @Override
    public String getTrack1() {
        return Track1;
    }

    @Override
    public String getTrack3() {
        return Track3;
    }

    public void setCodRespuesta(String CodRespuesta) {
        this.CodRespuesta = CodRespuesta;
    }

    public void setMsgRespuesta(String MsgRespuesta) {
        this.MsgRespuesta = MsgRespuesta;
    }

    public void setTrack2(String Track2) {
        this.Track2 = Track2;
    }

    public void setTrack1(String Track1) {
        this.Track1 = Track1;
    }

    public void setTrack3(String Track3) {
        this.Track3 = Track3;
    }

    void Limpiar() {
        this.CodRespuesta = "";
        this.MsgRespuesta = "";
        this.Track2 = "";
        this.Track1 = "";
        this.Track3 = "";
    }
}
