/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IAPIListener;
import com.uy.nad.transact4.api.IImpresion;
import com.uy.nad.transact4.api.IImpresionRespuesta;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Tarjetas;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class Impresion implements IImpresion {

    private final TransActAPI_Tarjetas_v400Impl _fachada;
    private final IAPIListener _listener;
    private final Configuracion _configuracion;

    public Impresion(TransActAPI_Tarjetas_v400Impl _fachada, IAPIListener _listener, Configuracion configuracion) {
        this._fachada = _fachada;
        this._listener = _listener;
        this._configuracion = configuracion;
    }
    private String EmpCod;
    private Boolean EmpCodSeteado = false;
    private String TermCod;
    private Boolean TermCodSeteado = false;

    private IImpresionRespuesta respuesta;

    public String getEmpCod() {
        return EmpCod;
    }

    public String getTermCod() {
        return TermCod;
    }

   
    @Override
    public void setEmpCod(String EmpCod) {
        this.EmpCod = EmpCod;
        EmpCodSeteado = true;

    }

    @Override
    public void setTermCod(String TermCod) {
        this.TermCod = TermCod;
        TermCodSeteado = true;
                
    }

    public void setIRespuesta(IImpresionRespuesta respuesta) {
        this.respuesta = respuesta;
    }

    @Override
    public Boolean ReImprimir() {
        Tarjetas objCore = null;
        Boolean resultado = false;
        try {
            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            SetearTransaccion(objCore);
            objCore.Reimprimir();
            CargarRespReimpresionOK();
              resultado = true;
        } catch (Exception ex) {
            CargarRespuestaByException(ex, respuesta);
            resultado = false;
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }
        return resultado;
    }

    @Override
    public IImpresionRespuesta getRespuesta() {
        return respuesta;
    }

    void SetearTransaccion(Tarjetas objCore) {
        _fachada.SeteaConfig(objCore);
        if (EmpCodSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_EMPCOD, EmpCod, false);
        }
        if (TermCodSeteado) 
        {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_TERMCOD, TermCod, false);
        }
    }
      private void  CargarRespReimpresionOK(){
           ImpresionRespuesta objResp = new ImpresionRespuesta();
           objResp.setMensaje("");
            respuesta = objResp;
      }
       private void CargarRespuestaByException(Exception ex, IImpresionRespuesta objResp) {
        ((ImpresionRespuesta)objResp).setAprobado(false);
        ((ImpresionRespuesta)objResp).setMensaje(ex.getMessage());
         ex.printStackTrace();

    }
      
}
