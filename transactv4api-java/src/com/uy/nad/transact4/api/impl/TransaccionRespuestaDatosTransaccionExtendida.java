/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.ITransaccionRespuestaDatosTransaccionExtendida;
import java.util.Date;

/**
 *
 * @author yunior
 */
public class TransaccionRespuestaDatosTransaccionExtendida implements ITransaccionRespuestaDatosTransaccionExtendida {

    private Date TransaccionFechaHora;
    private String EmpresaRUT;
    private String EmpresaNombre;
    private String SucursalNombre;
    private String SucursalDireccion;
    private String MerchantID;
    private String TerminalID;

    private String EmisorNombre;
    private String TarjetaMedio;
    private String TarjetaNombre;
    private String TarjetaTitular;
    private String TarjetaVencimiento;
    private String TarjetaDocIdentidad;

    private Long FacturaNro;
    private Long FacturaMonto;
    private Long FacturaMontoGravado;
    private Long FacturaMontoGravadoTrn;
    private Long FacturaMontoIVA;
    private Long FacturaMontoIVATrn;

    private Integer DecretoLeyId;
    private String DecretoLeyNom;
    private String DecretoLeyAdqId;
    private String DecretoLeyVoucher;

    private Integer PlanId;
    private Integer PlanVtaId;
    private String PlanNombre;
    private Integer PlanNroPlan;
    private Integer PlanNroTipoPlan;

    private Integer TipoCuentaId;
    private String TipoCuentaNombre;
    private String CuentaNro;

    private String EmvAppId;
    private String EmvAppName;

    private Boolean FirmarVoucher;
    private String TextoAdicional;

    @Override
    public Date getTransaccionFechaHora() {
        return TransaccionFechaHora;
    }

    @Override
    public String getEmpresaRUT() {
        return EmpresaRUT;
    }

    @Override
    public String getEmpresaNombre() {
        return EmpresaNombre;
    }

    @Override
    public String getSucursalNombre() {
        return SucursalNombre;
    }

    @Override
    public String getSucursalDireccion() {
        return SucursalDireccion;
    }

    @Override
    public String getMerchantID() {
        return MerchantID;
    }

    @Override
    public String getTerminalID() {
        return TerminalID;
    }

    @Override
    public String getEmisorNombre() {
        return EmisorNombre;
    }

    @Override
    public String getTarjetaMedio() {
        return TarjetaMedio;
    }

    @Override
    public String getTarjetaNombre() {
        return TarjetaNombre;
    }

    @Override
    public String getTarjetaTitular() {
        return TarjetaTitular;
    }

    @Override
    public String getTarjetaVencimiento() {
        return TarjetaVencimiento;
    }

    @Override
    public String getTarjetaDocIdentidad() {
        return TarjetaDocIdentidad;
    }

    @Override
    public Long getFacturaNro() {
        return FacturaNro;
    }

    @Override
    public Long getFacturaMonto() {
        return FacturaMonto;
    }

    @Override
    public Long getFacturaMontoGravado() {
        return FacturaMontoGravado;
    }

    @Override
    public Long getFacturaMontoGravadoTrn() {
        return FacturaMontoGravadoTrn;
    }

    @Override
    public Long getFacturaMontoIVA() {
        return FacturaMontoIVA;
    }

    @Override
    public Long getFacturaMontoIVATrn() {
        return FacturaMontoIVATrn;
    }

    @Override
    public Integer getDecretoLeyId() {
        return DecretoLeyId;
    }

    @Override
    public String getDecretoLeyNom() {
        return DecretoLeyNom;
    }

    @Override
    public String getDecretoLeyAdqId() {
        return DecretoLeyAdqId;
    }

    @Override
    public String getDecretoLeyVoucher() {
        return DecretoLeyVoucher;
    }

    @Override
    public Integer getPlanId() {
        return PlanId;
    }
    @Override
    public Integer getPlanVtaId() {
        return PlanVtaId;
    }

    @Override
    public String getPlanNombre() {
        return PlanNombre;
    }

    @Override
    public Integer getPlanNroPlan() {
        return PlanNroPlan;
    }

    @Override
    public Integer getPlanNroTipoPlan() {
        return PlanNroTipoPlan;
    }

    @Override
    public Integer getTipoCuentaId() {
        return TipoCuentaId;
    }

    @Override
    public String getTipoCuentaNombre() {
        return TipoCuentaNombre;
    }

    @Override
    public String getCuentaNro() {
        return CuentaNro;
    }

    @Override
    public String getEmvAppId() {
        return EmvAppId;
    }

    @Override
    public String getEmvAppName() {
        return EmvAppName;
    }

    @Override
    public Boolean getFirmarVoucher() {
        return FirmarVoucher;
    }

    @Override
    public String getTextoAdicional() {
        return TextoAdicional;
    }
    
    
    //--------------------------

    public void setTransaccionFechaHora(Date TransaccionFechaHora) {
        this.TransaccionFechaHora = TransaccionFechaHora;
    }

    public void setEmpresaRUT(String EmpresaRUT) {
        this.EmpresaRUT = EmpresaRUT;
    }

    public void setEmpresaNombre(String EmpresaNombre) {
        this.EmpresaNombre = EmpresaNombre;
    }

    public void setSucursalNombre(String SucursalNombre) {
        this.SucursalNombre = SucursalNombre;
    }

    public void setSucursalDireccion(String SucursalDireccion) {
        this.SucursalDireccion = SucursalDireccion;
    }

    public void setMerchantID(String MerchantID) {
        this.MerchantID = MerchantID;
    }

    public void setTerminalID(String TerminalID) {
        this.TerminalID = TerminalID;
    }

    public void setEmisorNombre(String EmisorNombre) {
        this.EmisorNombre = EmisorNombre;
    }

    public void setTarjetaMedio(String TarjetaMedio) {
        this.TarjetaMedio = TarjetaMedio;
    }

    public void setTarjetaNombre(String TarjetaNombre) {
        this.TarjetaNombre = TarjetaNombre;
    }

    public void setTarjetaTitular(String TarjetaTitular) {
        this.TarjetaTitular = TarjetaTitular;
    }

    public void setTarjetaVencimiento(String TarjetaVencimiento) {
        this.TarjetaVencimiento = TarjetaVencimiento;
    }

    public void setTarjetaDocIdentidad(String TarjetaDocIdentidad) {
        this.TarjetaDocIdentidad = TarjetaDocIdentidad;
    }

    public void setFacturaNro(Long FacturaNro) {
        this.FacturaNro = FacturaNro;
    }

    public void setFacturaMonto(Long FacturaMonto) {
        this.FacturaMonto = FacturaMonto;
    }

    public void setFacturaMontoGravado(Long FacturaMontoGravado) {
        this.FacturaMontoGravado = FacturaMontoGravado;
    }

    public void setFacturaMontoGravadoTrn(Long FacturaMontoGravadoTrn) {
        this.FacturaMontoGravadoTrn = FacturaMontoGravadoTrn;
    }

    public void setFacturaMontoIVA(Long FacturaMontoIVA) {
        this.FacturaMontoIVA = FacturaMontoIVA;
    }

    public void setFacturaMontoIVATrn(Long FacturaMontoIVATrn) {
        this.FacturaMontoIVATrn = FacturaMontoIVATrn;
    }

    public void setDecretoLeyId(Integer DecretoLeyId) {
        this.DecretoLeyId = DecretoLeyId;
    }

    public void setDecretoLeyNom(String DecretoLeyNom) {
        this.DecretoLeyNom = DecretoLeyNom;
    }

    public void setDecretoLeyAdqId(String DecretoLeyAdqId) {
        this.DecretoLeyAdqId = DecretoLeyAdqId;
    }

    public void setDecretoLeyVoucher(String DecretoLeyVoucher) {
        this.DecretoLeyVoucher = DecretoLeyVoucher;
    }

    public void setPlanId(Integer PlanId) {
        this.PlanId = PlanId;
    }
    public void setPlanVtaId(Integer PlanVtaId) {
           this.PlanVtaId = PlanVtaId;
    }
    public void setPlanNombre(String PlanNombre) {
        this.PlanNombre = PlanNombre;
    }

    public void setPlanNroPlan(Integer PlanNroPlan) {
        this.PlanNroPlan = PlanNroPlan;
    }

    public void setPlanNroTipoPlan(Integer PlanNroTipoPlan) {
        this.PlanNroTipoPlan = PlanNroTipoPlan;
    }

    public void setTipoCuentaId(Integer TipoCuentaId) {
        this.TipoCuentaId = TipoCuentaId;
    }

    public void setTipoCuentaNombre(String TipoCuentaNombre) {
        this.TipoCuentaNombre = TipoCuentaNombre;
    }

    public void setCuentaNro(String CuentaNro) {
        this.CuentaNro = CuentaNro;
    }

    public void setEmvAppId(String EmvAppId) {
        this.EmvAppId = EmvAppId;
    }

    public void setEmvAppName(String EmvAppName) {
        this.EmvAppName = EmvAppName;
    }

    public void setFirmarVoucher(Boolean FirmarVoucher) {
        this.FirmarVoucher = FirmarVoucher;
    }

    public void setTextoAdicional(String TextoAdicional) {
        this.TextoAdicional = TextoAdicional;
    }
    
    
}
