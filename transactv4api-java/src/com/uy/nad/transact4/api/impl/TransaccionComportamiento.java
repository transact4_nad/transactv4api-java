/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.ITransaccionComportamiento;

/**
 *
 * @author yunior
 */
public class TransaccionComportamiento implements ITransaccionComportamiento {

    private Boolean ModificarMoneda;
    private Boolean ModificarMonedaSeteado = false;
    private Boolean ModificarMontos;
    private Boolean ModificarMontosSeteado = false;
    private Boolean ModificarCuotas;
    private Boolean ModificarCuotasSeteado = false;
    private Boolean ModificarFactura;
    private Boolean ModificarFacturaSeteado = false;
    private Boolean ModificarTarjeta;
    private Boolean ModificarTarjetaSeteado = false;
    private Boolean ModificarPlan;
    private Boolean ModificarPlanSeteado = false;
    private Boolean ModificarDecretoLey;
    private Boolean ModificarDecretoLeySeteado = false;
    private Boolean ModificarTipoCuenta;
    private Boolean ModificarTipoCuentaSeteado = false;

    public void Limpiar() {
        ModificarCuotas = false;
        ModificarCuotasSeteado = false;
        ModificarDecretoLey = false;
        ModificarDecretoLeySeteado = false;
        ModificarFactura = false;
        ModificarFacturaSeteado = false;
        ModificarMoneda = false;
        ModificarMonedaSeteado = false;
        ModificarMontos = false;
        ModificarMontosSeteado = false;
        ModificarPlan = false;
        ModificarPlanSeteado = false;
        ModificarTarjeta = false;
        ModificarTarjetaSeteado = false;
        ModificarTipoCuenta = false;
        ModificarTipoCuentaSeteado = false;
    }

    @Override
    public void setModificarMoneda(Boolean ModificarMoneda) {
        this.ModificarMoneda = ModificarMoneda;
        this.ModificarMonedaSeteado = true;
    }

    @Override
    public void setModificarMontos(Boolean ModificarMontos) {
        this.ModificarMontos = ModificarMontos;
        this.ModificarMontosSeteado = true;
    }

    @Override
    public void setModificarCuotas(Boolean ModificarCuotas) {
        this.ModificarCuotas = ModificarCuotas;
        this.ModificarCuotasSeteado = true;
    }

    @Override
    public void setModificarFactura(Boolean ModificarFactura) {
        this.ModificarFactura = ModificarFactura;
        this.ModificarFacturaSeteado = true;
    }

    @Override
    public void setModificarTarjeta(Boolean ModificarTarjeta) {
        this.ModificarTarjeta = ModificarTarjeta;
        this.ModificarTarjetaSeteado = true;
    }

    @Override
    public void setModificarPlan(Boolean ModificarPlan) {
        this.ModificarPlan = ModificarPlan;
        this.ModificarPlanSeteado = true;
    }

    @Override
    public void setModificarDecretoLey(Boolean ModificarDecretoLey) {
        this.ModificarDecretoLey = ModificarDecretoLey;
        this.ModificarDecretoLeySeteado = true;
    }

    @Override
    public void setModificarTipoCuenta(Boolean ModificarTipoCuenta) {
        this.ModificarTipoCuenta = ModificarTipoCuenta;
        this.ModificarTipoCuentaSeteado = true;
    }

    public Boolean getModificarMoneda() {
        return ModificarMoneda;
    }

    public Boolean getModificarMonedaSeteado() {
        return ModificarMonedaSeteado;
    }

    public Boolean getModificarMontos() {
        return ModificarMontos;
    }

    public Boolean getModificarMontosSeteado() {
        return ModificarMontosSeteado;
    }

    public Boolean getModificarCuotas() {
        return ModificarCuotas;
    }

    public Boolean getModificarCuotasSeteado() {
        return ModificarCuotasSeteado;
    }

    public Boolean getModificarFactura() {
        return ModificarFactura;
    }

    public Boolean getModificarFacturaSeteado() {
        return ModificarFacturaSeteado;
    }

    public Boolean getModificarTarjeta() {
        return ModificarTarjeta;
    }

    public Boolean getModificarTarjetaSeteado() {
        return ModificarTarjetaSeteado;
    }

    public Boolean getModificarPlan() {
        return ModificarPlan;
    }

    public Boolean getModificarPlanSeteado() {
        return ModificarPlanSeteado;
    }

    public Boolean getModificarDecretoLey() {
        return ModificarDecretoLey;
    }

    public Boolean getModificarDecretoLeySeteado() {
        return ModificarDecretoLeySeteado;
    }

    public Boolean getModificarTipoCuenta() {
        return ModificarTipoCuenta;
    }

    public Boolean getModificarTipoCuentaSeteado() {
        return ModificarTipoCuentaSeteado;
    }
}
