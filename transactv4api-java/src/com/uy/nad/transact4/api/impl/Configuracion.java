/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IConfiguracion;

/**
 *
 * @author yunior
 */
public class Configuracion implements IConfiguracion {

    private Boolean ModoEmulacion;
    private Integer POSTipo;
    private Integer POSTipoCnx;
    private String POSDireccionIP;
    private Integer POSPuerto;
    private Integer POSSegsTimeout;
    private String URLConcentrador;
    //GUI
    private Integer GUITipo;
    private Integer GUIModo;
    private Boolean GUIMostrarTouchPad;
    private Integer GUIPosicionX;
    private Integer GUIPosicionY;
    //IMPRESION
    private Integer ImpresionTipo;
    private Integer ImpresionModo;
    private Integer ImpresionTipoImpresora;
    private String ImpresionNombreImpresora;
    private Integer ImpresionCopias;
    private String ImpresionDireccionIP;
    private Integer ImpresionPuerto;
    private Integer ImpresionSegsTimeout;
    private Boolean TrazaEncendida;
    private Integer SegTimeoutLecturaTarjFactura;
    
    private Boolean IngresaPropinaEnPos;

    public Configuracion() {
        ModoEmulacion = false;
        POSTipo = 0;
        POSTipoCnx = 1;
        POSDireccionIP = "";
        POSPuerto = 0;
        POSSegsTimeout = 60;
        URLConcentrador = "http://concentrador01.transact.com.uy:81";
        GUITipo = 9;
        GUIModo = 0;
        GUIMostrarTouchPad = false;
        GUIPosicionX = 0;
        GUIPosicionY = 0;
        //IMPRESION
        ImpresionTipo = 9;
        ImpresionModo = 0;
        ImpresionTipoImpresora = 0;
        ImpresionNombreImpresora = "";
        ImpresionCopias = 1;
        ImpresionDireccionIP = "";
        ImpresionPuerto = 0;
        ImpresionSegsTimeout = 60;
        TrazaEncendida = false;
        SegTimeoutLecturaTarjFactura = 20;
        IngresaPropinaEnPos = false;
    }

   @Override
    public void setModoEmulacion(Boolean ModoEmulacion) {
        this.ModoEmulacion = ModoEmulacion;
    }

    
    @Override
    public void setPOSTipo(Integer POSTipo) {
        this.POSTipo = POSTipo;
    }

   
    @Override
    public void setPOSTipoCnx(Integer POSTipoCnx) {
        this.POSTipoCnx = POSTipoCnx;
    }

  
    @Override
    public void setPOSDireccionIP(String POSDireccionIP) {
        this.POSDireccionIP = POSDireccionIP;
    }

   
    @Override
    public void setPOSPuerto(int POSPuerto) {
        this.POSPuerto = POSPuerto;
    }

   
    @Override
    public void setPOSSegsTimeout(Integer POSSegsTimeout) {
        this.POSSegsTimeout = POSSegsTimeout;
    }

  
    @Override
    public void setURLConcentrador(String URLConcentrador) {
        this.URLConcentrador = URLConcentrador;
    }

    public void setPOSPuerto(java.lang.Integer POSPuerto) {
        this.POSPuerto = POSPuerto;
    }

    @Override
    public void setGUITipo(java.lang.Integer GUITipo) {
        this.GUITipo = GUITipo;
    }

    @Override
    public void setGUIModo(java.lang.Integer GUIModo) {
        this.GUIModo = GUIModo;
    }

    @Override
    public void setGUIMostrarTouchPad(java.lang.Boolean GUIMostrarTouchPad) {
        this.GUIMostrarTouchPad = GUIMostrarTouchPad;
    }

    @Override
    public void setGUIPosicionX(java.lang.Integer GUIPosicionX) {
        this.GUIPosicionX = GUIPosicionX;
    }

    @Override
    public void setGUIPosicionY(java.lang.Integer GUIPosicionY) {
        this.GUIPosicionY = GUIPosicionY;
    }

    @Override
    public void setImpresionTipo(java.lang.Integer ImpresionTipo) {
        this.ImpresionTipo = ImpresionTipo;
    }

    @Override
    public void setImpresionModo(java.lang.Integer ImpresionModo) {
        this.ImpresionModo = ImpresionModo;
    }

    @Override
    public void setImpresionTipoImpresora(java.lang.Integer ImpresionTipoImpresora) {
        this.ImpresionTipoImpresora = ImpresionTipoImpresora;
    }

    @Override
    public void setImpresionNombreImpresora(String ImpresionNombreImpresora) {
        this.ImpresionNombreImpresora = ImpresionNombreImpresora;
    }

    @Override
    public void setImpresionCopias(java.lang.Integer ImpresionCopias) {
        this.ImpresionCopias = ImpresionCopias;
    }

    @Override
    public void setImpresionDireccionIP(String ImpresionDireccionIP) {
        this.ImpresionDireccionIP = ImpresionDireccionIP;
    }

    @Override
    public void setImpresionPuerto(java.lang.Integer ImpresionPuerto) {
        this.ImpresionPuerto = ImpresionPuerto;
    }

    @Override
    public void setImpresionSegsTimeout(java.lang.Integer ImpresionSegsTimeout) {
        this.ImpresionSegsTimeout = ImpresionSegsTimeout;
    }
    @Override
    public void setSegTimeoutLecturaTarjFactura(Integer SegTimeoutLecturaTarjFactura){
        this.SegTimeoutLecturaTarjFactura = SegTimeoutLecturaTarjFactura;
    }
   @Override
    public void setIngresaPropinaEnPos(Boolean IngresaPropinaEnPos){
        this.IngresaPropinaEnPos = IngresaPropinaEnPos;
    } 

    
    //------------ GET

    public Boolean getModoEmulacion() {
        return ModoEmulacion;
    }

    public Integer getPOSTipo() {
        return POSTipo;
    }

    public Integer getPOSTipoCnx() {
        return POSTipoCnx;
    }

    public String getPOSDireccionIP() {
        return POSDireccionIP;
    }

    public Integer getPOSPuerto() {
        return POSPuerto;
    }

    public Integer getPOSSegsTimeout() {
        return POSSegsTimeout;
    }

    public String getURLConcentrador() {
        return URLConcentrador;
    }

    public Integer getGUITipo() {
        return GUITipo;
    }

    public Integer getGUIModo() {
        return GUIModo;
    }

    public Boolean getGUIMostrarTouchPad() {
        return GUIMostrarTouchPad;
    }

    public Integer getGUIPosicionX() {
        return GUIPosicionX;
    }

    public Integer getGUIPosicionY() {
        return GUIPosicionY;
    }

    public Integer getImpresionTipo() {
        return ImpresionTipo;
    }

    public Integer getImpresionModo() {
        return ImpresionModo;
    }

    public Integer getImpresionTipoImpresora() {
        return ImpresionTipoImpresora;
    }

    public String getImpresionNombreImpresora() {
        return ImpresionNombreImpresora;
    }

    public Integer getImpresionCopias() {
        return ImpresionCopias;
    }

    public String getImpresionDireccionIP() {
        return ImpresionDireccionIP;
    }

    public Integer getImpresionPuerto() {
        return ImpresionPuerto;
    }

    public Integer getImpresionSegsTimeout() {
        return ImpresionSegsTimeout;
    }
     public Integer  getSegTimeoutLecturaTarjFactura(){
        return SegTimeoutLecturaTarjFactura;
    }
      public Boolean getIngresaPropinaEnPos(){
        return IngresaPropinaEnPos;
    }
}
