/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IAPIListener;
import com.uy.nad.transact4.api.ILectorTarjeta;
import com.uy.nad.transact4.api.ILecturaTarjetaRespuesta;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Tarjetas;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class LectorTarjeta implements ILectorTarjeta {

    private final TransActAPI_Tarjetas_v400Impl _fachada;
    private final IAPIListener _listener;
    private final Configuracion _configuracion;

    private String EmpCod;
    private Boolean EmpCodSeteado = false;
    private String TermCod;
    private Boolean TermCodSeteado = false;
    private Integer MultiEmp;
    private Boolean MultiEmpSeteado = false;

    private LecturaTarjetaRespuesta respuesta;

    public LectorTarjeta(TransActAPI_Tarjetas_v400Impl _fachada, IAPIListener _listener, Configuracion _configuracion) {
        this._fachada = _fachada;
        this._listener = _listener;
        this._configuracion = _configuracion;
        EmpCod = "";
        EmpCodSeteado = false;
        TermCod = "";
        TermCodSeteado = false;
        MultiEmp = 0;
        MultiEmpSeteado = false;
        respuesta = new LecturaTarjetaRespuesta();

    }

    public String getEmpCod() {
        return EmpCod;
    }

    @Override
    public void setEmpCod(String EmpCod) {
        this.EmpCod = EmpCod;
        EmpCodSeteado = true;
    }

    public String getTermCod() {
        return TermCod;
    }

    @Override
    public void setTermCod(String TermCod) {
        this.TermCod = TermCod;
        TermCodSeteado = true;
    }

    public Integer getMultiEmp() {
        return MultiEmp;
    }

    @Override
    public void setMultiEmp(Integer MultiEmp) {
        this.MultiEmp = MultiEmp;
        MultiEmpSeteado = true;
    }

    @Override
    public ILecturaTarjetaRespuesta getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(LecturaTarjetaRespuesta respuesta) {
        this.respuesta = respuesta;
    }

    @Override
    public Boolean LeerTarjeta() {
        Tarjetas objCore = null;
        try {

            _fachada.EsperarIntervaloMinimoEntreLlamadas();
            objCore = new Tarjetas(_listener, _configuracion);
            LimpiarRespuesta();
            SetearLecturaTarjeta(objCore);
            objCore.LeerTarjeta();
            CargarRespLecturaTarjeta(objCore);
            return true;
        } catch (Exception ex) {
            CargarRespuestaByException(ex);
        } finally {
            //objCore.RemoverListener();
            _fachada.ActualizarTmstUltimaLlamadas();
        }
        return false;
    }

    @Override
    public void CancelarOperacion() {
        //TODO: Implementar
    }

    private void LimpiarRespuesta() {
        respuesta.Limpiar();
    }
    private void CargarRespuestaByException(Exception ex) {
        respuesta.Limpiar();
            respuesta.setCodRespuesta("TS");
            respuesta.setMsgRespuesta(ex.getMessage());
    }

    private void SetearLecturaTarjeta(Tarjetas objCore) {
        _fachada.SeteaConfig(objCore);

        if (EmpCodSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_EMPCOD, EmpCod, false);
        }
        if (TermCodSeteado) {
            objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_TERMCOD, TermCod, false);
        }
        if (MultiEmpSeteado) {
            objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP, MultiEmp, false);
        }
    }

    private void CargarRespLecturaTarjeta(Tarjetas objCore) throws Exception{
        respuesta.Limpiar();
        respuesta.setCodRespuesta(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_LECTURATARJETA_RESPUESTA_CAMPO_CODRESP));
        respuesta.setMsgRespuesta(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_LECTURATARJETA_RESPUESTA_CAMPO_MSGRESP)); 
        respuesta.setTrack1(HexToString(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_LECTURATARJETA_RESPUESTA_CAMPO_TRACK1))); 
        respuesta.setTrack2(HexToString(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_LECTURATARJETA_RESPUESTA_CAMPO_TRACK2)));
        respuesta.setTrack3(HexToString(objCore.GetCampo_String(DiccionarioCampos.TRANSACT_LECTURATARJETA_RESPUESTA_CAMPO_TRACK3)));
    }

    private String HexToString(String arg) {        
    String str = "";
    for(int i=0;i<arg.length();i+=2)
    {
        String s = arg.substring(i, (i + 2));
        int decimal = Integer.parseInt(s, 16);
        str = str + (char) decimal;
    }       
    return str;
}

}
