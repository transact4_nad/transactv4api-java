/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.impl;

import com.uy.nad.transact4.api.IConfiguracion;
import com.uy.nad.transact4.api.ITransActAPI_Tarjetas_v400;
import com.uy.nad.transact4.api.ITransaccion;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Tarjetas;
import java.util.Date;
import com.uy.nad.transact4.api.IAPIListener;
import com.uy.nad.transact4.api.ICierreLote;
import com.uy.nad.transact4.api.IImpresion;
import com.uy.nad.transact4.api.ILectorTarjeta;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class TransActAPI_Tarjetas_v400Impl implements ITransActAPI_Tarjetas_v400 {

    IAPIListener listener;
    private Configuracion objConfiguracion = null;
    private Transaccion objTransaccion = null;
    private CierreLote objCierre = null;
    private Impresion objImpresion = null;
    private LectorTarjeta objLector = null;

    private final String Version = "40012";


    
     private final Integer MILISEGUNDOS_INTERVALO_LLAMADAS = 2000;

    private Date TmstUltimaLlamada = new Date();

    public TransActAPI_Tarjetas_v400Impl(IAPIListener listener) {
        this.listener = listener;
        objConfiguracion = new Configuracion();
        objTransaccion = new Transaccion(this, listener, objConfiguracion);
        objCierre = new CierreLote(this, listener, objConfiguracion);
        objImpresion = new Impresion(this, listener, objConfiguracion);
        objLector = new LectorTarjeta(this, listener, objConfiguracion);


    }
     public TransActAPI_Tarjetas_v400Impl() {
        this.listener = new APIListener();
        objConfiguracion = new Configuracion();
        objTransaccion = new Transaccion(this, listener, objConfiguracion);
        objCierre = new CierreLote(this, listener, objConfiguracion);
        objImpresion = new Impresion(this, listener, objConfiguracion);
        objLector = new LectorTarjeta(this, listener, objConfiguracion);


    }


    //-----------------PRIVADOS DE LA FACHADA--------------------
    void SeteaConfig(Tarjetas objCore) {
       
        objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER, Version, false);
        objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CFG_MODOEMULACION, objConfiguracion.getModoEmulacion(), false);
        //Conexion
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSTIPO, objConfiguracion.getPOSTipo(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSTIPOCNX, objConfiguracion.getPOSTipoCnx(), false);
        objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSDIRECCION, objConfiguracion.getPOSDireccionIP(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSPUERTO, objConfiguracion.getPOSPuerto(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSSEGSTIMEOUT, objConfiguracion.getPOSSegsTimeout(), false);
        objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CFG_URLCONCENTRADOR, objConfiguracion.getURLConcentrador(), false);

        //GUI
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUITIPO, objConfiguracion.getGUITipo(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIMODO, objConfiguracion.getGUIModo(), false);
        objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIMOSTRARTOUCHPAD, objConfiguracion.getGUIMostrarTouchPad(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIPOSICIONX, objConfiguracion.getGUIPosicionX(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIPOSICIONY, objConfiguracion.getGUIPosicionY(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIPOSICIONY, objConfiguracion.getGUIPosicionY(), false);
    
        //IMPRESION
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONTIPO, objConfiguracion.getImpresionTipo(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONMODO, objConfiguracion.getImpresionModo(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONTIPOIMPRESORA, objConfiguracion.getImpresionTipoImpresora(), false);
        objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONNOMBREIMPRESORA, objConfiguracion.getImpresionNombreImpresora(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONCOPIAS, objConfiguracion.getImpresionCopias(),false);
        objCore.SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONDIRECCION, objConfiguracion.getImpresionDireccionIP(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONPUERTO, objConfiguracion.getImpresionPuerto(), false);
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONSEGSTIMEOUT, objConfiguracion.getImpresionSegsTimeout(), false);
        //Otros
        objCore.SetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_TIMEOUT_LECTURATARJ_FACTURA, objConfiguracion.getSegTimeoutLecturaTarjFactura(), false);
        objCore.SetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CFG_INGRESA_PROPINA_EN_POS, objConfiguracion.getIngresaPropinaEnPos(), false);

    }

    //---------------------------------------
    @Override
    public IConfiguracion Configuracion() {
        return objConfiguracion;
    }

    @Override
    public ITransaccion Transaccion() {
        return objTransaccion;
    }

    @Override
    public String getVersion() {
        return Version;
    }
    
    @Override
    public ICierreLote CierreLote() {
       return objCierre;
    }
     @Override
    public IImpresion Impresion() {
        return objImpresion;
    }
    @Override
    public ILectorTarjeta LectorTarjeta(){
    return objLector;
    }
    public void EsperarIntervaloMinimoEntreLlamadas(){
       //TODO: Implementar
    }
    public void ActualizarTmstUltimaLlamadas(){
        //TODO: Implementar
    }
    

}
