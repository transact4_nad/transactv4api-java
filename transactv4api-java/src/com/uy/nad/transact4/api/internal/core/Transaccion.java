/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author yunior
 */
public class Transaccion {

    private List<TransaccionCampo> listaCampos = new ArrayList<TransaccionCampo>();

    public List<TransaccionCampo> ObtenerListaCampos() {
        return listaCampos;
    }

    public boolean EsCentralizada() {
        for (TransaccionCampo campo : listaCampos) {
            if (campo.getNombreCampo().toUpperCase().equals("CIERRECENTRALIZADO") && Boolean.parseBoolean(campo.getValorCampo().toString())) {
                return true;
            }
        }
        return false;
    }

    public Object ObtenerValorCampoSiExiste(String nombreCampo) {
        for (TransaccionCampo campo : listaCampos) {
            if (campo.getNombreCampo().equals(nombreCampo)) {
                return campo.getValorCampo();
            }
        }
        return null;
    }
     public Object ObtenerValorCampoSiExiste(String nombreCampo,Object defecto) {
        for (TransaccionCampo campo : listaCampos) {
            if (campo.getNombreCampo().equals(nombreCampo)) {
                return campo.getValorCampo();
            }
        }
        return defecto;
    }

    public Object ObtenerValorCampo(String nombreCampo) throws Exception {
        Object obj = ObtenerValorCampoSiExiste(nombreCampo);
        if (obj == null) {
            throw new Exception("CAMPO " + nombreCampo + " NO EXISTE EN LA TRANSACCION");
        } else {
            return obj;
        }
    }

    public void SetearValorCampo(String Campo, Object Valor, Boolean PermiteModificar, String TipoDatos) {
        for (TransaccionCampo iter : listaCampos) {
            if (iter.getNombreCampo().equals(Campo)) {
                iter.setValorCampo(Valor);
                return;
            }
        }
        listaCampos.add(new TransaccionCampo(Campo, Valor, PermiteModificar, TipoDatos));
    }

    public Boolean GetCampo_Boolean(String Campo) throws Exception {
        Object obj = ObtenerValorCampoSiExiste(Campo);
        if (obj == null) {
            return false;
        }
        try {
            if (String.valueOf(obj).isEmpty()) {
                return new Boolean(false);
            }
            return Boolean.parseBoolean(String.valueOf(obj));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Campo " + Campo + " no es del tipo Boolean!");
        }
    }

    public Date GetCampo_DateTime(String Campo) throws Exception {
        Object obj = ObtenerValorCampoSiExiste(Campo);
        if (obj == null) {
            return null;
        }
        try {
            try{
              return (Date)obj;
            }
            catch(Exception ex){
                
            }
            String datstr= String.valueOf(obj);
            if (String.valueOf(obj).isEmpty()) {
                return null;
            }
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(datstr);
        } catch (ParseException ex) {
            ex.printStackTrace();
            throw new Exception("Campo " + Campo + " no es del tipo Date!");
        }
    }

    public Integer GetCampo_Integer(String Campo) throws Exception {
        Object obj = ObtenerValorCampoSiExiste(Campo);
        if (obj == null) {
            return 0;
        }
        try {
            if (String.valueOf(obj).isEmpty()) {
                return 0;
            }
            return Integer.parseInt(String.valueOf(obj));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Campo " + Campo + " no es del tipo Integer!");
        }
    }

    public Long GetCampo_Long(String Campo) throws Exception {
        Object obj = ObtenerValorCampoSiExiste(Campo);
        if (obj == null) {
            return null;
        }
        try {
            if (String.valueOf(obj).isEmpty()) {
                return new Long(0);
            }
            return Long.parseLong(String.valueOf(obj));
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Campo " + Campo + " no es del tipo Long!");
        }
    }

    public String GetCampo_String(String Campo) throws Exception {
        Object obj = ObtenerValorCampoSiExiste(Campo);
        if (obj == null) {
            return "";
        }
        try {
            return ((String) obj);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Campo " + Campo + " no es del tipo String!");
        }
    }

    public List<Transaccion> GetCampo_Coleccion(String Campo) throws Exception {
        
        Object obj  = ObtenerValorCampoSiExiste(Campo);
        if(obj == null){
           return new ArrayList<Transaccion>();
        }else{
            List<Transaccion> coll = (List<Transaccion>)obj;
           return coll;
        }
    }

    public void SetCampo_Boolean(String Campo, Boolean Valor, Boolean PermiteModificar) {
        this.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_BOOLEAN);
    }

    public void SetCampo_DateTime(String Campo, Date Valor, Boolean PermiteModificar) {
        this.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_FECHA);
    }

    public void SetCampo_Integer(String Campo, Integer Valor, Boolean PermiteModificar) {
        this.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_NUMERICO);
    }

    public void SetCampo_Long(String Campo, Long Valor, Boolean PermiteModificar) {
        this.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_NUMERICOLARGO);
    }

    public void SetCampo_String(String Campo, String Valor, Boolean PermiteModificar) {
        this.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
    }

    void EliminarCampo(String NombreCampo) {
        for (TransaccionCampo var : listaCampos) {
            if(var.getNombreCampo().equals(NombreCampo)){
              listaCampos.remove(var);
              return;
            }
        }
    }
    
    public void AgregarColeccion(String Campo, Transaccion objColeccionItem){
        String TIPO_DATOS_COLECCION = "COL";
          List<Transaccion> LstColeccion;
          Object objCol = ObtenerValorCampoSiExiste(Campo);
           if(objCol == null){
              LstColeccion = new ArrayList<Transaccion>();
              LstColeccion.add(objColeccionItem);
              this.SetearValorCampo(Campo, LstColeccion, false, TIPO_DATOS_COLECCION);
           }
           else{
           LstColeccion = (List<Transaccion>)objCol;
           LstColeccion.add(objColeccionItem);
           
           }
    }
}
