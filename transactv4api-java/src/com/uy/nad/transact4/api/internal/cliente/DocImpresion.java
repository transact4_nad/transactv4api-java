/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.cliente;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class DocImpresion {
    private String nroSerie;
    private String appVer;
    private Integer tipoImpresora;

    private String nomImpresora;
    private Integer copias;
    private String disVoucher;
    private boolean resultado;
    private String msgResultado;
    public DocImpresion() {
    }
    
    public DocImpresion(String nroSerie, String appVer, Integer tipoImpresora,String nomImpresora, Integer copias, String disVoucher, boolean resultado, String msgResultado) {
        this.nroSerie = nroSerie;
        this.appVer = appVer;
        this.tipoImpresora = tipoImpresora;
        this.nomImpresora = nomImpresora;
        this.copias = copias;
        this.disVoucher = disVoucher;
        this.resultado = resultado;
        this.msgResultado = msgResultado;
    }

    public String getNroSerie() {
        return nroSerie;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }

    public String getAppVer() {
        return appVer;
    }

    public void setAppVer(String appVer) {
        this.appVer = appVer;
    }

    public Integer getTipoImpresora() {
        return tipoImpresora;
    }

    public void setTipoImpresora(Integer tipoImpresora) {
        this.tipoImpresora = tipoImpresora;
    }

    public String getNomImpresora() {
        return nomImpresora;
    }

    public void setNomImpresora(String nomImpresora) {
        this.nomImpresora = nomImpresora;
    }

    public Integer getCopias() {
        return copias;
    }

    public void setCopias(Integer copias) {
        this.copias = copias;
    }

    public String getDisVoucher() {
        return disVoucher;
    }

    public void setDisVoucher(String disVoucher) {
        this.disVoucher = disVoucher;
    }

    public boolean isResultado() {
        return resultado;
    }

    public void setResultado(boolean resultado) {
        this.resultado = resultado;
    }

    public String getMsgResultado() {
        return msgResultado;
    }

    public void setMsgResultado(String msgResultado) {
        this.msgResultado = msgResultado;
    }
    

    

}
