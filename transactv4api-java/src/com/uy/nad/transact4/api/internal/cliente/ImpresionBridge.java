/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.cliente;

import com.uy.nad.transact4.api.internal.cliente.BridgeClientePos;
import com.uy.nad.transact4.api.internal.cliente.MensajeWrapperEnvio;
import com.uy.nad.transact4.api.internal.cliente.MensajeWrapperRecibe;
import com.uy.nad.transact4.api.internal.cliente.TCPClientePos;
import com.uy.nad.transact4.api.internal.core.exceptions.NADGeneralException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class ImpresionBridge {

    private TCPClientePos objClienteTcp;
    private Integer timeoutMs;

    public ImpresionBridge(String ip, Integer puerto, Integer timeoutMS) {
        this.timeoutMs = timeoutMS;
        objClienteTcp = new TCPClientePos(ip, puerto, timeoutMS);
    }

    public void Imprimir(DocImpresion datosImp) {

        MensajeWrapperEnvio msgEnvio = new MensajeWrapperEnvio(MensajeWrapperEnvio.WRAPPER_TIPO_IMPRIMIR, datosImp.getTipoImpresora(),
                datosImp.getNomImpresora(), datosImp.getCopias(), datosImp.getDisVoucher());
        String msgEnvioStr;
        try {
            msgEnvioStr = msgEnvio.ObtenerMensajePOS();
            objClienteTcp.EnviarMensajeSinRespuesta(msgEnvioStr);
            datosImp.setResultado(true);
            datosImp.setMsgResultado("OK");

        } catch (Exception ex) {
            Logger.getLogger(BridgeClientePos.class.getName()).log(Level.SEVERE, null, ex);
            datosImp.setResultado(false);
            datosImp.setMsgResultado(ex.getMessage());
        }

    }

}
