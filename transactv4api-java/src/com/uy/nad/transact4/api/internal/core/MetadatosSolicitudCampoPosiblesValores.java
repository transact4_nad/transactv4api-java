/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

import com.uy.nad.transact4.api.IMetadatosSolicitudCampoPosiblesValores;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class MetadatosSolicitudCampoPosiblesValores implements IMetadatosSolicitudCampoPosiblesValores {

    private String Valor;
    private String Descripcion;

    public MetadatosSolicitudCampoPosiblesValores() {
    }

    @Override
    public String getValor() {
        return Valor;
    }

    public void setValor(String Valor) {
        this.Valor = Valor;
    }

    @Override
    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.Descripcion = Descripcion;
    }

}
