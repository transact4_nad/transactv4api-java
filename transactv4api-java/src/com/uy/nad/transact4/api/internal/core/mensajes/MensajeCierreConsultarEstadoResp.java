/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.StringCompresor;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.Arrays;
import java.util.Map;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author yunior
 */
public class MensajeCierreConsultarEstadoResp extends absMensajeRecibe {

    private final TipoMensajeRecibidoDesdePos tipoMensaje = TipoMensajeRecibidoDesdePos.RespuestaConsultaEstadoCierre;
    private String Msg;

    public MensajeCierreConsultarEstadoResp(String Msg) {
        this.Msg = Msg;
    }

    @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }

    public void CargarRespuestaEnTransaccion(Transaccion objTrn) throws Exception {
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(Msg);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CODRESP, ObtenerValorCampo(ListaCampos,
                DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CODRESP), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MSGRESP, ObtenerValorCampo(ListaCampos,
                DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MSGRESP), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_FINALIZADO, Boolean.parseBoolean(ObtenerValorCampo(ListaCampos,
                DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_FINALIZADO)), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_BOOLEAN);
        
        String CCORSPXML = ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CCORSPXML);
        System.out.println("CCORSPXML ANTES =" + CCORSPXML);
        String CCORSPXMLDescompreso = DescomprimirDatosCierre(CCORSPXML);
        System.out.println("CCORSPXML DESPUES=" + CCORSPXMLDescompreso);
        super.CargarTransaccionByXML(objTrn, CCORSPXMLDescompreso, "CCORSPXML");
    }

    private String DescomprimirDatosCierre(String CCORSPXML) throws Exception{
        String TagInicio = "<DATOSCIERRE>";
        String TagFinal = "</DATOSCIERRE>";
        Integer IxInicio = CCORSPXML.indexOf(TagInicio) + TagInicio.length();
        Integer IxFin = CCORSPXML.lastIndexOf(TagFinal);
        if(IxInicio <0 || IxFin < 0 || IxInicio + TagInicio.length() >= IxFin ) return CCORSPXML;
        String mensajeReem = CCORSPXML.substring(IxInicio, IxFin);
        byte[] valueDecoded = Base64.decodeBase64(mensajeReem);
        byte[] valueDecodedLimpio = Arrays.copyOfRange(valueDecoded, 4, valueDecoded.length);
        String DatosCierre = new StringCompresor().Descomprimir(valueDecodedLimpio);
        String respuestaFinal = CCORSPXML.replace(mensajeReem, DatosCierre);
        return respuestaFinal;
    }
}
