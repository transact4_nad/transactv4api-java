package com.uy.nad.transact4.api.internal.core.mensajes;

public abstract class absMensajeEnvio {
 

    public abstract String ObtenerMensajePOS() throws Exception;

    protected String AgregarCabeceraYCRCMensajePOS(String Msg){
        String MsgPOS = "";
        String LRC = String.format("%1$" + DiccionarioComandos.LARGO_LRC + "s", ""+Msg.length()).replace(' ', '0');
        MsgPOS = new StringBuilder().append(DiccionarioComandos.INICIO_ELEMENTO).append(DiccionarioComandos.PREFIJO_MENSAJE ).append(DiccionarioComandos.FIN_ELEMENTO).
                append(Msg).
                append(DiccionarioComandos.INICIO_ELEMENTO).append(DiccionarioComandos.SUFIJO_MENSAJE).append(DiccionarioComandos.FIN_ELEMENTO).
                append(DiccionarioComandos.INICIO_ELEMENTO).append(DiccionarioComandos.CAMPO_LRC).append(DiccionarioComandos.FIN_ELEMENTO).
                append(LRC).
                toString();
          return MsgPOS;
    }

    protected String AgregarCampo(String strMensaje, String NombreCampo , String ValorCampo ){
        strMensaje = new StringBuilder(strMensaje).append(DiccionarioComandos.INICIO_ELEMENTO).append( DiccionarioComandos.NOMBRE_CAMPO ).append( DiccionarioComandos.FIN_ELEMENTO ).append(NombreCampo ).
                append(DiccionarioComandos.INICIO_ELEMENTO).append(DiccionarioComandos.VALOR_CAMPO).append(DiccionarioComandos.FIN_ELEMENTO).append(ValorCampo).
                append(DiccionarioComandos.INICIO_ELEMENTO).  append(DiccionarioComandos.SEPARADOR_CAMPO).  append(DiccionarioComandos.FIN_ELEMENTO).toString();
       return strMensaje;
    }
     protected String AgregarCampo(String strMensaje, String NombreCampo , Object ValorCampo ){
        strMensaje = new StringBuilder(strMensaje).append(DiccionarioComandos.INICIO_ELEMENTO).append( DiccionarioComandos.NOMBRE_CAMPO ).append( DiccionarioComandos.FIN_ELEMENTO ).append(NombreCampo ).
                append(DiccionarioComandos.INICIO_ELEMENTO).append(DiccionarioComandos.VALOR_CAMPO).append(DiccionarioComandos.FIN_ELEMENTO).append(ValorCampo).
                append(DiccionarioComandos.INICIO_ELEMENTO).  append(DiccionarioComandos.SEPARADOR_CAMPO).  append(DiccionarioComandos.FIN_ELEMENTO).toString();
       return strMensaje;
    }


}
