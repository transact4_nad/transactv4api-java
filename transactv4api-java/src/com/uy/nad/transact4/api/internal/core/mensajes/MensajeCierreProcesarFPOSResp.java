/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.Map;

/**
 *
 * @author yunior
 */
public class MensajeCierreProcesarFPOSResp extends absMensajeRecibe {

    private final TipoMensajeRecibidoDesdePos tipoMensaje = TipoMensajeRecibidoDesdePos.RespuestaProcesarCierre;
    private String Msg;

    public MensajeCierreProcesarFPOSResp(String strRespuesta) throws Exception {
        if (absMensajeRecibe.GetTipoMensajeFromStr(strRespuesta) != tipoMensaje) {
            throw new Exception("Error al crear mensaje del tipo " + tipoMensaje.toString() + " con respuesta: " + strRespuesta);
        }
        Msg = strRespuesta;
    }

    @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }

    public void CargarRespuestaEnTransaccion(Transaccion objTrn) throws Exception{
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(this.Msg);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CODRESP, ObtenerValorCampo(ListaCampos, 
                DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CODRESP), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MSGRESP, ObtenerValorCampo(ListaCampos, 
                DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MSGRESP), false,DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TOKENCIERRE, ObtenerValorCampo(ListaCampos, 
                DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TOKENCIERRE), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
    }

}
