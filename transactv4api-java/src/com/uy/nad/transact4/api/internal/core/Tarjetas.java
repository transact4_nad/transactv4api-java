/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

import com.uy.nad.transact4.api.impl.Configuracion;
import java.util.Date;
import java.util.List;
import com.uy.nad.transact4.api.IAPIListener;

/**
 *
 * @author yunior
 */
public class Tarjetas {

    private final IAPIListener listener;
    private final Transaccion objTrn;
    private final Configuracion objConfiguracion;

    public Tarjetas(IAPIListener listener, Configuracion objConfiguracion) {
        this.listener = listener;
        this.objConfiguracion = objConfiguracion;
        objTrn = new Transaccion();
    }

    public void CargarDatosTarjeta() throws Exception{
         TransActAPICore objCore = new TransActAPICore(listener);
        objCore.TarjetasCargarDatosTarjeta(objTrn);
        String codResp = GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP);
        if (!codResp.equals("0")) {
            throw new Exception(GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        }
    }

    public void ProcesarTransaccion() throws Exception {
        TransActAPICore objCore = new TransActAPICore(listener);
        objCore.TarjetasProcesarTransaccion(objTrn);
        String codResp = GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP);
        if (!codResp.equals("0")) {
            throw new Exception(GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        }
    }

    public void ConfirmarTransaccion(Long TrnId) throws Exception {
        SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_TRNID, TrnId, false);
        TransActAPICore objCore = new TransActAPICore(listener);
        objCore.TarjetasConfirmarTransaccion(objTrn);
        String codResp = GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP);
        if (!codResp.equals("0")) {
            throw new Exception(GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        }
    }

    public void ReversarTransaccion(Long TrnId) throws Exception {
        SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_TRNID, TrnId, false);
        TransActAPICore objCore = new TransActAPICore(listener);
        objCore.TarjetasReversarTransaccion(objTrn);
        String codResp = GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP);
        if (!codResp.equals("0")) {
            throw new Exception(GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        }
    }

    public void ConsultarConfirmacionTransaccion(Long TrnId) throws Exception{
       SetCampo_Long(DiccionarioCampos.TRANSACT_CAMPO_TRNID, TrnId, false);
        TransActAPICore objCore = new TransActAPICore(listener);
        objCore.TarjetasConsultarConfirmacionTransaccion(objTrn);
        String codResp = GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP);
        if (!codResp.equals("0")) {
            throw new Exception(GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        }       
    }

    public void ProcesarCierre() throws Exception {
        TransActAPICore objCore = new TransActAPICore(listener);
        objCore.TarjetasProcesarCierre(objTrn);
        String codResp = GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP);
        if (!codResp.equals("0")) {
            throw new Exception(GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        }
    }

    public void ConsultarEstadoCierre(String TokenCierre) throws Exception {
        TransActAPICore objCore = new TransActAPICore(listener);
        SetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TOKENCIERRE, TokenCierre, false);
        objCore.TarjetasConsultarEstadoCierre(objTrn);
        String codResp = GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP);
        if (!codResp.equals("0")) {
            throw new Exception(GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        }
    }

    public void Reimprimir()throws Exception {
        TransActAPICore objCore = new TransActAPICore(listener);
        objCore.Reimprimir(objTrn);
        String codResp = GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP);
        if (!codResp.equals("0")) {
            throw new Exception(GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        }
        
    }

    public void LeerTarjeta() throws Exception{
         TransActAPICore objCore = new TransActAPICore(listener);
        objCore.LeerTarjeta(objTrn);
        String codResp = GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP);
        if (!codResp.equals("0")) {
            throw new Exception(GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP));
        }
    }
   
    
    public void DispositivoStatus() throws Exception {
        TransActAPICore objCore = new TransActAPICore(listener);
        objCore.DispositivoStatus(objTrn);
    }
    
    public void DispositivoReiniciar() throws Exception {
        TransActAPICore objCore = new TransActAPICore(listener);
        objCore.DispositivoReiniciar(objTrn);
    }
    public void Cancelar() throws Exception{
        TransActAPICore objCore = new TransActAPICore(listener);
        objCore.Cancelar(objTrn);
    }
            

    //-----
    public void SetCampo_Boolean(String Campo, Boolean Valor, Boolean PermiteModificar) {
        objTrn.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_BOOLEAN);
    }

    public void SetCampo_DateTime(String Campo, Date Valor, Boolean PermiteModificar) {
        objTrn.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_FECHA);
    }

    public void SetCampo_Integer(String Campo, Integer Valor, Boolean PermiteModificar) {
        objTrn.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_NUMERICO);
    }

    public void SetCampo_Long(String Campo, Long Valor, Boolean PermiteModificar) {
        objTrn.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_NUMERICOLARGO);
    }

    public void SetCampo_String(String Campo, String Valor, Boolean PermiteModificar) {
        objTrn.SetearValorCampo(Campo, Valor, PermiteModificar, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
    }

    public Boolean GetCampo_Boolean(String Campo) throws Exception {
        return objTrn.GetCampo_Boolean(Campo);
    }

    public Date GetCampo_DateTime(String Campo) throws Exception {
        return objTrn.GetCampo_DateTime(Campo);
    }

    public Integer GetCampo_Integer(String Campo) throws Exception {
        return objTrn.GetCampo_Integer(Campo);
    }

    public Long GetCampo_Long(String Campo) throws Exception {
        return objTrn.GetCampo_Long(Campo);
    }

    public String GetCampo_String(String Campo) throws Exception {
        return objTrn.GetCampo_String(Campo);
    }

    public List<Transaccion> GetCampo_Coleccion(String Campo) throws Exception {
        return objTrn.GetCampo_Coleccion(Campo);
    }    

}
