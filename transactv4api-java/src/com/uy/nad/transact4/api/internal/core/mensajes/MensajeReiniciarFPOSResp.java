/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.Map;
/**
 *
 * @author DanielCera
 */
public class MensajeReiniciarFPOSResp extends absMensajeRecibe{
     private final TipoMensajeRecibidoDesdePos tipoMensaje = TipoMensajeRecibidoDesdePos.RespuestaActualizarDispositivo;
     
      public MensajeReiniciarFPOSResp(String strRespuesta) throws Exception {
       CargarDatos(strRespuesta);
    }
      
      @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }

    private Integer CodResp;
    private String MsgResp;

    public Integer getCodResp() {
        return CodResp;
    }

    public String getMsgResp() {
        return MsgResp;
    }

    private void CargarDatos(String strRespuesta) throws Exception{
        if (absMensajeRecibe.GetTipoMensajeFromStr(strRespuesta) != tipoMensaje) {
            throw new Exception("Error al crear mensaje del tipo " + tipoMensaje.toString() + " con respuesta: " + strRespuesta);
        }
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(strRespuesta);
        CodResp = Integer.parseInt(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP));
        MsgResp = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP);
    }
    
     public void CargarRespuestaEnTransaccion(Transaccion objTrn) throws Exception{
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP, CodResp.toString(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP, MsgResp, false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
     }
}
