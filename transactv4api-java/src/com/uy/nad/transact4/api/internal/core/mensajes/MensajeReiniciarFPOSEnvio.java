/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;

/**
 *
 * @author DanielCera
 */
public class MensajeReiniciarFPOSEnvio extends absMensajeEnvio {

    private Transaccion objTrn;

    public MensajeReiniciarFPOSEnvio(Transaccion objTrn) {
        this.objTrn = objTrn;
    }

    @Override
    public String ObtenerMensajePOS() throws Exception {
        String strMensaje = DiccionarioComandos.COMANDO_ACTUALIZAR_DISPOSITIVO;
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER).toString());
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_EMPCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_EMPCOD).toString());
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_TERMCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_TERMCOD).toString());
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }
}
