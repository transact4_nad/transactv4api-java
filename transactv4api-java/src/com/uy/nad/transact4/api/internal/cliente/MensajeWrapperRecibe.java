/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.cliente;

import com.uy.nad.transact4.api.internal.core.exceptions.NADGeneralException;
import com.uy.nad.transact4.api.internal.core.mensajes.DiccionarioComandos;
import com.uy.nad.transact4.api.internal.core.mensajes.absMensajeRecibe;
import java.util.Map;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class MensajeWrapperRecibe extends absMensajeRecibe {

    private final String TRANSACT_COMANDO_WRAPPER_RESP = "WRPRSP";
    private final String TRANSACT_CAMPO_TIPO = "TIPO";
    private final String TRANSACT_CAMPO_MSG = "MSG";
    public static final String WRAPPER_TIPO_ENVIO_AL_POS = "ENVPOS";
    public static final String WRAPPER_TIPO_ERROR = "ERROR";

    private String Tipo;
    private String Msg;

    MensajeWrapperRecibe(String mensaje) throws Exception {
        CargarDatos(mensaje);
    }

    public String getMsg() {
        return Msg;
    }

    @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void CargarDatos(String StrMensaje) throws Exception {
        int LargoPrefijo = Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.PREFIJO_MENSAJE + DiccionarioComandos.FIN_ELEMENTO);
        if (StrMensaje.length() < LargoPrefijo + DiccionarioComandos.LARGO_NOMBRE_COMANDO) {
            throw new Exception("Mensaje: " + StrMensaje + " ,respondido por el POS, no reconocido!");
        }
        String Comando = StrMensaje.substring(LargoPrefijo, LargoPrefijo + DiccionarioComandos.LARGO_NOMBRE_COMANDO);
        if (!Comando.equals(TRANSACT_COMANDO_WRAPPER_RESP)) {
            throw new Exception("Comando recibido: " + Comando + " no valido");
        }
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(StrMensaje);
        Tipo = super.ObtenerValorCampo(ListaCampos, TRANSACT_CAMPO_TIPO);
        if (Tipo.equals(WRAPPER_TIPO_ENVIO_AL_POS)) {
            Msg = super.ObtenerValorCampo(ListaCampos, TRANSACT_CAMPO_MSG);
            Msg = Msg.replaceAll("<RS>", "<FS>");
        }
        else  if (Tipo.equals(WRAPPER_TIPO_ERROR)) {
            Msg = super.ObtenerValorCampo(ListaCampos, TRANSACT_CAMPO_MSG);
            throw new NADGeneralException(Msg);
        } 
        else {
            throw new Exception("Tipo Mensaje recibido: " + Tipo + " no valido");

        }
    }
}
