/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.cliente.DocImpresion;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.Map;

/**
 *
 * @author yunior
 */
public class MensajeImpresionFPOSResp extends absMensajeRecibe{
  private final TipoMensajeRecibidoDesdePos tipoMensaje = TipoMensajeRecibidoDesdePos.RespuestaImprimir;
    private String Msg;
    private DocImpresion objDocImpr;
    
    public MensajeImpresionFPOSResp(String Msg)  throws Exception{
        this.Msg = Msg;
    }
  
    @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }
    
     public void CargarDocImpresion(DocImpresion objDocImpr) throws Exception{
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(Msg);
        objDocImpr.setNroSerie(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.IMPRIMIR_NOMBRE_CAMPO_NROSERIE));
        objDocImpr.setAppVer(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.IMPRIMIR_NOMBRE_CAMPO_APPVER));
        objDocImpr.setTipoImpresora(Integer.parseInt(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.IMPRIMIR_NOMBRE_CAMPO_TIPOIMPRESORA)));
        objDocImpr.setNomImpresora(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.IMPRIMIR_NOMBRE_CAMPO_NOMIMPRESORA));
        objDocImpr.setCopias(Integer.parseInt(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.IMPRIMIR_NOMBRE_CAMPO_COPIAS)));
        objDocImpr.setDisVoucher(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.IMPRIMIR_NOMBRE_CAMPO_DISVOUCHER)); 
     }
     
}
