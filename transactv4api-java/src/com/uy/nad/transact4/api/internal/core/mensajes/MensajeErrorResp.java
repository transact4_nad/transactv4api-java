/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.Map;

/**
 *
 * @author yunior
 */
public class MensajeErrorResp extends absMensajeRecibe {

    private final TipoMensajeRecibidoDesdePos tipoMensaje = TipoMensajeRecibidoDesdePos.RespuestaError;

    private String MensajeError;

    public MensajeErrorResp(String strRespuesta) throws Exception{
        CargarDatos(strRespuesta);
    }
    

    @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }

    private void CargarDatos(String strRespuesta) throws Exception{
       if(absMensajeRecibe.GetTipoMensajeFromStr(strRespuesta)!= tipoMensaje){
            throw new Exception("Error al crear mensaje del tipo " + tipoMensaje.toString() + " con respuesta: " + strRespuesta);
       } 
        MensajeError = strRespuesta;
    }
    public void LanzarExcepcionConError(Transaccion objTrn) throws Exception{
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(MensajeError);
        try{
        if (objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_OPERACION).equals("DEVOLUCION AUTOMATICA")) {
            objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DVA_CODIGO, super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESPADQ), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
            objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_DVA_MENSAJE, super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        } else {
            throw new Exception(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.RESPUESTA_ERROR_NOMBRE_CAMPO_MSGRESP).toUpperCase());
        }
        }catch(Exception ex){
         throw new Exception(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.RESPUESTA_ERROR_NOMBRE_CAMPO_MSGRESP).toUpperCase());
        }
        
    }

}
