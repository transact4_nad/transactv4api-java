/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

import com.uy.nad.transact4.api.IAPIListener;

/**
 *
 * @author yunior
 */
public class TransActAPICore {

    private IAPIListener listener;
    private POS objPos;
    private Concentrador objConcentrador;
    private Transaccion objTrn;

    public TransActAPICore(IAPIListener listener) {
        this.listener = listener;
    }
  void TarjetasCargarDatosTarjeta(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            objPos.EnviarPin(objTrn);
            objPos.TarjetasCargarDatosTarjeta(objTrn);
        } catch (Exception ex) {
            throw ex;
        } finally {
            // ReportarFin();
        }
    }
    void TarjetasProcesarTransaccion(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            objPos.EnviarPin(objTrn);
            objPos.TarjetasProcesarTransaccion(objTrn);
        } catch (Exception ex) {
            throw ex;
        } finally {
            // ReportarFin();
        }
    }
     void TarjetasConfirmarTransaccion(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            objPos.TarjetasConfirmarTransaccion(objTrn);
        } catch (Exception ex) {
            throw ex;
        } finally {
            // ReportarFin();
        }
    }
      void TarjetasConsultarConfirmacionTransaccion(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            objPos.TarjetasConsultarConfirmacionTransaccion(objTrn);
        } catch (Exception ex) {
            throw ex;
        } finally {
            // ReportarFin();
        }
    }
     

    void TarjetasProcesarCierre(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            if (objTrn.EsCentralizada()) {
              objConcentrador.ProcesarCierreCentralizado(objTrn);
            } else {
                objPos.EnviarPin(objTrn);
                objPos.TarjetasProcesarCierre(objTrn);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            // ReportarFin();
        }
    }
    void TarjetasConsultarEstadoCierre(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            NotificarAvance("Procesando Cierre");
            if (objTrn.EsCentralizada()) {
              objConcentrador.ConsultarEstadoCierreCentralizado(objTrn);
            } else {
                objPos.TarjetasConsultarEstadoCierre(objTrn);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } finally {
            // ReportarFin();
        }
        
    }
    
    void DispositivoStatus(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            objPos.EnviarPin(objTrn);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }

    }

    void DispositivoReiniciar(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            objPos.DispositivoReiniciar(objTrn);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
    void Reimprimir(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            objPos.TarjetasReimprimir(objTrn);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
    void LeerTarjeta(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            objPos.TarjetasLeerTarjeta(objTrn);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
    
    void Cancelar(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            objPos.Cancelar(objTrn);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
    
    void TarjetasReversarTransaccion(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            DepurarCamposConfiguracion(objTrn);
            if (objTrn.EsCentralizada()) {
              throw new Exception("No soporta esta operacion de forma centralizada");
            } else {
                objPos.EnviarPin(objTrn);
                objPos.TarjetasReversarTransaccion(objTrn);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        } 
    }


    void InicializarByTransaccion(Transaccion objTrn) throws Exception {
        this.objTrn = objTrn;
        Boolean ModoEmulacion = objTrn.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CFG_MODOEMULACION);
        this.objPos = new POS(ModoEmulacion,listener);
        Integer POSTipoCnx = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSTIPOCNX);
        String POSDireccionIP = objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSDIRECCION);
        Integer POSPuerto = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSPUERTO);
        Integer POSSegsTimeout = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSSEGSTIMEOUT);

        Integer GUITipo = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUITIPO);
        Integer GUIModo = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIMODO);
        Boolean GUIMostrarTouchPad = objTrn.GetCampo_Boolean(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIMOSTRARTOUCHPAD);
        Integer GUIPosicionX = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIPOSICIONX);
        Integer GUIPosicionY = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIPOSICIONY);

        Integer ImpresionTipo = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONTIPO);
        Integer ImpresionModo = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONMODO);
        Integer ImpresionTipoImpresora = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONTIPOIMPRESORA);
        String ImpresionNombreImpresora = objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONNOMBREIMPRESORA);
        Integer ImpresionCopias = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONCOPIAS);
        String ImpresionDireccionIP = objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONDIRECCION);
        Integer ImpresionPuerto = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONPUERTO);
        Integer ImpresionSegsTimeout = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONSEGSTIMEOUT);
        String EmpCod = objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_EMPCOD);
        String TermCod = objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_TERMCOD);
        this.objPos.SetearConfiguracionConexion(EmpCod,TermCod,POSTipoCnx, POSDireccionIP, POSPuerto, POSSegsTimeout);  
        this.objPos.SetearConfiguracionGUI(GUITipo, GUIModo, GUIMostrarTouchPad, GUIPosicionX, GUIPosicionY);
        this.objPos.SetearConfiguracionImpresion(ImpresionTipo, ImpresionModo, ImpresionTipoImpresora, ImpresionNombreImpresora, ImpresionCopias, ImpresionDireccionIP, ImpresionPuerto, ImpresionSegsTimeout);
        
        String URLConcentrador = objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CFG_URLCONCENTRADOR);
        objConcentrador = new Concentrador(ModoEmulacion,listener,URLConcentrador);
        
    }
    
     void ReiniciarDispositivo(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            objPos.EnviarPin(objTrn);
            objPos.TarjetasProcesarTransaccion(objTrn);
        } catch (Exception ex) {
            throw ex;
        } finally {
            // ReportarFin();
        }
    }
     
      void StatusDispositivo(Transaccion objTrn) throws Exception {
        try {
            InicializarByTransaccion(objTrn);
            objPos.EnviarPin(objTrn);
        } catch (Exception ex) {
            throw ex;
        } finally {
            // ReportarFin();
        }
    }

    private void DepurarCamposConfiguracion(Transaccion objTrn) {
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIMOSTRARTOUCHPAD);
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIPOSICIONX);
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUIPOSICIONY);

        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_MODOEMULACION);
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSTIPO);
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSTIPOCNX);
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSDIRECCION);
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSPUERTO);
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_POSSEGSTIMEOUT);

        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONDIRECCION);
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONPUERTO);
        objTrn.EliminarCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_IMPRESIONSEGSTIMEOUT);
    }
    
    private void NotificarAvance(String mensaje){
        if(listener!= null){
             new ThreadNotificarAvance(listener, mensaje).start();
        }
    }

}
