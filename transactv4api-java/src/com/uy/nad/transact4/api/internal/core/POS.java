/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

import com.uy.nad.transact4.api.internal.cliente.ICliente;
import com.uy.nad.transact4.api.internal.cliente.TCPClientePos;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeErrorResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajePingFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajePingFPOSResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeReportarAvanceEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeReportarAvanceResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnProcesarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnProcesarFPOSResp;
import com.uy.nad.transact4.api.internal.core.mensajes.absMensajeEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.absMensajeRecibe;
import com.uy.nad.transact4.api.IAPIListener;
import com.uy.nad.transact4.api.IMetadatosSolicitudCampoPosiblesValores;
import com.uy.nad.transact4.api.internal.cliente.BridgeClientePos;
import com.uy.nad.transact4.api.internal.cliente.DocImpresion;
import com.uy.nad.transact4.api.internal.cliente.ImpresionBridge;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeCancelarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeCierreConsultarEstadoEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeCierreConsultarEstadoResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeCierreProcesarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeCierreProcesarFPOSResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeImpresionFPOSResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajePedirCampoEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajePedirCampoResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeReiniciarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeReiniciarFPOSResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnConfirmarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnConfirmarFPOSResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnConsultarConfirmacionEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnConsultarConfirmacionResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnRetornarTarjetaFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnRetornarTarjetaFPOSResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnReversarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnReversarFPOSResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeImpresionFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeLeerTarjetaFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeLeerTarjetaFPOSResp;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeReimprimirFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeReimprimirFPOSResp;


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yunior
 */
public class POS {

    private ICliente conexionCliente;
    private IAPIListener apiListener;
    private Boolean ModoEmulacion;

    private Integer POSTipoCnx;
    private String POSDireccionIP;
    private Integer POSPuerto;
    private Integer POSSegsTimeout;

    private Integer GUITipo;
    private Integer GUIModo;
    private Boolean GUIMostrarTouchPad;
    private Integer GUIPosicionX;
    private Integer GUIPosicionY;

    private Integer ImpresionTipo;
    private Integer ImpresionModo;
    private Integer ImpresionTipoImpresora;
    private String ImpresionNombreImpresora;
    private Integer ImpresionCopias;
    private String ImpresionDireccionIP;
    private Integer ImpresionPuerto;
    private Integer ImpresionSegsTimeout;
    private final Integer SEGUNDOS_TIMEOUT_PING = 10;
    private Integer RETARDO = 0;

    public POS(Boolean modoEmulacion, IAPIListener apiListener) {
        this.conexionCliente = null;
        this.apiListener = apiListener;
        this.ModoEmulacion = modoEmulacion;
    }

    public void SetearConfiguracionConexion(String EmpCod,String TermCod,Integer POSTipoCnx, String POSDireccionIP, Integer POSPuerto, Integer POSSegsTimeout) throws Exception {
        
        this.POSTipoCnx = POSTipoCnx;
        this.POSDireccionIP = POSDireccionIP;
        this.POSPuerto = POSPuerto;
        this.POSSegsTimeout = POSSegsTimeout;
          if (null == this.POSTipoCnx){
              throw new Exception("Tipo de conexion no soportada");
          }
          else switch (this.POSTipoCnx) {
            case 0:
                conexionCliente = new BridgeClientePos(EmpCod,TermCod, POSDireccionIP, POSPuerto, POSSegsTimeout * 1000);
                break;
            case 1:
                conexionCliente = new TCPClientePos(POSDireccionIP, POSPuerto, POSSegsTimeout * 1000);
                break;
            default:
                throw new Exception("Tipo de conexion no soportada");
        }
    }

    public void SetearConfiguracionGUI(Integer GUITipo, Integer GUIModo, Boolean GUIMostrarTouchPad, Integer GUIPosicionX, Integer GUIPosicionY) throws Exception {
        if(GUITipo ==9 || GUITipo ==0){
        this.GUITipo = GUITipo;
        this.GUIModo = GUIModo;
        this.GUIMostrarTouchPad = GUIMostrarTouchPad;
        this.GUIPosicionX = GUIPosicionX;
        this.GUIPosicionY = GUIPosicionY;
        
        }else{
            throw new Exception("GUI TIPO NO SOPORTADO VALORES PERMITIDOS 0 y 9");
        }        
    }

    public void SetearConfiguracionImpresion(Integer ImpresionTipo, Integer ImpresionModo, Integer ImpresionTipoImpresora, String ImpresionNombreImpresora,
            Integer ImpresionCopias, String ImpresionDireccionIP, Integer ImpresionPuerto, Integer ImpresionSegsTimeout) throws Exception {
        this.ImpresionTipo = ImpresionTipo;
        this.ImpresionModo = ImpresionModo;
        this.ImpresionTipoImpresora = ImpresionTipoImpresora;
        this.ImpresionNombreImpresora = ImpresionNombreImpresora;
        this.ImpresionCopias = ImpresionCopias;
        this.ImpresionDireccionIP = ImpresionDireccionIP;
        this.ImpresionPuerto = ImpresionPuerto;
        this.ImpresionSegsTimeout = ImpresionSegsTimeout;
    }

    public void EnviarPin(Transaccion objTrn) throws Exception {
        String strResp;
        if (ModoEmulacion) {
            strResp = new ModoEmulacion().EnviarPin();
        } else {
            MensajePingFPOSEnvio objMensajeEnvioAlFPOS = new MensajePingFPOSEnvio(objTrn);
            String strMsg = objMensajeEnvioAlFPOS.ObtenerMensajePOS();
            conexionCliente.SetTimeout(SEGUNDOS_TIMEOUT_PING * 1000);
            conexionCliente.Conectar();
            strResp = conexionCliente.EnviarMensaje(strMsg);
            conexionCliente.Desconectar();
        }
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    private String EnviarDatosAlPOS(absMensajeEnvio objMensajeEnvioAlFPOS) throws Exception {
        String strResp;
        if (ModoEmulacion) {
            strResp = new ModoEmulacion().EnviarDatosAlPOS(objMensajeEnvioAlFPOS);
            System.out.println("MODOEMULACION MSG= " + strResp);
        } else {
            System.out.println("com.uy.nad.transact4.api.internal.core.POS.EnviarDatosAlPOS()");
            String strMsg = objMensajeEnvioAlFPOS.ObtenerMensajePOS();
            System.out.println("strMsg= " + strMsg);
            conexionCliente.SetTimeout(POSSegsTimeout * 1000);
            conexionCliente.Conectar();
            strResp = conexionCliente.EnviarMensaje(strMsg);
            conexionCliente.Desconectar();
            System.out.println("strResp= " + strResp);
        }
        return strResp;
    }

    public void TarjetasCargarDatosTarjeta(Transaccion objTrn)  throws Exception {
        MensajeTrnRetornarTarjetaFPOSEnvio objMsgProcTrn = new MensajeTrnRetornarTarjetaFPOSEnvio(objTrn);
        String strResp = EnviarDatosAlPOS(objMsgProcTrn);
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    public void TarjetasProcesarTransaccion(Transaccion objTrn) throws Exception {
        MensajeTrnProcesarFPOSEnvio objMsgProcTrn = new MensajeTrnProcesarFPOSEnvio(objTrn);
        String strResp = EnviarDatosAlPOS(objMsgProcTrn);
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    public void TarjetasConfirmarTransaccion(Transaccion objTrn) throws Exception {
        String strResp = "";
        MensajeTrnConfirmarFPOSEnvio objMsgConfTrn = new MensajeTrnConfirmarFPOSEnvio(objTrn);
        strResp = EnviarDatosAlPOS(objMsgConfTrn);
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    public void TarjetasReversarTransaccion(Transaccion objTrn) throws Exception {
        String strResp = "";
        MensajeTrnReversarFPOSEnvio objMsgRevTrn = new MensajeTrnReversarFPOSEnvio(objTrn);
        strResp = EnviarDatosAlPOS(objMsgRevTrn);
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    public void TarjetasConsultarConfirmacionTransaccion(Transaccion objTrn) throws Exception {
        String strResp = "";
        MensajeTrnConsultarConfirmacionEnvio objMsgEnvio = new MensajeTrnConsultarConfirmacionEnvio(objTrn);
        strResp = EnviarDatosAlPOS(objMsgEnvio);
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    public void TarjetasProcesarCierre(Transaccion objTrn) throws Exception {
        MensajeCierreProcesarFPOSEnvio objMsgCierreProc = new MensajeCierreProcesarFPOSEnvio(objTrn);
        String strResp = EnviarDatosAlPOS(objMsgCierreProc);
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    public void TarjetasConsultarEstadoCierre(Transaccion objTrn) throws Exception {
        String strResp = "";
        MensajeCierreConsultarEstadoEnvio objMsgConsEstadoCie = new MensajeCierreConsultarEstadoEnvio(objTrn);
        strResp = EnviarDatosAlPOS(objMsgConsEstadoCie);
        ProcesarRespuestaPOS(objTrn, strResp);

    }

    public void TarjetasReimprimir(Transaccion objTrn)  throws Exception {
        String strResp = "";
         MensajeReimprimirFPOSEnvio objMsgReiniciarFPOSEnvio = new MensajeReimprimirFPOSEnvio(objTrn);
        strResp = EnviarDatosAlPOS(objMsgReiniciarFPOSEnvio);
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    public void TarjetasLeerTarjeta(Transaccion objTrn)  throws Exception{
        String strResp = "";
        MensajeLeerTarjetaFPOSEnvio objMsgLeerTarjetaFPOSEnvio = new MensajeLeerTarjetaFPOSEnvio(objTrn);
        strResp = EnviarDatosAlPOS(objMsgLeerTarjetaFPOSEnvio);
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    public void DispositivoReiniciar(Transaccion objTrn) throws Exception {
        String strResp = "";
        MensajeReiniciarFPOSEnvio objMsgReiniciarFPOSEnvio = new MensajeReiniciarFPOSEnvio(objTrn);
        strResp = EnviarDatosAlPOS(objMsgReiniciarFPOSEnvio);
        ProcesarRespuestaPOS(objTrn, strResp);
    }
   
     public void Cancelar(Transaccion objTrn) throws Exception {
        String strResp = "";
        MensajeCancelarFPOSEnvio objMsgReiniciarFPOSEnvio = new MensajeCancelarFPOSEnvio(objTrn);
        strResp = EnviarDatosAlPOS(objMsgReiniciarFPOSEnvio);
        ProcesarRespuestaPOS(objTrn, strResp);
    }

    //----------------------------
    private void ProcesarRespuestaPOS(Transaccion objTrn, String strResp) throws Exception {
        System.out.println("com.uy.nad.transact4.api.internal.core.POS.ProcesarRespuestaPOS()");
        System.out.println("strResp = " + strResp);
        absMensajeRecibe objMensajeRecibidoDesdeFPOS = absMensajeRecibe.ConstruirMensajeRecibido(strResp);

        switch (objMensajeRecibidoDesdeFPOS.getTipoMensajeRecibido()) {
            case RespuestaError:
                ProcesarRespuestaError(objTrn, (MensajeErrorResp) objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaPing:
                ProcesarRespuestaPing(objTrn, (MensajePingFPOSResp) objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaRetornarTarjetaTrn:
                CargarRespuestaCargarIDatosTarjetaEnIDatosTarjeta(objTrn, (MensajeTrnRetornarTarjetaFPOSResp) objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaProcesarTrn:
                CargarRespuestaProcesarITransaccionEnITransaccion(objTrn, (MensajeTrnProcesarFPOSResp) objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaConfirmarTrn:
                CargarRespuestaConfirmarITransaccionEnITransaccion(objTrn, (MensajeTrnConfirmarFPOSResp) objMensajeRecibidoDesdeFPOS);
                break;
            case ReportarAvanceEnAPI:
                ReportarAvanceEnAPI(objTrn, (MensajeReportarAvanceResp) objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaProcesarCierre:
                CargarRespuestaProcesarCierreEnITransaccion(objTrn, (MensajeCierreProcesarFPOSResp) objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaConsultaEstadoCierre:
                CargarRespuestaConsultarEstadoCierreEnITransaccion(objTrn, (MensajeCierreConsultarEstadoResp) objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaActualizarDispositivo:
                CargarRespuestaTransaccionReiniciarDispositivo(objTrn, (MensajeReiniciarFPOSResp) objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaReversarTrn:
                CargarRespuestaReversarITransaccionEnITransaccion(objTrn, (MensajeTrnReversarFPOSResp) objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaConsultaConfirmacionTrn:
                CargarRespuestaConsultarConfirmacionTransaccion(objTrn, (MensajeTrnConsultarConfirmacionResp) objMensajeRecibidoDesdeFPOS);
                break;
            case PedirCampoEnAPI:
                PedirCampoEnAPI(objTrn, (MensajePedirCampoResp)objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaImprimir:
                Imprimir(objTrn, (MensajeImpresionFPOSResp)objMensajeRecibidoDesdeFPOS);
                break;
            case RespuestaLeerTarjeta:
                CargarRespuestaLeerTarjetaEnITransaccion(objTrn, (MensajeLeerTarjetaFPOSResp)objMensajeRecibidoDesdeFPOS);
                break;
                

        }

    }
         private void  CargarRespuestaReimprimirEnITransaccion( Transaccion objTrn, MensajeReimprimirFPOSResp objMensResp) throws Exception{
             objMensResp.CargarRespuestaEnTransaccion(objTrn);
         }
        private void Imprimir(Transaccion objTrn, MensajeImpresionFPOSResp objMensResp) throws Exception {
            long tickInicioOpearacion = System.currentTimeMillis();

            DocImpresion docImp = new DocImpresion();
            objMensResp.CargarDocImpresion(docImp);
            ImpresionBridge objImpresion = new ImpresionBridge(POSDireccionIP, POSPuerto, POSSegsTimeout);
            objImpresion.Imprimir(docImp);
          
             MensajeImpresionFPOSEnvio objMensResultImprimir = new MensajeImpresionFPOSEnvio(docImp);
             long tickFinOpearacion = System.currentTimeMillis();
             long milisegundosOperacion = ((tickFinOpearacion - tickInicioOpearacion));
             if (RETARDO > 0 && milisegundosOperacion < RETARDO) {
                Thread.sleep( RETARDO - milisegundosOperacion);
            } else {
                Thread.sleep(250);
            }
        String respuesta = EnviarDatosAlPOS(objMensResultImprimir);
        ProcesarRespuestaPOS(objTrn, respuesta);
   }

    private void ProcesarRespuestaPing(Transaccion objTrn, MensajePingFPOSResp objMensResp) throws Exception {
        if (objMensResp.getCodResp() != 0) {
            String msgErrorPin = "POS NO ESTA LISTO";
            if (!objMensResp.getMsgResp().isEmpty()) {
                msgErrorPin = msgErrorPin.concat(" - " + objMensResp.getMsgResp());
            }
            throw new Exception(msgErrorPin);
        }

        objMensResp.CargarRespuestaEnTransaccion(objTrn);

        this.RETARDO = objMensResp.getRetardo();
        if (RETARDO > 0) {
            Thread.sleep(RETARDO);
        } else {
            Thread.sleep(250);
        }
    }

    private void CargarRespuestaProcesarITransaccionEnITransaccion(Transaccion objTrn, MensajeTrnProcesarFPOSResp objMensResp) throws Exception {
        objMensResp.CargarRespuestaEnTransaccion(objTrn);
    }
    private void  CargarRespuestaCargarIDatosTarjetaEnIDatosTarjeta(Transaccion objTrn, MensajeTrnRetornarTarjetaFPOSResp objMensResp) throws Exception{
         objMensResp.CargarRespuestaEnTransaccion(objTrn);
     }
    
    private void CargarRespuestaLeerTarjetaEnITransaccion(Transaccion objTrn, MensajeLeerTarjetaFPOSResp objMensajeRecibidoDesdeFPOS) throws Exception{
         objMensajeRecibidoDesdeFPOS.CargarRespuestaEnTransaccion(objTrn);
    }

    private void ReportarAvanceEnAPI(Transaccion objTrn, MensajeReportarAvanceResp objMensResp) throws Exception {
        int GUITipo = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUITIPO);
        if (GUITipo == 9 ||GUITipo == 0 ) {
            if (this.apiListener == null) {
                throw new Exception("LISTENER NO SETEADO");
            } else {
                new ThreadNotificarAvance(apiListener, objMensResp.getDescripcion()).start();
            }
        } else {
            throw new Exception("GUITIPO NO SOPORTADO AUN");
        }
        if (RETARDO > 0) {
            Thread.sleep(RETARDO);
        }
        String respuesta = EnviarDatosAlPOS(new MensajeReportarAvanceEnvio(0));
        ProcesarRespuestaPOS(objTrn, respuesta);
    }
    
      private void PedirCampoEnAPI(Transaccion objTrn, MensajePedirCampoResp objMensResp) throws Exception{
          int GUITipo = objTrn.GetCampo_Integer(DiccionarioCampos.TRANSACT_CAMPO_CFG_GUITIPO);
        if (GUITipo == 9|| GUITipo == 0) {
            if (this.apiListener == null) {
                throw new Exception("LISTENER NO SETEADO");
            } else {
                MetadatosSolicitudCampo campo = new MetadatosSolicitudCampo();
                    campo.setWfCampoKey(objMensResp.getKey());
                    campo.setWfCampoTpoDatos(objMensResp.getTipoDatos());
                    campo.setWfCampoDescripcion(objMensResp.getDescripcion());
                    campo.setWfCampoMensaje(objMensResp.getMensaje());
                    campo.setWfCampoLargoMin(StringToInt(objMensResp.getLargoMinimo()));
                    campo.setWfCampoLargoMax(StringToInt(objMensResp.getLargoMaximo()));
                    campo.setWfCampoValorDesde(StringToInt(objMensResp.getValoresDesde()));
                    campo.setWfCampoValorHasta(StringToInt(objMensResp.getValoresHasta()));
                    campo.setWfCampoMascara(objMensResp.getMascara());
                    campo.setWfCampoRequerido(StringToBoolean(objMensResp.getRequerido()));
                    campo.setWfCampoNoLegible(StringToBoolean(objMensResp.getNoElegible()));
                    List<IMetadatosSolicitudCampoPosiblesValores> listaPosiblesValores = new ArrayList<IMetadatosSolicitudCampoPosiblesValores>();
                    String auxMul = objMensResp.getMultiSelecOpcion();
                    if(!auxMul.isEmpty()){
                        String arr[]= auxMul.split("\\|");
                       for(String item: arr){
                           String arr2[]= item.split("@"); {
                            if(arr2.length > 1){
                                MetadatosSolicitudCampoPosiblesValores objItem = new MetadatosSolicitudCampoPosiblesValores();
                                objItem.setValor(arr2[0]);
                                objItem.setDescripcion(arr2[1]);
                                listaPosiblesValores.add(objItem);
                            }   
                           }
                           
                       } 
                        
                    }
                    campo.setListaPosiblesValores(listaPosiblesValores);       
                    campo.setValorPorDefecto(objMensResp.getValorPorDefecto());
                    campo.setValor(null);
                    campo.setCancelado(false);
                    campo.setMostrarTouchPad(false);
                    campo.setPosicionX(0);
                    campo.setPosicionY(0);
                    campo.setSoloLectura(StringToBoolean(objMensResp.getSoloLectura()));
                    campo.setSegundosTimeoutIngreso(StringToInt(objMensResp.getSegundosTimeout()));
                    this.apiListener.evtSolicitarCampoEnAPI(campo);
                    if (RETARDO > 0) {
                       Thread.sleep(RETARDO);
                    }
                    String respuesta = EnviarDatosAlPOS(new MensajePedirCampoEnvio("0","",campo.getWfCampoKey(),campo.getValor(),campo.getCancelado()));
                    ProcesarRespuestaPOS(objTrn, respuesta);
                    
            }
        } else {
            throw new Exception("GUITIPO NO SOPORTADO AUN");
        }
      
       
      }
    
      private boolean StringToBoolean(String valor){
        if(valor.toLowerCase().equals("true") || valor.toLowerCase().equals("t") || valor.toLowerCase().equals("1") || 
                valor.toLowerCase().equals("v"))
        {
            return true;
        }
        else{
            return false;
        }
      }
       private Integer StringToInt(String valor){
           if(valor.isEmpty())
               return 0;
           else
               return Integer.parseInt(valor);
      }
    
    private void ProcesarRespuestaError(Transaccion objTrn, MensajeErrorResp objMensResp) throws Exception {
        objMensResp.LanzarExcepcionConError(objTrn);
    }

    private void CargarRespuestaProcesarCierreEnITransaccion(Transaccion objTrn, MensajeCierreProcesarFPOSResp mensajeCierreProcesarFPOSResp) throws Exception {
        mensajeCierreProcesarFPOSResp.CargarRespuestaEnTransaccion(objTrn);
    }

    private void CargarRespuestaConfirmarITransaccionEnITransaccion(Transaccion objTrn, MensajeTrnConfirmarFPOSResp mensajeTrnConfirmarFPOSResp) throws Exception {
        mensajeTrnConfirmarFPOSResp.CargarRespuestaEnTransaccion(objTrn);
    }
    
    private void CargarRespuestaReversarITransaccionEnITransaccion(Transaccion objTrn, MensajeTrnReversarFPOSResp mensajeReversarFPOS)throws Exception {
    mensajeReversarFPOS.CargarRespuestaEnTransaccion(objTrn);
    }

    private void CargarRespuestaConsultarEstadoCierreEnITransaccion(Transaccion objTrn, MensajeCierreConsultarEstadoResp mensajeCierreConsultarEstadoResp) throws Exception {
        mensajeCierreConsultarEstadoResp.CargarRespuestaEnTransaccion(objTrn);
    }

    private void CargarRespuestaTransaccionReiniciarDispositivo(Transaccion objTrn, MensajeReiniciarFPOSResp objMensResp) throws Exception {
        objMensResp.CargarRespuestaEnTransaccion(objTrn);
    }
     private void CargarRespuestaConsultarConfirmacionTransaccion(Transaccion objTrn, MensajeTrnConsultarConfirmacionResp objMensResp)throws Exception {
        objMensResp.CargarRespuestaEnTransaccion(objTrn);
    }
}
