/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;

/**
 *
 * @author Yunior
 */
public class MensajePedirCampoEnvio extends absMensajeEnvio {

    String codResp;
    String msgResp;
    String key;
    Object valor;
    Boolean cancelado;

    public MensajePedirCampoEnvio(String codResp, String msgResp, String key, Object valor, Boolean cancelado) {
        this.codResp = codResp;
        this.msgResp = msgResp;
        this.key = key;
        this.valor = valor;
        this.cancelado = cancelado;
    }

    @Override
    public String ObtenerMensajePOS() throws Exception {
          String canceladoStr = "FALSE";
        if(cancelado){
            canceladoStr = "TRUE";
        }
        String valorStr="";
        if(this.valor !=null){
         valorStr = this.valor.toString();
        }
        String strMensaje = DiccionarioComandos.COMANDO_RESULTADO_PEDIR_CAMPO;
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.RESPUESTA_SOLICITUD_NOMBRE_CAMPO_CODRESP, this.codResp);
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.RESPUESTA_SOLICITUD_NOMBRE_CAMPO_MSGRESP, this.msgResp);
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_KEY, this.key);
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.RESPUESTA_SOLICITUD_NOMBRE_CAMPO_VALOR, valorStr);
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.RESPUESTA_SOLICITUD_NOMBRE_CAMPO_CANCELADO, canceladoStr);
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }

}
