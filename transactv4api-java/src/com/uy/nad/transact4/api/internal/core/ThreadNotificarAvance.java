/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

import com.uy.nad.transact4.api.IAPIListener;

/**
 *
 * @author yunior
 */
public class ThreadNotificarAvance extends Thread{
    private IAPIListener listener;
    private String mensaje;
    ThreadNotificarAvance(IAPIListener listener,String mensaje){
        this.listener = listener;
        this.mensaje = mensaje;
    }
    public void run() {
      listener.evtNotificadAvance(mensaje);
 } 
}
