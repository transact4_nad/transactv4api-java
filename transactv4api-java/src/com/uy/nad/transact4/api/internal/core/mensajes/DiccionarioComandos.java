package com.uy.nad.transact4.api.internal.core.mensajes;

public class DiccionarioComandos {

    //FORMATO MENSAJES
    public static final int LARGO_NOMBRE_COMANDO = 6;
    public static final String INICIO_ELEMENTO = "<";
    public static final String FIN_ELEMENTO = ">";
    public static final String PREFIJO_MENSAJE = "INICMD";
    public static final String SUFIJO_MENSAJE = "FINCMD";
    public static final String CAMPO_LRC = "LRC";
    public static final int LARGO_LRC = 5;
    public static final String NOMBRE_CAMPO = "CAMPO";
    public static final String VALOR_CAMPO = "VALOR";
    public static final String SEPARADOR_CAMPO = "FS";

    //COMANDOS
    public static final String COMANDO_RESULTADO_PEDIR_CAMPO = "ACARSP";
    public static final String COMANDO_RESULTADO_REPORTARAVANCEENAPI = "ARARSP";
    public static final String COMANDO_PING = "APIRQC";
    public static final String COMANDO_RETORNARTARJETATRN = "ATJRQC";
    public static final String COMANDO_PROCESARTRN = "ATRRQC";
    public static final String COMANDO_CONFIRMARTRN = "ATCRQC";
    public static final String COMANDO_CONSULTARCONFIRMACIONTRN = "ATORQC";
    public static final String COMANDO_REVERSARTRN = "ARVRQC";
    public static final String COMANDO_RESULTADO_IMPRIMIR = "PRNRSP";
    public static final String COMANDO_PROCESARCIE = "ACRRQC";
    public static final String COMANDO_CONSULTARESTADOCIE = "ACORQC";
    public static final String COMANDO_REIMPRIMIR = "PRRRQC";
    public static final String COMANDO_LEERTARJETA = "AMSRQC";
    public static final String COMANDO_ACTUALIZAR_DISPOSITIVO = "UPDRQC";
    public static final String COMANDO_CANCELAR = "CANRQC";


    public static final String COMANDO_RESPUESTAPING = "APIRSP";
    public static final String COMANDO_PEDIRCAMPOENAPI = "ACARQC";
    public static final String COMANDO_REPORTARAVANCEENAPI = "ARARQC";
    public static final String COMANDO_RESPUESTARETORNARTARJETATRN = "ATJRSP";
    public static final String COMANDO_RESPUESTATRNPROCESAR = "ATRRSP";
    public static final String COMANDO_RESPUESTATRNCONFIRMAR = "ATCRSP";
    public static final String COMANDO_RESPUESTATRNCONSULTARCONFIRMACION = "ATORSP";
    public static final String COMANDO_RESPUESTATRNREVERSAR = "ARVRSP";
    public static final String COMANDO_RESPUESTACIEPROCESAR = "ACRRSP";
    public static final String COMANDO_RESPUESTACIECONSULTARESTADO = "ACORSP";
    public static final String COMANDO_IMPRIMIR = "PRNRQC";
    public static final String COMANDO_RESPUESTAREIMPRIMIR = "PRRRSP";
    public static final String COMANDO_RESPUESTALEERTARJETA = "AMSRSP";
    public static final String COMANDO_RESPUESTAERROR = "ERRRSP";
    public static final String COMANDO_RESPUESTAACTUALIZAR_DISPOSITIVO = "UPDRSP";
    
    //
}
