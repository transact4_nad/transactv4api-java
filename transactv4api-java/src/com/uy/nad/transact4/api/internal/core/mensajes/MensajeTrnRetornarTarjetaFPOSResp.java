/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.Map;

/**
 *
 * @author Yunior
 */
public class MensajeTrnRetornarTarjetaFPOSResp extends absMensajeRecibe{
    private final TipoMensajeRecibidoDesdePos tipoMensaje = TipoMensajeRecibidoDesdePos.RespuestaRetornarTarjetaTrn;
    private String Msg;
    public MensajeTrnRetornarTarjetaFPOSResp(String Msg) {
     this.Msg = Msg;
    }
    @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }
     public void CargarRespuestaEnTransaccion(Transaccion objTrn ) throws Exception{
      Map<String,String> ListaCampos = super.ObtenerCamposMensaje(Msg);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP, ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP, ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
         String TRJRSPXML  = ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TRJRSPXML);
        if(!TRJRSPXML.trim().isEmpty()){
            super.CargarTransaccionByXML(objTrn, TRJRSPXML, "TRJRSPXML");
        }
     }
    
}
