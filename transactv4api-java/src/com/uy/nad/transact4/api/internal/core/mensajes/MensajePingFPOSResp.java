package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.Map;

public class MensajePingFPOSResp extends absMensajeRecibe {

    private final TipoMensajeRecibidoDesdePos tipoMensaje = TipoMensajeRecibidoDesdePos.RespuestaPing;

    public MensajePingFPOSResp(String strRespuesta) throws Exception {
       CargarDatos(strRespuesta);
    }

    @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }

    private Integer CodResp;
    private String MsgResp;
    private Integer retardo;

    public Integer getCodResp() {
        return CodResp;
    }

    public String getMsgResp() {
        return MsgResp;
    }

    public Integer getRetardo() {
        return retardo;
    }

    private void CargarDatos(String strRespuesta) throws Exception{
        if (absMensajeRecibe.GetTipoMensajeFromStr(strRespuesta) != tipoMensaje) {
            throw new Exception("Error al crear mensaje del tipo " + tipoMensaje.toString() + " con respuesta: " + strRespuesta);
        }
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(strRespuesta);
        CodResp = Integer.parseInt(super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP));
        MsgResp = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP);
        String RetardoEntrePeticiones = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_RETARDO);
        if (!RetardoEntrePeticiones.isEmpty()) {
            retardo = Integer.parseInt(RetardoEntrePeticiones);
        } else {
            retardo = 0;
        }
    }
    
     public void CargarRespuestaEnTransaccion(Transaccion objTrn) throws Exception{
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP, CodResp.toString(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP, MsgResp, false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
     }
    
}
