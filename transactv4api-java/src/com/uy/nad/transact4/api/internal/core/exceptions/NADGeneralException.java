/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.exceptions;

/**
 *
 * @author Yunior
 */
public class NADGeneralException extends Exception{
    private String Mensaje;
    
    public NADGeneralException(String Mensaje){
     super(Mensaje);
     this.Mensaje = Mensaje;
    }
    public String getMessage() {
        return Mensaje;
    }
}
