/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.Map;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class MensajeReimprimirFPOSResp extends absMensajeRecibe
{
      private final absMensajeRecibe.TipoMensajeRecibidoDesdePos tipoMensaje = absMensajeRecibe.TipoMensajeRecibidoDesdePos.RespuestaReimprimir;
      private String Msg = "";

      public MensajeReimprimirFPOSResp(String strRespuesta) throws Exception {
            if(absMensajeRecibe.GetTipoMensajeFromStr(strRespuesta) != tipoMensaje) {
                 throw new Exception("Error al crear mensaje del tipo " + tipoMensaje.toString() + " con respuesta: " + strRespuesta);
            }
          this.Msg = strRespuesta;
    }
      
      @Override
    public absMensajeRecibe.TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }

    public void CargarRespuestaEnTransaccion(Transaccion objTrn) throws Exception{
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(Msg);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_REIMPRIMIR_RESPUESTA_CAMPO_CODRESP, super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_REIMPRIMIR_RESPUESTA_CAMPO_CODRESP), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_REIMPRIMIR_RESPUESTA_CAMPO_MSGRESP, ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_REIMPRIMIR_RESPUESTA_CAMPO_MSGRESP), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
    }
  

   
    

   

}
