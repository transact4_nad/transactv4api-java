/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import java.util.Map;

/**
 *
 * @author yunior
 */
public class MensajeReportarAvanceResp extends absMensajeRecibe{
    private final TipoMensajeRecibidoDesdePos tipoMensaje = TipoMensajeRecibidoDesdePos.ReportarAvanceEnAPI;
    private String descripcion;

    public MensajeReportarAvanceResp(String strRespuesta) throws Exception{
        CargarDatos(strRespuesta);
    }

    public String getDescripcion() {
        return descripcion;
    }
    
    
    @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }

    private void CargarDatos(String strRespuesta) throws Exception{
       if (absMensajeRecibe.GetTipoMensajeFromStr(strRespuesta) != tipoMensaje) {
            throw new Exception("Error al crear mensaje del tipo " + tipoMensaje.toString() + " con respuesta: " + strRespuesta);
        }
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(strRespuesta);
        descripcion = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.REPORTE_NOMBRE_CAMPO_DESCRIPCION);
    }
    
}
