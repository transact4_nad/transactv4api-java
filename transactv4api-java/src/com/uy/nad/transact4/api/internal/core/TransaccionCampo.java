/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

/**
 *
 * @author yunior
 */
public class TransaccionCampo {
    private String nombreCampo;
    private Object valorCampo;
    private boolean permiteModificar;
    private String tipoDatos;

    public TransaccionCampo() {
    }

    public TransaccionCampo(String nombreCampo, Object valorCampo, boolean permiteModificar, String tipoDatos) {
        this.nombreCampo = nombreCampo;
        this.valorCampo = valorCampo;
        this.permiteModificar = permiteModificar;
        this.tipoDatos = tipoDatos;
    }
    

    public String getNombreCampo() {
        return nombreCampo;
    }

    public void setNombreCampo(String nombreCampo) {
        this.nombreCampo = nombreCampo;
    }

    public Object getValorCampo() {
        return valorCampo;
    }

    public void setValorCampo(Object valorCampo) {
        this.valorCampo = valorCampo;
    }

    public boolean isPermiteModificar() {
        return permiteModificar;
    }

    public void setPermiteModificar(boolean permiteModificar) {
        this.permiteModificar = permiteModificar;
    }

    public String getTipoDatos() {
        return tipoDatos;
    }

    public void setTipoDatos(String tipoDatos) {
        this.tipoDatos = tipoDatos;
    }
    
}
