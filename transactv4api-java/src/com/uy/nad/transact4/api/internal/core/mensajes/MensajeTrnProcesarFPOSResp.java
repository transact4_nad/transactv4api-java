/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import java.util.Map;

/**
 *
 * @author yunior
 */
public class MensajeTrnProcesarFPOSResp extends absMensajeRecibe {

    private final TipoMensajeRecibidoDesdePos tipoMensaje = TipoMensajeRecibidoDesdePos.RespuestaProcesarTrn;
    private String Msg ;

    public MensajeTrnProcesarFPOSResp(String strRespuesta) throws Exception {
        Msg = strRespuesta;
    }

    @Override
    public TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }

    public void CargarRespuestaEnTransaccion(Transaccion objTrn) throws Exception{
        Map<String,String> ListaCampos = super.ObtenerCamposMensaje(Msg);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP, ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESP),
                false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP, ObtenerValorCampo(ListaCampos,DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_MSGRESP),
                false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESPADQ, ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_CODRESPADQ), 
                false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_APROBADA, Boolean.parseBoolean(ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_APROBADA)),
                false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_BOOLEAN);
        String TRNRSPXML = ObtenerValorCampo(ListaCampos, DiccionarioCampos.TRANSACT_CAMPO_RESPUESTA_TRNRSPXML);
        super.CargarTransaccionByXML(objTrn, TRNRSPXML, "TRNRSPXML");
    }

}
