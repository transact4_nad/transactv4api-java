/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import com.uy.nad.transact4.api.internal.core.TransaccionCampo;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class MensajeReimprimirFPOSEnvio extends absMensajeEnvio{
     private Transaccion objTrn;

    public MensajeReimprimirFPOSEnvio(Transaccion objTrn) {
        this.objTrn = objTrn;
    }
    
    @Override
    public String ObtenerMensajePOS() throws Exception {
         String strMensaje = "";
        strMensaje += DiccionarioComandos.COMANDO_REIMPRIMIR;
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_REIMPRIMIR_PRRRQCXML, ObtenerPRRRQCXMLByTransaccion(objTrn));
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }

    private String ObtenerPRRRQCXMLByTransaccion(Transaccion objTrn) throws Exception
    {
     String strWKF = "";
        strWKF = "<PRRRQCXML>";
        strWKF += "<MSGVERSION>";
        strWKF += "<VALOR>" + objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER) + "</VALOR>";
        strWKF += "</MSGVERSION>";
        for (TransaccionCampo var : objTrn.ObtenerListaCampos()) {
             strWKF += "<" + var.getNombreCampo().toUpperCase() + ">";
            strWKF += "<VALOR>" + var.getValorCampo().toString().toUpperCase() + "</VALOR>";
            strWKF += "</" + var.getNombreCampo().toUpperCase() + ">";
        }
        strWKF += "</PRRRQCXML>";
        return  strWKF;
    }
       
  
}
