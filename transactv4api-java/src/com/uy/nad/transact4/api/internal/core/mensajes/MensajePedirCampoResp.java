/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import java.util.Map;

/**
 *
 * @author Yunior
 */
public class MensajePedirCampoResp extends absMensajeRecibe {
    private final absMensajeRecibe.TipoMensajeRecibidoDesdePos tipoMensaje = absMensajeRecibe.TipoMensajeRecibidoDesdePos.PedirCampoEnAPI;
    private String tipoDatos;;
    private String descripcion;
    private String mensaje;
    private String valorPorDefecto;
    private String largoMinimo;
    private String largoMaximo;
    private String valoresDesde;
    private String valoresHasta;
    private String mascara;;
    private String noElegible;
    private String requerido;
    private String soloLectura;
    private String multiSelecOpcion;
    private String segundosTimeout;
    private String key;

    public MensajePedirCampoResp(String strRespuesta) throws Exception{
        CargarDatos(strRespuesta);
    }

    @Override
    public absMensajeRecibe.TipoMensajeRecibidoDesdePos getTipoMensajeRecibido() {
        return tipoMensaje;
    }

    private void CargarDatos(String strRespuesta) throws Exception{
       if (absMensajeRecibe.GetTipoMensajeFromStr(strRespuesta) != tipoMensaje) {
            throw new Exception("Error al crear mensaje del tipo " + tipoMensaje.toString() + " con respuesta: " + strRespuesta);
        }
        Map<String, String> ListaCampos = super.ObtenerCamposMensaje(strRespuesta);
        key = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_KEY);
        tipoDatos =  super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_TPODATOS);
        descripcion = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_DESCRIPCION);
        mensaje = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_MENSAJE);
        valorPorDefecto = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_VALORPORDEFECTO);
        largoMinimo = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_LARGOMIN);
        largoMaximo = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_LARGOMAX);
        valoresDesde = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_VALORDESDE);
        valoresHasta = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_VALORHASTA);
        mascara = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_MASCARA);
        noElegible = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_NOLEGIBLE);
        requerido = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_REQUERIDO);
        soloLectura = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_SOLOLECTURA);
        multiSelecOpcion = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_MULTISELECTOPCION);
        segundosTimeout = super.ObtenerValorCampo(ListaCampos, DiccionarioCampos.SOLICITUD_NOMBRE_CAMPO_SEGUNDOSTIMEOUT);
    }

    public TipoMensajeRecibidoDesdePos getTipoMensaje() {
        return tipoMensaje;
    }
    public String getKey() {
        return key;
    }
    public String getDescripcion() {
        return descripcion;
    }
    
    public String getTipoDatos() {
        return tipoDatos;
    }

    public String getMensaje() {
        return mensaje;
    }

    public String getValorPorDefecto() {
        return valorPorDefecto;
    }

    public String getLargoMinimo() {
        return largoMinimo;
    }

    public String getLargoMaximo() {
        return largoMaximo;
    }

    public String getValoresDesde() {
        return valoresDesde;
    }

    public String getValoresHasta() {
        return valoresHasta;
    }

    public String getMascara() {
        return mascara;
    }

    public String getNoElegible() {
        return noElegible;
    }

    public String getRequerido() {
        return requerido;
    }

    public String getSoloLectura() {
        return soloLectura;
    }

    public String getMultiSelecOpcion() {
        return multiSelecOpcion;
    }

    public String getSegundosTimeout() {
        return segundosTimeout;
    }
    
    
}
