/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

import com.uy.nad.transact4.api.IAPIListener;

class Concentrador
{
    public Concentrador(Boolean ModoEmulacion, IAPIListener listener, String URLConcentrador) {}
    public void ProcesarCierreCentralizado(Transaccion objTrn) throws Exception {}
    public void ConsultarEstadoCierreCentralizado(Transaccion objTrn) {}
    private void ConsultarUltimoCierre(Transaccion objTrn) throws Exception {}
}


//////////
//////////import com.uy.nad.transact4.api.IAPIListener;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosCierre;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosProducto;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMoneda;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlan;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlanDecretos;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ArrayOfstring;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400Cierre;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400Comportamiento;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400Configuracion;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400RespuestaConsultarCierre;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400RespuestaConsultarCierreIDatosCierre;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400RespuestaConsultarCierreIDatosProducto;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMoneda;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlan;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlanDecretos;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.ITarjetasCierreJava400RespuestaPostearCierre;
//////////import com.uy.nad.transact4.api.internal.cliente.concentrador.TarjetasCierreJava400;
//////////import java.net.URL;
//////////import java.util.List;
//////////
///////////**
////////// *
////////// * @author Yunior Bauta nad.uy
////////// */
//////////public class Concentrador {
//////////
//////////    private final String NOM_SERVICIO_CIERRE = "TarjetasCierreJava_400.svc";
//////////    private String URLConcentrador = "";
//////////    private Boolean ModoEmulacion = false;
//////////    private IAPIListener listener;
//////////
//////////    public Concentrador(Boolean ModoEmulacion, IAPIListener listener, String URLConcentrador) {
//////////        this.ModoEmulacion = ModoEmulacion;
//////////        this.listener = listener;
//////////        this.URLConcentrador = URLConcentrador;
//////////    }
//////////
//////////    public void ProcesarCierreCentralizado(Transaccion objTrn) throws Exception {
//////////        ITarjetasCierreJava400 clienteWSConcentrador = ObtenerClienteConcentrador();
//////////        ITarjetasCierreJava400Cierre objCierre = new ITarjetasCierreJava400Cierre();
//////////        CargarDatosSolicitudCierre(objTrn, objCierre);
//////////        NotificarAvance("ENVIANDO CIERRE CENTRALIZADO");
//////////        ITarjetasCierreJava400RespuestaPostearCierre objRespuestaCierre = clienteWSConcentrador.postearCierre(objCierre);
//////////        CargarDatosRepuestaCierre(objTrn, objRespuestaCierre);
//////////    }
//////////
//////////    public void ConsultarEstadoCierreCentralizado(Transaccion objTrn) {
//////////        System.out.println("URL CONCENTRADOR: " + this.URLConcentrador);
//////////        try {
//////////            String tokenCierre = objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TOKENCIERRE, "").toString().trim().toUpperCase();
//////////            Boolean EsConsultaUltimo = tokenCierre.equals("ULTIMO");
//////////            if (EsConsultaUltimo) {
//////////                ConsultarUltimoCierre(objTrn);
//////////            } else {
//////////                ConsultarCierreEnCurso(objTrn, 0);
//////////            }
//////////        } catch (Exception ex) {
//////////            objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CODRESP, "1", false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MSGRESP, ex.getMessage().toUpperCase(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_FINALIZADO, "TRUE", false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_BOOLEAN);
//////////        }
//////////    }
//////////
//////////    private void ConsultarUltimoCierre(Transaccion objTrn) throws Exception {
//////////        ITarjetasCierreJava400 clienteWSConcentrador = ObtenerClienteConcentrador();
//////////        ITarjetasCierreJava400Cierre objCierre = new ITarjetasCierreJava400Cierre();
//////////        CargarDatosParaConsultaCierre(objTrn, objCierre);
//////////        ITarjetasCierreJava400RespuestaPostearCierre objRespPostear = clienteWSConcentrador.postearConsultaUltimoCierre(objCierre);
//////////        if (objRespPostear.getRespCodigoRespuesta() != 0) {
//////////            throw new Exception(objRespPostear.getRespMensajeError());
//////////        }
//////////        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TOKENCIERRE, objRespPostear.getTokenNro(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////        Boolean Finalizado = false;
//////////        int SegundosConsultar = objRespPostear.getTokenSegundosConsultar();
//////////        while (!Finalizado) {
//////////            if (SegundosConsultar != 0) {
//////////                Thread.sleep(SegundosConsultar * 1000);
//////////            }
//////////            ConsultarCierreEnCurso(objTrn, SegundosConsultar);
//////////            Finalizado = (Boolean) objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_FINALIZADO, false);
//////////        }
//////////
//////////    }
//////////
//////////    private void ConsultarCierreEnCurso(Transaccion objTrn, Integer SegundosConsultar) throws Exception {
//////////        ITarjetasCierreJava400 clienteWSConcentrador = ObtenerClienteConcentrador();
//////////        ITarjetasCierreJava400RespuestaConsultarCierre objRespConsulta = new ITarjetasCierreJava400RespuestaConsultarCierre();
//////////        objRespConsulta = clienteWSConcentrador.consultarCierre(objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TOKENCIERRE));
//////////        SegundosConsultar = objRespConsulta.getRespTokenSegundosReConsultar();
//////////        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CODRESP, objRespConsulta.getRespCodigoRespuesta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_NUMERICO);
//////////        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MSGRESP, objRespConsulta.getRespMensajeError().trim().toUpperCase(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_FINALIZADO, objRespConsulta.isRespCierreFinalizado(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_BOOLEAN);
//////////        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ESTADO, objRespConsulta.getEstado().trim(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////        System.out.println("FINALIZADO= " + objRespConsulta.isRespCierreFinalizado());
//////////        if (objRespConsulta.isRespCierreFinalizado()) {
//////////            objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_DISVOUCHER, ObtenerLineasVoucherByString(objRespConsulta.getVoucher()), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            CargarLotesByDatosCierre(objTrn, objRespConsulta.getDatosCierre());
//////////        }
//////////    }
//////////
//////////    private ITarjetasCierreJava400 ObtenerClienteConcentrador() throws Exception {
//////////
//////////        //http://wwwi.transact.com.uy/Concentrador/TarjetasCierreJava_400.svc?wsdl
//////////        if (!URLConcentrador.endsWith("/")) {
//////////            URLConcentrador = URLConcentrador.concat("/");
//////////        }
//////////        String url = URLConcentrador + NOM_SERVICIO_CIERRE + "?wsdl";
//////////        System.out.println("OBTENER CLIENTE CONCENTRADOR WSDL: " + url);
//////////        URL wsdlLocation = new URL(url);
//////////        TarjetasCierreJava400 servicio = new TarjetasCierreJava400(wsdlLocation);
//////////        ITarjetasCierreJava400 binding = servicio.getBasicHttpBindingITarjetasCierreJava4001();
//////////        return binding;
//////////    }
//////////
//////////    private void CargarDatosSolicitudCierre(Transaccion objTrn, ITarjetasCierreJava400Cierre objCierre) throws Exception {
//////////        ITarjetasCierreJava400Configuracion objCierreConfiguracion = new ITarjetasCierreJava400Configuracion();
//////////        objCierreConfiguracion.setModoEmulacion(ModoEmulacion);
//////////        ITarjetasCierreJava400Comportamiento objCierreComportamiento = new ITarjetasCierreJava400Comportamiento();
//////////        objCierre.setConfiguracion(objCierreConfiguracion);
//////////        objCierre.setComportamiento(objCierreComportamiento);
//////////        objCierre.setEmpHASH(objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPHASH));
//////////        objCierre.setEmpCod(objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD));
//////////        objCierre.setTermCod(objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD));
//////////        objCierre.setMultiEmp(Integer.parseInt(objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_MULTIEMP, 0).toString()));
//////////        objCierre.setProcesadorId(Integer.parseInt(objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_PROCESADORID, 0).toString()));
//////////        objCierre.setCierreCentralizado(true);
//////////    }
//////////
//////////    private void CargarDatosRepuestaCierre(Transaccion objTrn, ITarjetasCierreJava400RespuestaPostearCierre objRespuestaCierre) {
//////////        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CODRESP, objRespuestaCierre.getRespCodigoRespuesta(), false,
//////////                DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MSGRESP, objRespuestaCierre.getRespMensajeError().trim().toUpperCase(), false,
//////////                DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////        objTrn.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TOKENCIERRE, objRespuestaCierre.getTokenNro().trim(), false,
//////////                DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////    }
//////////
//////////    private void NotificarAvance(String mensaje) {
//////////        if (listener != null) {
//////////            listener.evtNotificadAvance(mensaje);
//////////        }
//////////    }
//////////
//////////    private void CargarDatosParaConsultaCierre(Transaccion objTrn, ITarjetasCierreJava400Cierre objCierre) throws Exception {
//////////        ITarjetasCierreJava400Configuracion objCierreConfiguracion = new ITarjetasCierreJava400Configuracion();
//////////        objCierreConfiguracion.setModoEmulacion(ModoEmulacion);
//////////        objCierre.setConfiguracion(objCierreConfiguracion);
//////////        objCierre.setEmpHASH(objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPHASH));
//////////        objCierre.setEmpCod(objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD));
//////////        objCierre.setTermCod(objTrn.GetCampo_String(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD));
//////////        objCierre.setMultiEmp(Integer.parseInt(objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_MULTIEMP, 0).toString()));
//////////        objCierre.setCierreCentralizado(true);
//////////    }
//////////
//////////    private String ObtenerLineasVoucherByString(ArrayOfstring voucher) {
//////////        String vaucherTxt = "";
//////////        if (voucher != null) {
//////////            for (String linea : voucher.getString()) {
//////////                vaucherTxt = vaucherTxt.concat(linea + "\n");
//////////            }
//////////        }
//////////        return vaucherTxt;
//////////    }
//////////
//////////    private void CargarLotesByDatosCierre(Transaccion objTrn, ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosCierre datosCierre) throws Exception {
//////////        List<ITarjetasCierreJava400RespuestaConsultarCierreIDatosCierre> listaCierres = datosCierre.getITarjetasCierreJava400RespuestaConsultarCierreIDatosCierre();
//////////
//////////        for (ITarjetasCierreJava400RespuestaConsultarCierreIDatosCierre objCierre : listaCierres) {
//////////            Transaccion objLote = new Transaccion();
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PROCESADORID, objCierre.getProcesadorId(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_LOTE, objCierre.getLote(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_NROAUTORIZACION, objCierre.getNroAutorizacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_APROBADO, objCierre.isAprobado(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_BOOLEAN);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CODRESP, objCierre.getCodRespuesta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MSGRESP, objCierre.getMsgRespuesta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_FCHHORA, objCierre.getDatosCierreExtendida().getCierreFechaHora(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_EMPRUT, objCierre.getDatosCierreExtendida().getEmpresaRUT(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_EMPNOM, objCierre.getDatosCierreExtendida().getEmpresaNombre(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUCNOM, objCierre.getDatosCierreExtendida().getSucursalNombre(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUCDIR, objCierre.getDatosCierreExtendida().getSucursalDireccion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MID, objCierre.getDatosCierreExtendida().getMerchantID(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TID, objCierre.getDatosCierreExtendida().getTerminalID(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTVENTA, objCierre.getDatosCierreExtendida().getCantVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOVENTA, objCierre.getDatosCierreExtendida().getMontoVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTREVERSOVENTA, objCierre.getDatosCierreExtendida().getCantReversoVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOREVERSOVENTA, objCierre.getDatosCierreExtendida().getMontoReversoVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTANULACION, objCierre.getDatosCierreExtendida().getCantAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOANULACION, objCierre.getDatosCierreExtendida().getMontoAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTREVERSOANULACION, objCierre.getDatosCierreExtendida().getCantReversoAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOREVERSOANULACION, objCierre.getDatosCierreExtendida().getMontoReversoAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTDEVOLUCION, objCierre.getDatosCierreExtendida().getCantDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTODEVOLUCION, objCierre.getDatosCierreExtendida().getMontoDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_CANTREVERSODEVOLUCION, objCierre.getDatosCierreExtendida().getCantReversoDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objLote.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONTOREVERSODEVOLUCION, objCierre.getDatosCierreExtendida().getMontoReversoDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            CargarProductosByDatosCierre(objTrn, objCierre.getDatosCierreExtendida().getProductos());
//////////            objTrn.AgregarColeccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_DATOSCIERRE, objLote);
//////////        }
//////////    }
//////////
//////////    private void CargarProductosByDatosCierre(Transaccion objTrn, ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosProducto productos) throws Exception {
//////////        List<ITarjetasCierreJava400RespuestaConsultarCierreIDatosProducto> listaProductos = productos.getITarjetasCierreJava400RespuestaConsultarCierreIDatosProducto();
//////////        for (ITarjetasCierreJava400RespuestaConsultarCierreIDatosProducto objProducto : listaProductos) {
//////////            Transaccion objProd = new Transaccion();
//////////            objProd.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ADQID, objProducto.getTarjetaId(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objProd.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ADQNOM, objProducto.getTarjetaNombre(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objProd.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ADQPRODID, objProducto.getProductoId(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objProd.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_ADQPRODNOM, objProducto.getProductoNombre(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objProd.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TARJETATIPO, objProducto.getTarjetaTipo(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objProd.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TARJETAPRESTACIONES, objProducto.isTarjetaPrestaciones(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objProd.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_TARJETAALIMENTACION, objProducto.isTarjetaAlimentacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            CargarMonedasByDatosCierre(objTrn, objProducto.getMonedas());
//////////            objTrn.AgregarColeccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PRODUCTOSCIERRE, objProd);
//////////        }
//////////    }
//////////
//////////    private void CargarMonedasByDatosCierre(Transaccion objTrn, ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMoneda monedas) throws Exception {
//////////        List<ITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMoneda> listaMonedas = monedas.getITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMoneda();
//////////        for (ITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMoneda objMoneda : listaMonedas) {
//////////            Transaccion objMon = new Transaccion();
//////////            objMon.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONEDA, objMoneda.getMonedaISO(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objMon.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALCANTMONEDA, objMoneda.getSubTotalCantMoneda(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objMon.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALMONTOMONEDA, objMoneda.getSubTotalMontoMoneda(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            CargarPlanesByDatosCierre(objTrn, objMoneda.getPlanes());
//////////            objTrn.AgregarColeccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_MONEDASCIERRE, objMon);
//////////        }
//////////    }
//////////
//////////    private void CargarPlanesByDatosCierre(Transaccion objTrn, ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlan planes) throws Exception {
//////////        List<ITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlan> listaPlanes = planes.getITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlan();
//////////        for (ITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlan plan : listaPlanes) {
//////////            Transaccion objPlan = new Transaccion();
//////////            objPlan.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANID, plan.getPlanId(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objPlan.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANNOMBRE, plan.getPlanNombre(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objPlan.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANADQPLAN, plan.getPlanNroPlan(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objPlan.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANADQTIPOPLAN, plan.getPlanNroTipoPlan(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objPlan.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMID, plan.getMerchantID(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objPlan.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANID, plan.getSubTotalCantPlan(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objPlan.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALMONTOPLAN, plan.getSubTotalMontoPlan(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objPlan.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALDECRETOLEYPLAN, plan.getSubTotalDecretoLeyPlan(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            CargarColeccionByWSDecretos(objPlan, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_NACIONALESCIERRE, plan.getNacionales());
//////////            CargarColeccionByWSDecretos(objPlan, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_EXTRANJERASCIERRE, plan.getExtranjeras());
//////////            objTrn.AgregarColeccion(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANESCIERRE, objPlan);
//////////        }
//////////    }
//////////
//////////    private void CargarColeccionByWSDecretos(Transaccion objPlan, String padre, ArrayOfITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlanDecretos ProdMonPlanDecreto) throws Exception {
//////////        List<ITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlanDecretos> listaDecretos = ProdMonPlanDecreto.getITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlanDecretos();
//////////        for (ITarjetasCierreJava400RespuestaConsultarCierreIDatosProductoMonedaPlanDecretos decreto : listaDecretos) {
//////////            Transaccion objDecreto = new Transaccion();
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_DECRETOLEYAPLICADO, decreto.isDecretoLeyAplicado(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_DECRETOLEYNRO, decreto.getDecretoLeyNro(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALCANTDECRETO, decreto.getSubTotalCantDecreto(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_SUBTOTALMONTODECRETO, decreto.getSubTotalMontoDecreto(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTVENTA, decreto.getVenta().getCantVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOVENTA, decreto.getVenta().getMontoVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAVENTA, decreto.getVenta().getMontoPropinaVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKVENTA, decreto.getVenta().getMontoCashbackVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYVENTA, decreto.getVenta().getMontoDecretoLeyVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTREVERSOVENTA, decreto.getVenta().getCantReversoVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOREVERSOVENTA, decreto.getVenta().getMontoReversoVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAREVERSOVENTA, decreto.getVenta().getMontoPropinaReversoVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKREVERSOVENTA, decreto.getVenta().getMontoCashbackReversoVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYREVERSOVENTA, decreto.getVenta().getMontoDecretoLeyReversoVenta(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTANULACION, decreto.getAnulacion().getCantAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOANULACION, decreto.getAnulacion().getMontoAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAANULACION, decreto.getAnulacion().getMontoPropinaAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKANULACION, decreto.getAnulacion().getMontoCashbackAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYANULACION, decreto.getAnulacion().getMontoDecretoLeyAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTREVERSOANULACION, decreto.getAnulacion().getCantReversoAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOREVERSOANULACION, decreto.getAnulacion().getMontoReversoAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAREVERSOANULACION, decreto.getAnulacion().getMontoPropinaReversoAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKREVERSOANULACION, decreto.getAnulacion().getMontoCashbackReversoAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYREVERSOANULACION, decreto.getAnulacion().getMontoDecretoLeyReversoAnulacion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTDEVOLUCION, decreto.getDevolucion().getCantDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODEVOLUCION, decreto.getDevolucion().getMontoDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINADEVOLUCION, decreto.getDevolucion().getMontoPropinaDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKDEVOLUCION, decreto.getDevolucion().getMontoCashbackDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYDEVOLUCION, decreto.getDevolucion().getMontoDecretoLeyDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANCANTREVERSODEVOLUCION, decreto.getDevolucion().getCantReversoDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOREVERSODEVOLUCION, decreto.getDevolucion().getMontoReversoDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOPROPINAREVERSODEVOLUCION, decreto.getDevolucion().getMontoPropinaReversoDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTOCASHBACKREVERSODEVOLUCION, decreto.getDevolucion().getMontoCashbackReversoDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objDecreto.SetearValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_RESPUESTA_PLANMONTODECRETOLEYREVERSODEVOLUCION, decreto.getDevolucion().getMontoDecretoLeyReversoDevolucion(), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
//////////            objPlan.AgregarColeccion(padre, objDecreto);
//////////        }
//////////
//////////    }

//////////}
