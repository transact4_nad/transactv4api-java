package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import com.uy.nad.transact4.api.internal.core.TransaccionCampo;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

public abstract class absMensajeRecibe {

    public abstract TipoMensajeRecibidoDesdePos getTipoMensajeRecibido();

    public enum TipoMensajeRecibidoDesdePos {
        RespuestaPing,
        RespuestaRetornarTarjetaTrn,
        RespuestaProcesarTrn,
        RespuestaConfirmarTrn,
        RespuestaConsultaConfirmacionTrn,
        RespuestaReversarTrn,
        RespuestaProcesarCierre,
        RespuestaConsultaEstadoCierre,
        PedirCampoEnAPI,
        ReportarAvanceEnAPI,
        Imprimir,
        RespuestaReimprimir,
        RespuestaLeerTarjeta,
        RespuestaError,
        RespuestaActualizarDispositivo,
        RespuestaImprimir
    }

    public static absMensajeRecibe ConstruirMensajeRecibido(String strMensajeRecibido) throws Exception {
        TipoMensajeRecibidoDesdePos tipo = absMensajeRecibe.GetTipoMensajeFromStr(strMensajeRecibido);
        switch (tipo) {
            case RespuestaPing:
                return new MensajePingFPOSResp(strMensajeRecibido);
            case ReportarAvanceEnAPI:
                return new MensajeReportarAvanceResp(strMensajeRecibido);
             case PedirCampoEnAPI:
                return new MensajePedirCampoResp(strMensajeRecibido);
            case RespuestaError:
                return new MensajeErrorResp(strMensajeRecibido);
            case RespuestaRetornarTarjetaTrn:
                return new MensajeTrnRetornarTarjetaFPOSResp(strMensajeRecibido);
            case RespuestaProcesarTrn:
                return new MensajeTrnProcesarFPOSResp(strMensajeRecibido);
            case RespuestaConfirmarTrn:
                return new MensajeTrnConfirmarFPOSResp(strMensajeRecibido);
            case RespuestaConsultaConfirmacionTrn:
                return new MensajeTrnConsultarConfirmacionResp(strMensajeRecibido);
            case RespuestaProcesarCierre:
                return new MensajeCierreProcesarFPOSResp(strMensajeRecibido);
            case RespuestaConsultaEstadoCierre:
                return new MensajeCierreConsultarEstadoResp(strMensajeRecibido);
            case RespuestaActualizarDispositivo:
                return new MensajeReiniciarFPOSResp(strMensajeRecibido);
            case RespuestaReversarTrn:
                return new MensajeTrnReversarFPOSResp(strMensajeRecibido);
            case RespuestaImprimir:
                return new MensajeImpresionFPOSResp(strMensajeRecibido);
            case RespuestaReimprimir:
                return new MensajeReimprimirFPOSResp(strMensajeRecibido);
            case RespuestaLeerTarjeta:
                return new MensajeLeerTarjetaFPOSResp(strMensajeRecibido);

            default:
                throw new Exception("Comando no valido");
        }

    }

    protected static TipoMensajeRecibidoDesdePos GetTipoMensajeFromStr(String StrMensaje) throws Exception {
        int LargoPrefijo = Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.PREFIJO_MENSAJE + DiccionarioComandos.FIN_ELEMENTO);
        if (StrMensaje.length() < LargoPrefijo + DiccionarioComandos.LARGO_NOMBRE_COMANDO) {
            throw new Exception("Mensaje: " + StrMensaje + " ,respondido por el POS, no reconocido!");
        }
        String Comando = StrMensaje.substring(LargoPrefijo, LargoPrefijo + DiccionarioComandos.LARGO_NOMBRE_COMANDO);
        if (Comando.equals(DiccionarioComandos.COMANDO_RESPUESTAPING)) {
            return TipoMensajeRecibidoDesdePos.RespuestaPing;
        } else if (Comando.equals(DiccionarioComandos.COMANDO_REPORTARAVANCEENAPI)) {
            return TipoMensajeRecibidoDesdePos.ReportarAvanceEnAPI;
        }else if (Comando.equals(DiccionarioComandos.COMANDO_PEDIRCAMPOENAPI)) {
            return TipoMensajeRecibidoDesdePos.PedirCampoEnAPI;
        }else if (Comando.equals(DiccionarioComandos.COMANDO_RESPUESTAERROR)) {
            return TipoMensajeRecibidoDesdePos.RespuestaError;
        }else if (Comando.equals(DiccionarioComandos.COMANDO_RESPUESTARETORNARTARJETATRN)) {
            return TipoMensajeRecibidoDesdePos.RespuestaRetornarTarjetaTrn;
        }else if (Comando.equals(DiccionarioComandos.COMANDO_RESPUESTATRNPROCESAR)) {
            return TipoMensajeRecibidoDesdePos.RespuestaProcesarTrn;
        } else if (Comando.equals(DiccionarioComandos.COMANDO_RESPUESTATRNCONFIRMAR)) {
            return TipoMensajeRecibidoDesdePos.RespuestaConfirmarTrn;
        } else if (Comando.equals(DiccionarioComandos.COMANDO_RESPUESTATRNCONSULTARCONFIRMACION)) {
            return TipoMensajeRecibidoDesdePos.RespuestaConsultaConfirmacionTrn;
        } else if (Comando.equals(DiccionarioComandos.COMANDO_RESPUESTACIEPROCESAR)) {
            return TipoMensajeRecibidoDesdePos.RespuestaProcesarCierre;
        } else if (Comando.equals(DiccionarioComandos.COMANDO_RESPUESTACIECONSULTARESTADO)) {
            return TipoMensajeRecibidoDesdePos.RespuestaConsultaEstadoCierre;
        }else if(Comando.equals(DiccionarioComandos.COMANDO_RESPUESTAACTUALIZAR_DISPOSITIVO)){
            return TipoMensajeRecibidoDesdePos.RespuestaActualizarDispositivo;
        }else if(Comando.equals(DiccionarioComandos.COMANDO_RESPUESTATRNREVERSAR)){
            return TipoMensajeRecibidoDesdePos.RespuestaReversarTrn;
        }else if(Comando.equals(DiccionarioComandos.COMANDO_RESPUESTAREIMPRIMIR)){
            return TipoMensajeRecibidoDesdePos.RespuestaReimprimir;
        }else if(Comando.equals(DiccionarioComandos.COMANDO_RESPUESTALEERTARJETA)){
            return TipoMensajeRecibidoDesdePos.RespuestaLeerTarjeta;
        }
        else {
            throw new Exception("Comando no valido");
        }

    }

    protected Map<String, String> ObtenerCamposMensaje(String Msg) throws Exception {
        boolean MsgConSubComando = false;
        Map<String, String> Result = new HashMap<String,String>();
        Msg = ControlarCRCYRecortarMensajePOS(Msg);
        Msg = Right(Msg, Len(Msg) - DiccionarioComandos.LARGO_NOMBRE_COMANDO);

        while (Msg.indexOf(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.SEPARADOR_CAMPO + DiccionarioComandos.FIN_ELEMENTO) != -1) {
            String strCampo = "";
            strCampo = Left(Msg, Msg.indexOf(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.SEPARADOR_CAMPO + DiccionarioComandos.FIN_ELEMENTO));
            int InicioNom = strCampo.indexOf(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.NOMBRE_CAMPO + DiccionarioComandos.FIN_ELEMENTO) + Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.NOMBRE_CAMPO + DiccionarioComandos.FIN_ELEMENTO);
            int InicioVal = strCampo.indexOf(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.VALOR_CAMPO + DiccionarioComandos.FIN_ELEMENTO) + Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.VALOR_CAMPO + DiccionarioComandos.FIN_ELEMENTO);
            String StrNom = strCampo.substring(InicioNom, InicioVal - Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.VALOR_CAMPO + DiccionarioComandos.FIN_ELEMENTO));
            String StrVal = strCampo.substring(InicioVal, Len(strCampo));
            Result.put(StrNom, StrVal);
            Msg = Right(Msg, Len(Msg) - Len(strCampo) - Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.SEPARADOR_CAMPO + DiccionarioComandos.FIN_ELEMENTO));
        }
        return Result;
    }

    protected String ObtenerValorCampo(Map<String, String> ListaCampos, String Campo) {
        if (ListaCampos.containsKey(Campo)) {
            return ListaCampos.get(Campo);
        } else {
            return "";
        }
    }

    protected String ControlarCRCYRecortarMensajePOS(String Msg) throws Exception {

        int PosInicial = Msg.lastIndexOf(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.CAMPO_LRC + DiccionarioComandos.FIN_ELEMENTO) + Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.CAMPO_LRC + DiccionarioComandos.FIN_ELEMENTO);
        String largoAsString = Msg.substring(PosInicial, PosInicial + DiccionarioComandos.LARGO_LRC);
        int LargoEsperado = Integer.parseInt(largoAsString);

        String MsgSinSufijo = Left(Msg, (Len(Msg) - (Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.SUFIJO_MENSAJE + DiccionarioComandos.FIN_ELEMENTO + DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.CAMPO_LRC + DiccionarioComandos.FIN_ELEMENTO) + DiccionarioComandos.LARGO_LRC)));
        PosInicial = Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.PREFIJO_MENSAJE + DiccionarioComandos.FIN_ELEMENTO);
        int PosFinal = PosInicial + Len(MsgSinSufijo) - Len(DiccionarioComandos.INICIO_ELEMENTO + DiccionarioComandos.PREFIJO_MENSAJE + DiccionarioComandos.FIN_ELEMENTO);
        String MsgSinPrefijoNiSufijo = MsgSinSufijo.substring(PosInicial, PosFinal);
        //EL substring de java difiere del de .net
        if (Len(MsgSinPrefijoNiSufijo) != LargoEsperado) {
            throw new Exception("Error de comprobacion de LRC de respuesta. Largo en LRC: " + LargoEsperado + " , Largo real: " + Len(MsgSinPrefijoNiSufijo));

        }
        return MsgSinPrefijoNiSufijo;
    }

    protected static int Len(String cad) {
        return cad.length();
    }

    protected static String Left(String cad, int cant) {
        return cad.substring(0, cant);
    }

    protected static String Right(String cad, int cant) {
        if (cad.length() < cant) {
            return cad;
        }
        return cad.substring(cad.length() - cant);
    }

    public void CargarTransaccionByXML(Transaccion objTrn, String XML, String RootName) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        builder = factory.newDocumentBuilder();
        org.w3c.dom.Document doc = builder.parse(new InputSource(new StringReader(XML)));
        doc.getDocumentElement().normalize();
        NodeList listaInicial = doc.getElementsByTagName(RootName);
        if (listaInicial.getLength() > 0) {
            CargarValoresDeXMLRespuesta(objTrn, RootName, listaInicial);
        } else {
            throw new Exception("XML no contiene item " + RootName);
        }
        ImprimirCamposObjTransaccion(objTrn);
    }

    private void CargarValoresDeXMLRespuesta(Transaccion objTrn, String Padre, NodeList listaCampos) {

        System.out.println("-------- " + Padre + " ----------- ");
        for (int i = 0; i < listaCampos.getLength(); i++) {
            Node nodoCampo = listaCampos.item(i);
            System.out.println("-------- " + nodoCampo.getNodeName() + " ----------- ");
            if (nodoCampo.hasChildNodes()) {

                if (nodoCampo.getNodeName().toUpperCase().equals("COLECCION") && nodoCampo.hasChildNodes()) {
                    Transaccion objTrnColeccion = new Transaccion();
                    CargarValoresDeXMLRespuesta(objTrnColeccion, nodoCampo.getNodeName().toUpperCase(), nodoCampo.getChildNodes());
                    objTrn.AgregarColeccion(Padre, objTrnColeccion);
                } else {
                    if (ObtenerValorXML(nodoCampo).equals("")) {
                        CargarValoresDeXMLRespuesta(objTrn, nodoCampo.getNodeName(), nodoCampo.getChildNodes());

                    } else {
                        objTrn.SetearValorCampo(Padre, ObtenerValorXML(nodoCampo), false, DiccionarioCampos.TRANSACT_TIPO_DATOS_CAMPO_ALFANUMERICO);
                    }
                }
            }
        }
    }

    private String ObtenerValorXML(Node xmlNode) {
        if (xmlNode.getNodeName().toUpperCase().equals("VALOR")) {
            return xmlNode.getTextContent();
        } else {
            return "";
        }
    }

    void ImprimirCamposObjTransaccion(Transaccion objTrn) {
        List<TransaccionCampo> campos = objTrn.ObtenerListaCampos();
        System.out.println("############################################################");
        for (TransaccionCampo campo : campos) {
            if (campo.getTipoDatos().equals("COL")) {
                System.out.println(campo.getNombreCampo() + "= TIPOCOLECCION");
            } else {
                System.out.println(campo.getNombreCampo() + "=" + campo.getValorCampo().toString());
            }

        }
        System.out.println("############################################################");

        System.out.println("");
    }
}
