/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.cliente;

import com.uy.nad.transact4.api.internal.core.mensajes.absMensajeEnvio;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class MensajeWrapperEnvio extends absMensajeEnvio {

    private final String TRANSACT_COMANDO_WRAPPER_ENV = "WRPRQC";

    public static final String WRAPPER_TIPO_ENVIO_AL_POS = "ENVPOS";
    private final String TRANSACT_CAMPO_TIPO = "TIPO";
    private final String TRANSACT_CAMPO_EMPCOD = "EMPCOD";
    private final String TRANSACT_CAMPO_TERMCOD = "TERMCOD";
    private final String TRANSACT_CAMPO_MSG = "MSG";
    private final String TRANSACT_CAMPO_TIMEOUT = "TIMEOUT";

    public static final String WRAPPER_TIPO_IMPRIMIR = "IMPRIM";
    public static final String TRANSACT_CAMPO_TIPO_IMPRESORA = "TIPOIMPRESORA";
    public static final String TRANSACT_CAMPO_NOMBRE_IMPRESORA = "NOMBREIMPRESORA";
    public static final String TRANSACT_CAMPO_COPIAS_IMPRESORA = "COPIAS";
    public static final String TRANSACT_CAMPO_VOUCHER_IMPRESORA = "VOUCHER";

    private final String tipo;
    private String empCod;
    private String termCod;
    private String Msg;
    private Integer timeoutMS;

    private Integer tipoImpresora;
    private String nombreImpresora;
    private Integer copias;
    private String voucher;

    public MensajeWrapperEnvio(String tipo, String empCod, String termCod, String Msg, Integer timeoutMS) {
        this.tipo = tipo;
        this.empCod = empCod;
        this.termCod = termCod;
        this.Msg = Msg;
        this.timeoutMS = timeoutMS;
    }

    public MensajeWrapperEnvio(String tipo,Integer tipoImpresora, String nombreImpresora, Integer copias, String voucher) {
        this.tipo = tipo;
        this.tipoImpresora = tipoImpresora;
        this.nombreImpresora = nombreImpresora;
        this.copias = copias;
        this.voucher = voucher;
    }

    @Override
    public String ObtenerMensajePOS() throws Exception {
        String strMensaje = TRANSACT_COMANDO_WRAPPER_ENV;
        strMensaje = super.AgregarCampo(strMensaje, TRANSACT_CAMPO_TIPO, tipo);

        if (tipo.equals(WRAPPER_TIPO_ENVIO_AL_POS)) {
            strMensaje = super.AgregarCampo(strMensaje, TRANSACT_CAMPO_EMPCOD, empCod);
            strMensaje = super.AgregarCampo(strMensaje, TRANSACT_CAMPO_TERMCOD, termCod);
            strMensaje = super.AgregarCampo(strMensaje, TRANSACT_CAMPO_MSG, Msg);
            strMensaje = super.AgregarCampo(strMensaje, TRANSACT_CAMPO_TIMEOUT, (timeoutMS / 1000) + 1000);
        } else if (tipo.equals(WRAPPER_TIPO_IMPRIMIR)) {
            strMensaje = super.AgregarCampo(strMensaje, TRANSACT_CAMPO_TIPO_IMPRESORA, tipoImpresora);
            strMensaje = super.AgregarCampo(strMensaje, TRANSACT_CAMPO_NOMBRE_IMPRESORA, nombreImpresora);
            strMensaje = super.AgregarCampo(strMensaje, TRANSACT_CAMPO_COPIAS_IMPRESORA, copias);
            strMensaje = super.AgregarCampo(strMensaje, TRANSACT_CAMPO_VOUCHER_IMPRESORA, voucher);
        }
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }

}
