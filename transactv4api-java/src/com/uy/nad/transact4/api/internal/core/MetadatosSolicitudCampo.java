/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

import com.uy.nad.transact4.api.IMetadatosSolicitudCampo;
import java.util.ArrayList;
import java.util.List;
import com.uy.nad.transact4.api.IMetadatosSolicitudCampoPosiblesValores;

/**
 *
 * @author yunior
 */
public class MetadatosSolicitudCampo implements IMetadatosSolicitudCampo {

    private String wfCampoKey;
    private String wfCampoTpoDatos;
    private String wfCampoDescripcion;
    private String wfCampoMensaje;
    private Integer wfCampoLargoMin;
    private Integer wfCampoLargoMax;
    private Integer wfCampoValorDesde;
    private Integer wfCampoValorHasta;
    private String wfCampoMascara;
    private Boolean wfCampoRequerido;
    private Boolean wfCampoNoLegible;

    private List<IMetadatosSolicitudCampoPosiblesValores> listaPosiblesValores = new ArrayList<IMetadatosSolicitudCampoPosiblesValores>();
    private Object valorPorDefecto;
    private Object valor;
    private Boolean cancelado;
    private Boolean mostrarTouchPad;
    private Integer posicionX;
    private Integer posicionY;
    private Boolean soloLectura;
    private Integer segundosTimeoutIngreso;

    public MetadatosSolicitudCampo() {
    
    }

    @Override
    public String getWfCampoKey() {
        return wfCampoKey;
    }

    @Override
    public String getWfCampoTpoDatos() {
        return wfCampoTpoDatos;
    }

    @Override
    public String getWfCampoDescripcion() {
        return wfCampoDescripcion;
    }

    @Override
    public String getWfCampoMensaje() {
        return wfCampoMensaje;
    }

    @Override
    public Integer getWfCampoLargoMin() {
        return wfCampoLargoMin;
    }

    @Override
    public Integer getWfCampoLargoMax() {
        return wfCampoLargoMax;
    }

    @Override
    public Integer getWfCampoValorDesde() {
        return wfCampoValorDesde;
    }

    @Override
    public Integer getWfCampoValorHasta() {
        return wfCampoValorHasta;
    }

    @Override
    public String getWfCampoMascara() {
        return wfCampoMascara;
    }

    @Override
    public Boolean getWfCampoRequerido() {
        return wfCampoRequerido;
    }

    @Override
    public Boolean getWfCampoNoLegible() {
        return wfCampoNoLegible;
    }

    @Override
    public List<IMetadatosSolicitudCampoPosiblesValores> getListaPosiblesValores() {
        return listaPosiblesValores;
    }

    @Override
    public Object getValorPorDefecto() {
        return valorPorDefecto;
    }

    @Override
    public Boolean getMostrarTouchPad() {
        return mostrarTouchPad;
    }

    @Override
    public Integer getPosicionX() {
        return posicionX;
    }

    @Override
    public Integer getPosicionY() {
        return posicionY;
    }

    @Override
    public Boolean getSoloLectura() {
        return soloLectura;
    }

    @Override
    public Integer getSegundosTimeoutIngreso() {
        return segundosTimeoutIngreso;
    }

    @Override
    public void setValor(Object valor) {
        this.valor = valor;
    }

    @Override
    public void setCancelado(Boolean cancelado) {
        this.cancelado = cancelado;
    }
    //------------------------------

    public Object getValor() {
        return valor;
    }

    public Boolean getCancelado() {
        return cancelado;
    }

    public void setWfCampoKey(String wfCampoKey) {
        this.wfCampoKey = wfCampoKey;
    }

    public void setWfCampoTpoDatos(String wfCampoTpoDatos) {
        this.wfCampoTpoDatos = wfCampoTpoDatos;
    }

    public void setWfCampoDescripcion(String wfCampoDescripcion) {
        this.wfCampoDescripcion = wfCampoDescripcion;
    }

    public void setWfCampoMensaje(String wfCampoMensaje) {
        this.wfCampoMensaje = wfCampoMensaje;
    }

    public void setWfCampoLargoMin(Integer wfCampoLargoMin) {
        this.wfCampoLargoMin = wfCampoLargoMin;
    }

    public void setWfCampoLargoMax(Integer wfCampoLargoMax) {
        this.wfCampoLargoMax = wfCampoLargoMax;
    }

    public void setWfCampoValorDesde(Integer wfCampoValorDesde) {
        this.wfCampoValorDesde = wfCampoValorDesde;
    }

    public void setWfCampoValorHasta(Integer wfCampoValorHasta) {
        this.wfCampoValorHasta = wfCampoValorHasta;
    }

    public void setWfCampoMascara(String wfCampoMascara) {
        this.wfCampoMascara = wfCampoMascara;
    }

    public void setWfCampoRequerido(Boolean wfCampoRequerido) {
        this.wfCampoRequerido = wfCampoRequerido;
    }

    public void setWfCampoNoLegible(Boolean wfCampoNoLegible) {
        this.wfCampoNoLegible = wfCampoNoLegible;
    }

    public void setListaPosiblesValores(List<IMetadatosSolicitudCampoPosiblesValores> listaPosiblesValores) {
        this.listaPosiblesValores = listaPosiblesValores;
    }

    public void setValorPorDefecto(Object valorPorDefecto) {
        this.valorPorDefecto = valorPorDefecto;
    }

    public void setMostrarTouchPad(Boolean mostrarTouchPad) {
        this.mostrarTouchPad = mostrarTouchPad;
    }

    public void setPosicionX(Integer posicionX) {
        this.posicionX = posicionX;
    }

    public void setPosicionY(Integer posicionY) {
        this.posicionY = posicionY;
    }

    public void setSoloLectura(Boolean soloLectura) {
        this.soloLectura = soloLectura;
    }

    public void setSegundosTimeoutIngreso(Integer segundosTimeoutIngreso) {
        this.segundosTimeoutIngreso = segundosTimeoutIngreso;
    }
    
    
}
