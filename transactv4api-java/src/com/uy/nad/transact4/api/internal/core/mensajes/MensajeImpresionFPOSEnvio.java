/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.cliente.DocImpresion;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;

/**
 *
 * @author Yunior
 */
public class MensajeImpresionFPOSEnvio extends absMensajeEnvio {

    String codResp;
    String msgResp;
    public MensajeImpresionFPOSEnvio(DocImpresion docImp) {
        if(docImp.isResultado()){
            this.codResp = "00";
        }else{
            this.codResp = "01";
        }
        this.msgResp = docImp.getMsgResultado();       
    }

    @Override
    public String ObtenerMensajePOS() throws Exception {
     
        String strMensaje = DiccionarioComandos.COMANDO_RESULTADO_IMPRIMIR;
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.RESPUESTA_SOLICITUD_NOMBRE_CAMPO_CODRESP, this.codResp);
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.RESPUESTA_SOLICITUD_NOMBRE_CAMPO_MSGRESP, this.msgResp);
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }

}
