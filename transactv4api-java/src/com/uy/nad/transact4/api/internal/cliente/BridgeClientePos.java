/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.cliente;

import com.uy.nad.transact4.api.internal.core.exceptions.NADGeneralException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public class BridgeClientePos implements ICliente
{
    private final TCPClientePos objClienteTCP;
    private final String empCod;
    private final String termCod;
    private final Integer timeoutMs;
     public BridgeClientePos(String empCod,String termCod,String ip, Integer puerto, Integer timeoutMs) {
        this.empCod = empCod;
        this.termCod = termCod;
        this.timeoutMs = timeoutMs;
        objClienteTCP = new TCPClientePos(ip,puerto,timeoutMs);
        
    }
    @Override
    public void Conectar() throws NADGeneralException {
        objClienteTCP.Conectar();
    }

    @Override
    public void Desconectar() throws NADGeneralException {
        objClienteTCP.Desconectar();
    }

    @Override
    
    public String EnviarMensaje(String mensaje) throws NADGeneralException {
        MensajeWrapperEnvio msgEnvio = new MensajeWrapperEnvio(MensajeWrapperEnvio.WRAPPER_TIPO_ENVIO_AL_POS,empCod,termCod,mensaje,timeoutMs);
        String msgEnvioStr;
        try {
            msgEnvioStr = msgEnvio.ObtenerMensajePOS();
            String respuestaSTR = objClienteTCP.EnviarMensaje(msgEnvioStr);
            MensajeWrapperRecibe msgRespuesta = new MensajeWrapperRecibe(respuestaSTR);
        return msgRespuesta.getMsg();
        } catch (Exception ex) {
            Logger.getLogger(BridgeClientePos.class.getName()).log(Level.SEVERE, null, ex);
            throw new NADGeneralException(ex.getMessage());
        }
    }
   @Override
    public void EnviarMensajeSinRespuesta(String mensaje) throws NADGeneralException
    {
     MensajeWrapperEnvio msgEnvio = new MensajeWrapperEnvio(MensajeWrapperEnvio.WRAPPER_TIPO_ENVIO_AL_POS,empCod,termCod,mensaje,timeoutMs);
        String msgEnvioStr;
        try {
            msgEnvioStr = msgEnvio.ObtenerMensajePOS();
            objClienteTCP.EnviarMensajeSinRespuesta(msgEnvioStr);
        } catch (Exception ex) {
            Logger.getLogger(BridgeClientePos.class.getName()).log(Level.SEVERE, null, ex);
            throw new NADGeneralException(ex.getMessage());
        }
    }
    @Override
    public void SetTimeout(int timeoutMs) {
        objClienteTCP.SetTimeout(timeoutMs);
    }
}
