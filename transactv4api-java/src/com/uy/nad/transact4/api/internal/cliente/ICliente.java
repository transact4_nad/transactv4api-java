/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.cliente;

import com.uy.nad.transact4.api.internal.core.exceptions.NADGeneralException;

/**
 *
 * @author Yunior Bauta nad.uy
 */
 enum EstadoClienteConectado
 {
    IniciandoConexion,
    ConexionEstablecida,
    ConexionDetenidaPorUsuario,
    ConexionCerrada
 }
 interface ClienteListener {
   void evtCambioEstadoConexion(EstadoClienteConectado estado);
   void evtLlegoMensajeDeServidor(String mensaje);
   void evtOcurrioError(String mensajeDeError);    
}
public interface ICliente {
    void Conectar() throws NADGeneralException;
    void Desconectar() throws NADGeneralException;
    String EnviarMensaje(String mensaje)throws NADGeneralException;
    void EnviarMensajeSinRespuesta(String mensaje) throws NADGeneralException;
    void SetTimeout(int timeoutMs);
    
}
