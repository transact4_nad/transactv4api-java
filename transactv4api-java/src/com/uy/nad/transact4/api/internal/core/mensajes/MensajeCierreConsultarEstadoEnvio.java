/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import com.uy.nad.transact4.api.internal.core.TransaccionCampo;

/**
 *
 * @author yunior
 */
public class MensajeCierreConsultarEstadoEnvio extends absMensajeEnvio{
    private Transaccion objTrn;

    public MensajeCierreConsultarEstadoEnvio(Transaccion objTrn) {
        this.objTrn = objTrn;
    }
    
    @Override
    public String ObtenerMensajePOS() throws Exception {
         String strMensaje = "";
        strMensaje += DiccionarioComandos.COMANDO_CONSULTARESTADOCIE;
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP, objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP,0));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TOKENCIERRE, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TOKENCIERRE));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_ACRRQCXML, ObtenerACRRQCXMLByTransaccion(objTrn));
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }

    private String ObtenerACRRQCXMLByTransaccion(Transaccion objTrn) throws Exception{
        String strWKF = "";
        strWKF = "<ACRRQCXML>";
        strWKF += "<MSGVERSION>";
        strWKF += "<VALOR>" + objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER) + "</VALOR>";
        strWKF += "</MSGVERSION>";
        for(TransaccionCampo var : objTrn.ObtenerListaCampos()) {
              strWKF += "<" + var.getNombreCampo().toUpperCase() + ">";
            strWKF += "<VALOR>" + var.getValorCampo().toString().toUpperCase() + "</VALOR>";
            strWKF += "</" + var.getNombreCampo().toUpperCase() + ">";
        }
       strWKF += "</ACRRQCXML>";
       return strWKF;
    }
    
}
