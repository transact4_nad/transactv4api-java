/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
/**
 *
 * @author Yunior
 */
public class MensajeTrnRetornarTarjetaFPOSEnvio extends absMensajeEnvio{
      private Transaccion objTrn;

    public MensajeTrnRetornarTarjetaFPOSEnvio(Transaccion objTrn) {
        this.objTrn = objTrn;
    }
     

    @Override
    public String ObtenerMensajePOS() throws Exception {
        String strMensaje = DiccionarioComandos.COMANDO_RETORNARTARJETATRN;
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP, objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP,0));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_OPERACION, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_OPERACION));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CFG_TIMEOUT_LECTURATARJ_FACTURA, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_TIMEOUT_LECTURATARJ_FACTURA));
        String MontoTap =  objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_MONTO_TAP,"").toString();
        if(!MontoTap.isEmpty()){
            strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_MONTO_TAP, MontoTap);
            strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_MONEDA, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_MONEDA));
         }
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }
}
