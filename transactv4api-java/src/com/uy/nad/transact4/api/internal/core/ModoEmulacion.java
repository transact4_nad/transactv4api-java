/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core;

import com.uy.nad.transact4.api.internal.core.mensajes.MensajeCierreConsultarEstadoEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeCierreProcesarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeReportarAvanceEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnConfirmarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeTrnProcesarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajePingFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.MensajeReiniciarFPOSEnvio;
import com.uy.nad.transact4.api.internal.core.mensajes.absMensajeEnvio;

/**
 *
 * @author yunior
 */
public class ModoEmulacion {

    private int numeroReporteAvance = 0;

    public String EnviarPin() throws Exception {
        numeroReporteAvance = 0;
        return "<INICMD>APIRSP<CAMPO>CODRESP<VALOR>0<FS><CAMPO>MSGRESP<VALOR>TRN OK<FS><CAMPO>RETARDO<VALOR>250<FS><FINCMD><LRC>00091";
    }

    public String EnviarDatosAlPOS(absMensajeEnvio objMensajeEnvioAlFPOS) throws Exception {
        if (objMensajeEnvioAlFPOS instanceof MensajeTrnProcesarFPOSEnvio) {
            return ObtenerRespuestaMensajeTrnProcesarEnvio();
        }
        if (objMensajeEnvioAlFPOS instanceof MensajeReportarAvanceEnvio) {
            return ObtenerRespuestaMensajeReportarAvanceEnvio();
        }
        if (objMensajeEnvioAlFPOS instanceof MensajeTrnConfirmarFPOSEnvio) {
            return ObtenerRespuestaMensajeTrnConfirmarFPOSEnvio();
        }
        if (objMensajeEnvioAlFPOS instanceof MensajeCierreProcesarFPOSEnvio) {
            return ObtenerRespuestaMensajeCierreProcesarCierre();
        }
        if (objMensajeEnvioAlFPOS instanceof MensajeCierreConsultarEstadoEnvio) {
            return ObtenerRespuestaMensajeCierreConsultarEstadoEnvio();
        }
        if (objMensajeEnvioAlFPOS instanceof MensajePingFPOSEnvio) {
            return ObtenerRespuestaMensajePingFPOSEnvio();
        }
        if (objMensajeEnvioAlFPOS instanceof MensajeReiniciarFPOSEnvio) {
            return ObtenerRespuestaMensajeReiniciarFPOSEnvio();
        }

        throw new Exception("MODO EMULACION NO SOPORTADO PARA MENSAJE " + absMensajeEnvio.class.getName());
    }

    private String ObtenerRespuestaMensajeTrnProcesarEnvio() {
        numeroReporteAvance++;
        return "<INICMD>ARARQC<CAMPO>NROSERIE<VALOR>275-119-120<FS><CAMPO>APPVER<VALOR>4101<FS><CAMPO>DESCRIPCION<VALOR>SOLICITANDO TARJETA EN POS<FS><FINCMD><LRC>00126";
    }

    private String ObtenerRespuestaMensajeReportarAvanceEnvio() throws Exception {

        if (numeroReporteAvance == 1) {
            Thread.sleep(3000);
            return "<INICMD>ARARQC<CAMPO>NROSERIE<VALOR>275-119-120<FS><CAMPO>APPVER<VALOR>4101<FS><CAMPO>DESCRIPCION<VALOR>SOLICITANDO TIPO DE CUIENTA EN POS<FS><FINCMD><LRC>00134";
        }
        if (numeroReporteAvance == 2) {
            Thread.sleep(3000);
            return "<INICMD>ARARQC<CAMPO>NROSERIE<VALOR>275-119-120<FS><CAMPO>APPVER<VALOR>4101<FS><CAMPO>DESCRIPCION<VALOR>SOLICITANDO PIN EN POS<FS><FINCMD><LRC>00122";
        }
        if (numeroReporteAvance == 3) {
            Thread.sleep(3000);
            return "<INICMD>ARARQC<CAMPO>NROSERIE<VALOR>275-119-120<FS><CAMPO>APPVER<VALOR>4101<FS><CAMPO>DESCRIPCION<VALOR>PROCESANDO TRANSACCION<FS><FINCMD><LRC>00122";
        } else {
            Thread.sleep(6000);
            return "<INICMD>ATRRSP<CAMPO>CODRESP<VALOR>0<FS><CAMPO>MSGRESP<VALOR>APROBADA<FS><CAMPO>APROBADA<VALOR>TRUE<FS><CAMPO>CODRESPADQ<VALOR>00<FS><CAMPO>TRNRSPXML<VALOR><TRNRSPXML><NROAUTORIZACION><VALOR>790155</VALOR></NROAUTORIZACION><TRNID><VALOR>575793</VALOR></TRNID><ESOFFLINE><VALOR>FALSE</VALOR></ESOFFLINE><LOTE><VALOR>155</VALOR></LOTE><TICKET><VALOR>790</VALOR></TICKET><DISVOUCHER><VALOR>--\n"
                    + "#CF#\n"
                    + "09//05//2019                           13:22\n"
                    + "#LOGO#\n"
                    + "/H              VENTA MAESTRO               /N\n"
                    + "              ESTACION ESSO               \n"
                    + "                RUT: 1234                 \n"
                    + "            Gral. Rivera 1234             \n"
                    + "\n"
                    + "#CF#\n"
                    + "#CF#\n"
                    + "         DEBITO - ON LINE - BANDA         \n"
                    + "Com.: NACOMB                Term.: NAD0001\n"
                    + "Ticket: 790                      Lote: 155\n"
                    + "Tar.: 501073******0532         Vto.: **//**\n"
                    + "Plan//Cuotas: 0//1              Aut.: 790155\n"
                    + "\n"
                    + "              NO APLICA LEY               \n"
                    + "/HTOTAL:                             $ 50,00/N\n"
                    + "CAJA AHORRO $ \n"
                    + "\n"
                    + "#CF#\n"
                    + "#CF#\n"
                    + "           NO SE REQUIERE FIRMA           \n"
                    + "                                          \n"
                    + "#CF#\n"
                    + "#CF#\n"
                    + "/I          *** COPIA COMERCIO ***          /N\n"
                    + "#CF#\n"
                    + "#BR#\n"
                    + "#CF#\n"
                    + "09//05//2019                           13:22\n"
                    + "#LOGO#\n"
                    + "/H              VENTA MAESTRO               /N\n"
                    + "              ESTACION ESSO               \n"
                    + "                RUT: 1234                 \n"
                    + "            Gral. Rivera 1234             \n"
                    + "\n"
                    + "#CF#\n"
                    + "#CF#\n"
                    + "         DEBITO - ON LINE - BANDA         \n"
                    + "Com.: NACOMB                Term.: NAD0001\n"
                    + "Ticket: 790                      Lote: 155\n"
                    + "Tar.: 501073******0532         Vto.: **//**\n"
                    + "Plan//Cuotas: 0//1              Aut.: 790155\n"
                    + "\n"
                    + "              NO APLICA LEY               \n"
                    + "/HTOTAL:                             $ 50,00/N\n"
                    + "CAJA AHORRO $ \n"
                    + "\n"
                    + "#CF#\n"
                    + "#CF#\n"
                    + "/I          *** COPIA CLIENTE ***           /N\n"
                    + "#CF#\n"
                    + "</VALOR></DISVOUCHER><OPERACION><VALOR>VTA</VALOR></OPERACION><MONEDA><VALOR>0858</VALOR></MONEDA><MONTO><VALOR>5000</VALOR></MONTO><PROPINA><VALOR>0</VALOR></PROPINA><CASHBACK><VALOR>0</VALOR></CASHBACK><CUOTAS><VALOR>1</VALOR></CUOTAS><ADQID><VALOR>17</VALOR></ADQID><EMISORID><VALOR>3</VALOR></EMISORID><EMISORNOM><VALOR>SCOTIABANK</VALOR></EMISORNOM><PAN><VALOR>501073******0532</VALOR></PAN><PANIIN><VALOR>501073</VALOR></PANIIN><TARJETATIPO><VALOR>DEB</VALOR></TARJETATIPO><TARJETAEXTRANJERA><VALOR>FALSE</VALOR></TARJETAEXTRANJERA><TARJETAPRESTACIONES><VALOR>FALSE</VALOR></TARJETAPRESTACIONES><TARJETAALIMENTACION><VALOR>FALSE</VALOR></TARJETAALIMENTACION><DECRETOLEYAPLICADO><VALOR>FALSE</VALOR></DECRETOLEYAPLICADO><DECRETOLEYNRO><VALOR>0</VALOR></DECRETOLEYNRO><DECRETOLEYMONTO><VALOR>0</VALOR></DECRETOLEYMONTO><FCHHORA><VALOR>2019-05-09T13:22:49Z</VALOR></FCHHORA><EMPRUT><VALOR>1234</VALOR></EMPRUT><EMPNOM><VALOR>ESTACION ESSO</VALOR></EMPNOM><SUCNOM><VALOR>ESSO</VALOR></SUCNOM><SUCDIR><VALOR>Gral. Rivera 1234</VALOR></SUCDIR><MID><VALOR>NACOMB</VALOR></MID><TID><VALOR>NAD0001</VALOR></TID><MEDIO><VALOR>BAN</VALOR></MEDIO><ADQNOM><VALOR>MAESTRO</VALOR></ADQNOM><TITULAR><VALOR></VALOR></TITULAR><VENCIMIENTO><VALOR>**/**</VALOR></VENCIMIENTO><DOCIDENTIDAD><VALOR></VALOR></DOCIDENTIDAD><FACTURA><VALOR></VALOR></FACTURA><MONTOFACTURA><VALOR>0</VALOR></MONTOFACTURA><MONTOGRAVADO><VALOR>0</VALOR></MONTOGRAVADO><MONTOGRAVADOTRN><VALOR>0</VALOR></MONTOGRAVADOTRN><MONTOIVA><VALOR>0</VALOR></MONTOIVA><MONTOIVATRN><VALOR>0</VALOR></MONTOIVATRN><DECRETOLEY><VALOR>0</VALOR></DECRETOLEY><DECRETOLEYNOM><VALOR>BOLETA CON RUT</VALOR></DECRETOLEYNOM><DECRETOLEYADQID><VALOR>0</VALOR></DECRETOLEYADQID><DECRETOLEYVOUCHER><VALOR></VALOR></DECRETOLEYVOUCHER><PLAN><VALOR>1</VALOR></PLAN><PLANNOM><VALOR>SIN PLAN</VALOR></PLANNOM><PLANADQPLAN><VALOR>0</VALOR></PLANADQPLAN><PLANADQTIPOPLAN><VALOR>0</VALOR></PLANADQTIPOPLAN><TIPOCUENTA><VALOR>1</VALOR></TIPOCUENTA><TIPOCUENTANOM><VALOR>CAJA AHORRO $</VALOR></TIPOCUENTANOM><CUENTA><VALOR></VALOR></CUENTA><EMVAPPID><VALOR></VALOR></EMVAPPID><EMVAPPNAME><VALOR></VALOR></EMVAPPNAME><FIRMARVOUCHER><VALOR>FALSE</VALOR></FIRMARVOUCHER><TEXTOADICIONAL><VALOR></VALOR></TEXTOADICIONAL></TRNRSPXML><FS><FINCMD><LRC>03999";
        }

    }

    private String ObtenerRespuestaMensajeTrnConfirmarFPOSEnvio() throws Exception {
        Thread.sleep(2000);
        return "<INICMD>ATCRSP<CAMPO>CODRESP<VALOR>0<FS><CAMPO>MSGRESP<VALOR>TRN OK<FS><FINCMD><LRC>00063";
    }

    private String ObtenerRespuestaMensajeCierreProcesarCierre() throws Exception {
        Thread.sleep(1000);
        String strResp = "<INICMD>ACRRSP";
        strResp += "<CAMPO>CODRESP<VALOR>0<FS>";
        strResp += "<CAMPO>MSGRESP<VALOR>TRN OK!<FS>";
        strResp += "<CAMPO>TOKEN<VALOR>419ef6ea-ee75-4b65-a77c-2d23910e2200<FS>";
        strResp += "<FINCMD>";
        strResp += "<LRC>00123";
        return strResp;
    }

    private String ObtenerRespuestaMensajeCierreConsultarEstadoEnvio() throws Exception {
        Thread.sleep(1000);
        String strResp = "<INICMD>ACORSP";
        strResp += "<CAMPO>CODRESP<VALOR>0<FS>";
        strResp += "<CAMPO>MSGRESP<VALOR>TRN OK!<FS>";
        strResp += "<CAMPO>FINALIZADO<VALOR>TRUE<FS>";
        strResp += "<CAMPO>CCORSPXML<VALOR>";
        strResp += "<CCORSPXML>";
        strResp += "<DISVOUCHER><VALOR></VALOR></DISVOUCHER>";
        strResp += "<DATOSCIERRE>ICYAAB+LCAAAAAAABAC1WgtyozAM9VFygZ18mqa00+kMgbRNt6FsINnP/Q+ysoACtiwL40wm1B/p6VmWZcf0WWXqS32qA/zN1BHKhXpRz1g+qDN8DlgvofQFrQdVqVTlUD6DRI59V2j5xJYXtYX6ctSi65y2lqtbK0O9e7UhsYbyaYu8b1FNjBqeF5ClcCjdAltS0Klbjv+gNvSLiWLW/Qja3zn6tQIGJuaKRLV1TlB6c6KMx5aSmDbC2IrW/EXwkzDsdV+h5x0+WjK10DaAtlY79QOeG3iuoFxD+V49qTtoeYKWBDxIWbSRDzCmEme8JixpCzv4rPG7QsuPJLKJ09QLkDhZuAX0/lYLaHmD0gK9XTs8buJUYCFz4JYYOwV8deyU2ENhmhhNPQeds4W5x7WgZStkfGznawH+fnD4wsQ7kav+ET479OkO5msLNXrt9tq1I3vcways4bnCvxRGr5lBTwH1K46qJuJr7YxWSvOEa1SvWzdiAmNbtR96hDRGZ1F7/IqZtWLtaB9wzDmcnoPc2hpjQDIuDrNjp58X0HfnTn5eaP2eQyi+D4H27tzRSNBc/p0/UglexzRHWX0euASO1YXQ8wm34cegPT5/VDI8l89jjFiG2Oz6ObbWmOfNcxx92jP1mrOV3sPlZ7yxfFOj97YU2g7qD4PS6zX1jh/Fht4laL1xayg7W19nrLP6AL0a93+9Y9un0Qxngd7TXPrjnrLdsevB+tV109IrliuRNQ5zLKlRjuidYiQ9xzqHeWq56DOVNJKHOlR9vOYS2PUSZuX1evoctMe1UaNklxk4fNfpQ4plyvXZgLO6a08oCXOik6GWuE800SDNI43GsCxfs6ZGV2/W2f7b9lCvwlPy4luWwzVxuvZuVffc/ZnZrTvu6dbyHHQKI9Y53BWLLr5TY3qI4448l7XwWObs5hi3Z8xATRT/dTLQvxsf0I/0eXwqdjHIcSn222uLwnC1p4h+RH9PvQGR4lFyzT2HHR0JzhbtKx7HFUFjrbnxaKO5Y4i3/IgjXeG9wrTYtHG7CAn/RcsjdL0xfuH6sOz+5rxUYqZO2bFx/pyGa0tneIp4xzuxTP1keEjHzSPa8lT8u1n4Mk8Ivhklc28jpuDZfOPdTkzD9kWSlNcWWMkZyW34I03KMDSSp7Gj4k7KcAPzmsB3J+Y61ZoZo6E3KjIcm+9cez4kX6TFsy/B9cdWPD4yZFkExWMlR+ezZ6w4laD68mc874Sx4XLnreI7jCmfSW8X+2Fs/bn0tutiCuv5d9RSJJr7PJt+LF88xuQgQ/ZHXUxOUmxZZMWeMSk+n33jxa70fQCff2N6KZQRl3VvF/OhbPm8e8v1EMrYn3dvvVZk1uj7oaZHcmum39nodaH5fGAP9dZgKZYb3totHffdvRTFyH6L4Mbh33V0/0E1fkPHsfK9B1ySLTbaf2t2+BcgJgAA</DATOSCIERRE>";
        strResp += "</CCORSPXML><FS>";
        strResp += "<FINCMD>";
        strResp += "<LRC>01665";

        return strResp;
    }

    private String ObtenerRespuestaMensajePingFPOSEnvio() throws Exception {
        Thread.sleep(1000);
        String strResp = "<INICMD>APIRSP<CAMPO>CODRESP<VALOR>0<FS><CAMPO>MSGRESP<VALOR>TRN OK<FS><CAMPO>RETARDO<VALOR>500<FS><FINCMD><LRC>00091";
        return strResp;
    }

    private String ObtenerRespuestaMensajeReiniciarFPOSEnvio() throws Exception {
        Thread.sleep(1000);
        String strResp = "<INICMD>UPDRSP<CAMPO>CODRESP<VALOR>0<FS><CAMPO>MSGRESP<VALOR><FS><FINCMD><LRC>00057";
        return strResp;
    }

}
