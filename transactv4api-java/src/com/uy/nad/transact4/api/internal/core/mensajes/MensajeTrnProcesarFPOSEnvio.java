/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
import com.uy.nad.transact4.api.internal.core.TransaccionCampo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yunior
 */
public class MensajeTrnProcesarFPOSEnvio extends absMensajeEnvio {

    private Transaccion objTrn;

    public MensajeTrnProcesarFPOSEnvio(Transaccion objTrn) {
        this.objTrn = objTrn;
    }

    @Override
    public String ObtenerMensajePOS() throws Exception {
        String strMensaje = DiccionarioComandos.COMANDO_PROCESARTRN;
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_EMPCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_EMPCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_TERMCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_TERMCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP, objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP, 0));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_OPERACION, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_OPERACION).toString().toUpperCase());
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CFG_INGRESA_PROPINA_EN_POS, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_INGRESA_PROPINA_EN_POS));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_ATRRQCXML, ObtenerATRRQCXMLByTransaccion(objTrn));
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }

    private String ObtenerATRRQCXMLByTransaccion(Transaccion objTrn) throws Exception {
        String strWKF = "<ATRRQCXML>";
        strWKF += "<MSGVERSION>";
        strWKF += "<VALOR>" + objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER) + "</VALOR>";
        strWKF += "</MSGVERSION>";
        for (TransaccionCampo var : objTrn.ObtenerListaCampos()) {
            if (CampoSeEnvia(var.getNombreCampo())) {
                strWKF += "<" + var.getNombreCampo().toUpperCase() + ">";
                strWKF += "<VALOR>" + var.getValorCampo().toString().toUpperCase() + "</VALOR>";
                strWKF += "</" + var.getNombreCampo().toUpperCase() + ">";
            }
        }
        strWKF += "</ATRRQCXML>";
        return strWKF;
    }

    private Boolean CampoSeEnvia(String NombreCampo) {
        List<String> ListaCamposNoUsados = new ArrayList();
        ListaCamposNoUsados.add(DiccionarioCampos.TRANSACT_CAMPO_CFG_INGRESA_PROPINA_EN_POS);
        if (ListaCamposNoUsados.contains(NombreCampo)) {
            return false;
        }
        return true;
    }
    /*
     Private Function CampoSeEnvia(NombreCampo As String) As Boolean
        Dim ListaCamposNoUsados As New List(Of String)({TRANSACT_CAMPO_MULTITRANSACCION, TRANSACT_CAMPO_APAGARCTLS})
        If ListaCamposNoUsados.Contains(NombreCampo) Then
            Return False
        End If
        Return True
    End Function*/

}
