/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;

import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;

/**
 *
 * @author yunior
 */
public class MensajeReportarAvanceEnvio extends absMensajeEnvio{
    private int CodResp;

    public MensajeReportarAvanceEnvio(int CodResp) {
        this.CodResp = CodResp;
    }
    
    @Override
    public String ObtenerMensajePOS() throws Exception {
        String strMensaje =DiccionarioComandos.COMANDO_RESULTADO_REPORTARAVANCEENAPI;
        strMensaje = super.AgregarCampo(strMensaje,DiccionarioCampos.RESPUESTA_SOLICITUD_NOMBRE_CAMPO_CODRESP, CodResp);
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }
}
