/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.core.mensajes;
import com.uy.nad.transact4.api.internal.core.DiccionarioCampos;
import com.uy.nad.transact4.api.internal.core.Transaccion;
/**
 *
 * @author Yunior
 */
public class MensajeLeerTarjetaFPOSEnvio extends absMensajeEnvio{
      private Transaccion objTrn;

    public MensajeLeerTarjetaFPOSEnvio(Transaccion objTrn) {
        this.objTrn = objTrn;
    }
     

    @Override
    public String ObtenerMensajePOS() throws Exception {
        String strMensaje = DiccionarioComandos.COMANDO_LEERTARJETA;
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CFG_APIVER));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_EMPCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD, objTrn.ObtenerValorCampo(DiccionarioCampos.TRANSACT_CAMPO_CIERRE_TERMCOD));
        strMensaje = super.AgregarCampo(strMensaje, DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP, objTrn.ObtenerValorCampoSiExiste(DiccionarioCampos.TRANSACT_CAMPO_MULTIEMP,0));
        strMensaje = super.AgregarCabeceraYCRCMensajePOS(strMensaje);
        return strMensaje;
    }
}
