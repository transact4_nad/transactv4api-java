/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api.internal.cliente;

import com.uy.nad.transact4.api.internal.core.exceptions.NADGeneralException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NoRouteToHostException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.Arrays;

/**
 *
 * @author yunior
 */
public class TCPClientePos implements ICliente {

    private final ClienteListener clienteListener = null;
    private Socket socket = null;
    private PrintWriter mBufferOut = null;
    private BufferedReader mBufferIn = null;
    private final String ip;
    private final int puerto;
    private int timeoutMs;
    //TODO: JUgar con un solo intento y subir el timeout de conexion.
    private final int timeoutConexionMS = 5000;
    private final int milisegundosIntentoReconexion = 2000;
    private final char CHR_MSG_INIT = 2;
    private final char CHR_MSG_FIN = 3;
    private final EstadoClienteConectado estadoCliente = EstadoClienteConectado.ConexionCerrada;

    public TCPClientePos(String ip, int puerto, int timeoutMs) {
        this.ip = ip;
        this.puerto = puerto;
        this.timeoutMs = timeoutMs;
    }

    @Override
    public void Conectar() throws NADGeneralException {
        Boolean conectado = false;
        Boolean salir = false;
        int intentosConexion = 5;
        while (!salir) {
            Boolean lanzadaEx = false;
            String MensajeEx = "ERROR DE CONEXION CON " + this.ip;
            try {
                InetAddress serverAddr = InetAddress.getByName(this.ip);
                SocketAddress sockaddr = new InetSocketAddress(serverAddr, this.puerto);
                socket = new Socket();
                socket.connect(sockaddr, this.timeoutConexionMS);
                conectado = true;
                intentosConexion = 3;
                salir = true;
                
            }
            catch(SocketTimeoutException ex){
                lanzadaEx = true;
                ex.printStackTrace();
                MensajeEx = MensajeEx.concat(" (NO SE CONECTA AL DISPOSITIVO DE COBRO)");
            }
            catch(NoRouteToHostException ex){
                lanzadaEx = true;
                ex.printStackTrace();
                MensajeEx = MensajeEx.concat(" (NO ESTA CONECTADO A LA RED)");
            } 
            
            catch (UnknownHostException ex) {
                lanzadaEx = true;
                ex.printStackTrace();
                MensajeEx = MensajeEx.concat(" (NO SE RESUELVE RESUELVE HOST)");
            } catch (IllegalArgumentException ex) {
                lanzadaEx = true;
                ex.printStackTrace();
                MensajeEx = MensajeEx.concat(" (ARGUMENTOS NO VALIDOS)");
            } catch (SocketException ex) {
                lanzadaEx = true;
                ex.printStackTrace();
                if (ex.getMessage().toLowerCase().contains("socket is closed")) {
                    MensajeEx = MensajeEx.concat(" (SOCKET CERRADO)");
                }
            } catch (UnsupportedOperationException ex) {
                lanzadaEx = true;
                ex.printStackTrace();
                MensajeEx = MensajeEx.concat(" (OPERACION NO PERMITIDA)");
            } catch (IOException ex) {
                lanzadaEx = true;
                ex.printStackTrace();
            } catch (Exception ex) {
                lanzadaEx = true;
                ex.printStackTrace();
            }

            if (lanzadaEx) {
                if (intentosConexion <= 0) {
                    salir = true;
                    throw new NADGeneralException(MensajeEx);
                } else {
                    intentosConexion--;

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void Desconectar() throws NADGeneralException {
        try {
            socket.close();
        } catch (IOException ex) {
           throw new NADGeneralException("ERROR CERRANDO SOCKET");
        }
    }

    @Override
    public String EnviarMensaje(String mensaje) throws NADGeneralException {
        if (socket.isConnected()) {
            String Msg = new StringBuilder().append(CHR_MSG_INIT)
                    .append(mensaje)
                    .append(CHR_MSG_FIN).toString();
            try {
                mBufferOut = new PrintWriter(socket.getOutputStream());
            } catch (IOException ex) {
                ex.printStackTrace();
                throw new NADGeneralException("ERROR ENVIANDO DATOS AL " + this.ip);
            }
            mBufferOut.write(Msg);
            mBufferOut.flush();
            try {
                mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            } catch (IOException ex) {
                  ex.printStackTrace();
                throw new NADGeneralException("ERROR ENVIANDO DATOS AL " + this.ip +"(CREANDO BUFFER DE LECTURA)");
            }
            String datosRecibidos = LeerDatos(this.timeoutMs);
            return datosRecibidos;
        } else {
            throw new NADGeneralException("Cliente no conectado");
        }
    }

    private String LeerDatos(int milisegundosTimeout) throws NADGeneralException {
        boolean completado = false;
        String data = "";
        int posIni = -1;
        int posFin = -1;
        int recvBytesTotal = 0;
        String _datos = "";
        try {
            this.socket.setSoTimeout(milisegundosTimeout);
        } catch (SocketException ex) {
           ex.printStackTrace();
           throw new NADGeneralException("ERROR LEYENDO DATOS DESDE " + this.ip +"(SETEANDO SOCKET TIMEOUT)");
        }
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        long tickInicio = timestamp.getTime();
        while (true) {
            char[] readBytes = new char[10240];
            Arrays.fill(readBytes, (char) 0);
            int recvBytes = 0;
            try {
                recvBytes = mBufferIn.read(readBytes);
            } catch (IOException ex) {
                ex.printStackTrace();
                throw new NADGeneralException("ERROR LEYENDO DATOS DESDE " + this.ip);
            }
            catch (Exception ex) {
                ex.printStackTrace();
                throw new NADGeneralException("ERROR LEYENDO DATOS DESDE " + this.ip);
            }
            if (recvBytes > 0) {
                String datos = new String(readBytes).substring(0, recvBytes);
                if (posIni < 0) {
                    posIni = datos.indexOf(CHR_MSG_INIT);
                    if (posIni < 0) {
                        continue;
                    }
                }
                if (_datos.isEmpty()) {
                    datos = datos.substring(posIni + 1);
                }

                if (posFin < 0) {
                    posFin = datos.indexOf(CHR_MSG_FIN);
                    if (posFin >= 0) {
                        _datos = _datos + datos.substring(0, posFin);
                        break;
                    }
                }
                _datos = _datos + datos;

            } else if (recvBytes != 0) {
                 Timestamp timestamp2 = new Timestamp(System.currentTimeMillis());
                 long tickFin = timestamp2.getTime();
                 if(tickFin - tickInicio > milisegundosTimeout){
                  throw new NADGeneralException(this.ip + " NO RESPONDE, TIEMPO DE ESPERA AGOTADO" );
                 }
                  try {
                        Thread.sleep(20);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                continue;
            } else {
                //Log.d(TAG, "Did not get any data: " + recvBytes);
                break;
            }
        }
        if (_datos.length() != 0) {
            return _datos;
        }
        throw new NADGeneralException("NO RECIBE DADOS DESDE " + this.ip );
    }
     @Override
    public void EnviarMensajeSinRespuesta(String mensaje) throws NADGeneralException
    {
     if (socket.isConnected()) {
            String Msg = new StringBuilder().append(CHR_MSG_INIT)
                    .append(mensaje)
                    .append(CHR_MSG_FIN).toString();
            try {
                mBufferOut = new PrintWriter(socket.getOutputStream());
            } catch (IOException ex) {
                ex.printStackTrace();
                throw new NADGeneralException("ERROR ENVIANDO DATOS AL " + this.ip);
            }
            mBufferOut.write(Msg);
            mBufferOut.flush();
        } else {
            throw new NADGeneralException("Cliente no conectado");
        }
    }

    @Override
    public void SetTimeout(int timeoutMs) {
        this.timeoutMs = timeoutMs;
    }

}
