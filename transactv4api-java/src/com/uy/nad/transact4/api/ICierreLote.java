/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author Yunior Bauta nad.uy
 */
public interface ICierreLote {
    public void setEmpCod(String EmpCod);
    public void setTermCod(String TermCod);
    public void setMultiEmp(Integer MultiEmp);
    public void setProcesadorId(Integer ProcesadorId);
    public void setCierreCentralizado(Boolean CierreCentralizado);
    public void setEmpHash(String EmpHash);
    public ICierreLoteComportamiento getComportamiento();
    public ICierreLoteRespuesta getRespuesta();
     
    public void ProcesarCierre() throws Exception;
    public Boolean ConsultarEstadoCierre(String TokenCierre) throws Exception;
    
}
