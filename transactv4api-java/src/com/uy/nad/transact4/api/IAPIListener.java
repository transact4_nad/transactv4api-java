/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface IAPIListener {
   void evtNotificadAvance(String mensaje);
   void evtSolicitarCampoEnAPI(IMetadatosSolicitudCampo datosSolicitudCampo);    

}
