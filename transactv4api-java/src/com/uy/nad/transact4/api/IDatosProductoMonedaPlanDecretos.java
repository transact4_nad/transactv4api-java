/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface IDatosProductoMonedaPlanDecretos {

    public Boolean getDecretoLeyAplicado();

    public String getDecretoLeyNro();

    public Integer getSubTotalCantDecreto();

    public Long getSubTotalMontoDecreto();

    public IDatosProductoMonedaPlanDecretosVentas getVenta();

    public IDatosProductoMonedaPlanDecretosAnulaciones getAnulacion();

    public IDatosProductoMonedaPlanDecretosDevoluciones getDevolucion();
}
