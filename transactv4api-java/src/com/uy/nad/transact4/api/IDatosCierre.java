/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uy.nad.transact4.api;

/**
 *
 * @author yunior
 */
public interface IDatosCierre {

    public Integer getProcesadorId();

    public Long getLote();

    public String getNroAutorizacion();

    public Boolean getAprobado();

    public String getCodRespuesta();

    public String getMsgRespuesta();

    public IDatosCierreExtendida getExtendida();
}
